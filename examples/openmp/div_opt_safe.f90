MODULE operators
  USE mpi_base
  USE mesh,       only: m
  USE boundaries
  implicit none
PUBLIC
CONTAINS

!*******************************************************************************
! Divergence operator -- uses 4th order centered finite difference.
! the ghost zones are calculated after finishing the transfers.
!*******************************************************************************
FUNCTION div(vx,vy,vz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: div
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx,vy,vz
  integer :: idim, ix, iy, iz
  real    :: ax, bx, ay, by, az, bz
  real(8) :: wc
  !
  ax = 8. / (12.*m(1)%d); bx = -1. / (12.*m(1)%d)
  ay = 8. / (12.*m(2)%d); by = -1. / (12.*m(2)%d)
  az = 8. / (12.*m(3)%d); bz = -1. / (12.*m(3)%d)
  !
  ! These OpenMP statemts require (as a safe guard) everything to be declared
  ! either private or shared.
  !
  !$omp parallel default(none) private(ix,iy,iz) shared(ax,ay,ax,bx,by,bz,div,vx,vy,vz)
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    !$omp do
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      div(ix,iy,iz) = ax*(vx(ix+1,iy,iz) - vx(ix-1,iy,iz)) &
                    + bx*(vx(ix+2,iy,iz) - vx(ix-2,iy,iz))                     ! d(vx)/dx
    end do
    end do
    end do
    !$omp end do nowait
  endif
  ...
  !$omp end parallel
  call boundary (div)
  !
END FUNCTION div
END MODULE operators
