SUBROUTINE planck (n, lambda, tt, ss)
  implicit none
  integer:: n
  real, dimension(n,n,n):: tt, ss
  real, dimension(n):: e
  real:: lambda, cp, c2
  integer:: iy, iz
!...............................................................................
  cp=1.191e7
  c2=1.438e4
  do iz=1,n
  do iy=1,n
    e(:) = exp(-c2/(tt(:,iy,iz)*lambda))
    ss(:,iy,iz) = cp*e(:)/((1.0-e(:))*lambda**5)
   end do
   end do
END SUBROUTINE
