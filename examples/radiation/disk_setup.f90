SUBROUTINE disk_setup (n, rho, tt, r, z)
  implicit none
  integer:: n
  real, dimension(n,n,n):: rho, tt, r, z
  real:: tt0, ds, r0, h0, hp, sp, x, y, hz, sigma, fz
  integer:: ix, iy, iz
!...............................................................................
  tt0 = 100                                            ! edge temperature
  ds = 1.0/n                                           ! grid spacing
  r0 = 3*ds; h0=0.5; hp=1.5; sp=-0.5                   ! disk shape parameters
!
  do iz=1,n
  do iy=1,n
  do ix=1,n
    z(ix,iy,iz) = (iz-n/2)*ds
    y           = (iy-n/2)*ds
    x           = (ix-n/2)*ds
    r(ix,iy,iz) = sqrt(x**2+y**2)                      ! cylinder radius
    hz = h0*(r(ix,iy,iz)+r0)**hp                       ! scale height
    sigma = (r(ix,iy,iz)+r0)**sp                       ! surface density
    fz = exp(-(z(ix,iy,iz)/hz)**2)/hz                  ! vertical profile
    rho(ix,iy,iz) = sigma*fz/(r(ix,iy,iz)+r0)+1e-10    ! density
    tt(ix,iy,iz) = tt0/sqrt(r(ix,iy,iz)+r0)            ! temperature
  end do
  end do
  end do  
END SUBROUTINE
