!*******************************************************************************
MODULE radiation_mod
CONTAINS

!*******************************************************************************
! Simple integral method, using a piece-wise linear approximation of the source
! function.  The radiation intensity is computed in the positive x-direction
!*******************************************************************************
SUBROUTINE integral1x (n, dtau, ss, ii, ii_fwd)
  implicit none
  integer:: n
  real, dimension(n,n,n):: ss, dtau, ii
  real, dimension(n,n):: ii_fwd
  integer:: ix, iy, iz
  real:: a0
!...............................................................................
  do iz=1,n
  do iy=1,n
    ii(1,iy,iz) = 0.0
    do ix=1,n-1
      a0 = exp(-dtau(ix,iy,iz))
      ii(ix+1,iy,iz) = ii(ix,iy,iz)*a0 &
        + ss(ix,iy,iz)*(1.0-a0) &
        + (ss(ix+1,iy,iz)-ss(ix,iy,iz))/dtau(ix,iy,iz)*(a0-(1-dtau(ix,iy,iz)))
    end do
  end do
  end do
  ii_fwd(:,:) = ii(n,:,:)
END SUBROUTINE

!*******************************************************************************
! Simple integral method, using a piece-wise linear approximation of the source
! function.  The radiation intensity is computed in the positive y-direction
!*******************************************************************************
SUBROUTINE integral1y (n, dtau, ss, ii, ii_fwd)
  implicit none
  integer:: n
  real, dimension(n,n,n):: ss, dtau, ii
  real, dimension(n,n):: ii_fwd
  integer:: ix, iy, iz
  real:: a0
!...............................................................................
  ii(:,1,:) = 0.0
  do iz=1,n
  do iy=1,n-1
    do ix=1,n
      a0 = exp(-dtau(ix,iy,iz))
      ii(ix,iy+1,iz) = ii(ix,iy,iz)*a0 &
        + ss(ix,iy,iz)*(1.0-a0) &
        + (ss(ix,iy+1,iz)-ss(ix,iy,iz))/dtau(ix,iy,iz)*(a0-(1-dtau(ix,iy,iz)))
    end do
  end do
  end do
  ii_fwd(:,:) = ii(:,n,:)
END SUBROUTINE

!*******************************************************************************
! Simple integral method, using a piece-wise linear approximation of the source
! function.  The radiation intensity is computed in the positive z-direction
!*******************************************************************************
SUBROUTINE integral1z (n, dtau, ss, ii, ii_fwd)
  implicit none
  integer:: n
  real, dimension(n,n,n):: ss, dtau, ii
  real, dimension(n,n):: ii_fwd
  integer:: ix, iy, iz
  real:: a0
!...............................................................................
  ii(:,:,1) = 0.0
  do iz=1,n-1
    do iy=1,n
      do ix=1,n
        a0 = exp(-dtau(ix,iy,iz))
        ii(ix,iy,iz+1) = ii(ix,iy,iz)*a0 &
          + ss(ix,iy,iz)*(1.0-a0) &
          + (ss(ix,iy,iz+1)-ss(ix,iy,iz))/dtau(ix,iy,iz)*(a0-(1-dtau(ix,iy,iz)))
      end do
    end do
  end do
  ii_fwd(:,:) = ii(:,:,n)
END SUBROUTINE

!*******************************************************************************
! Same as above, except with (:,:,iz) syntax
!*******************************************************************************
SUBROUTINE integral2z (n, dtau, ss, ii, ii_fwd)
  implicit none
  integer:: n
  real, dimension(n,n,n):: ss, dtau, ii
  real, dimension(n,n):: ii_fwd, a0
  integer:: i
!...............................................................................
  ii(:,:,1) = 0.0
  do i=1,n-1
    a0 = exp(-dtau(:,:,i))
    ii(:,:,i+1) = ii(:,:,i)*a0 &
                + ss(:,:,i)*(1.0-a0) &
                + (ss(:,:,i+1)-ss(:,:,i))/dtau(:,:,i)*(a0-(1-dtau(:,:,i)))
  end do
  ii_fwd(:,:) = ii(:,:,n)
END SUBROUTINE

!*******************************************************************************
! Same as integral1x, except the order of the ix and iy loops have been swapped
!*******************************************************************************
SUBROUTINE integral2x (n, dtau, ss, ii)
  implicit none
  integer:: n
  real, dimension(n,n,n):: ss, dtau, ii
  integer:: ix, iy, iz
  real:: a0
!...............................................................................
  do iz=1,n
  do iy=1,n
    ii(1,iy,iz) = 0.0
  end do
  do ix=2,n
    do iy=1,n
      a0 = exp(-dtau(ix,iy,iz))
      ii(ix+1,iy,iz) = ii(ix,iy,iz)*a0 &
        + ss(ix,iy,iz)*(1.0-a0) &
        + (ss(ix+1,iy,iz)-ss(ix,iy,iz))/dtau(ix,iy,iz)*(a0-(1-dtau(ix,iy,iz)))
    end do
  end do
  end do
END SUBROUTINE

!***********************************************************************
! Comprehensive integral solver, which is accurate (exact!) to 2nd order
! in the source function
!***********************************************************************
SUBROUTINE integral3a (n, dtau, S, Q, Irv0, Ifw1)
  implicit none
  integer n
  real, dimension(n,n,n) :: dtau, s, q, qfw, qre
  real, dimension(n,n)   :: Ifw1, Irv0
!
! The equations solved are Q' = Q - dS/dtau in the forward direction and
! Q' = -Q + dS/dtau in the reverse direction.  S is assumed to be quadratic,
! so dS/dtau is assumed to be linear in tau.  The analytic solution may
! be written as a linear combination of the formal solution for constant
! and linear source functions.
!
  real, dimension(n,n,n) :: ex0, ds1, d1s, d2s
  real, dimension(n,n)   :: ctau
  real ex1, ex2
  integer i, j, k
!-----------------------------------------------------------------------
!
  do i=2,n
    do j=1,n; do k=1,n
    ex0(j,k,i) = exp(-dtau(j,k,i))                          !          1e
    ds1(j,k,i) = (s(j,k,i)-s(j,k,i-1))/dtau(j,k,i)          ! 1d    1a      ! half-point centered dS/dtau
    end do; end do
  end do
!
!  dS/dtau and d2S/dtau2 (3a+5m+1d)
!
  do i=2,n-1
    do j=1,n; do k=1,n
    ctau(j,k) = 1./(dtau(j,k,i+1)+dtau(j,k,i))                   ! 1d    1a
    d1s(j,k,i) = &
      (ds1(j,k,i+1)*dtau(j,k,i)+ds1(j,k,i)*dtau(j,k,i+1))*ctau(j,k) ! 3m 1a      ! dS/dtau, 2nd. order accurate
    d2s(j,k,i) = (ds1(j,k,i+1)-ds1(j,k,i))*2.*ctau(j,k)          !    2m 1a      ! d2S/dtau2, 2nd. order accurate
    end do; end do
  end do
!
    do j=1,n; do k=1,n
  d2s(j,k,1) = d2s(j,k,2)                                                   ! assume constant d2S/dtau2
  d1s(j,k,1) = d1s(j,k,2) - dtau(j,k,2)*d2s(j,k,2)                          ! extrapolate to 1
  d2s(j,k,n) = d2s(j,k,n-1)                                                 ! assume constant d2S/dtau2
  d1s(j,k,n) = d1s(j,k,n-1) + dtau(j,k,n)*d2s(j,k,n)                        ! extrapolate to n
  qfw(j,k,1) = - s(j,k,1)                                                   ! initial forward Q
    end do; end do
!
!  Forward rays (2a+3m)
!
  do i=2,n
    do j=1,n; do k=1,n
    qfw(j,k,i) = qfw(j,k,i-1)*ex0(j,k,i) &
           - d1s(j,k,i)*(1.-ex0(j,k,i)) &
           + d2s(j,k,i)*(1.-ex0(j,k,i)*(1.+dtau(j,k,i)))    !    3m 2a      ! formal solution forward
    end do; end do
  end do
    do j=1,n; do k=1,n
    Ifw1(j,k) = qfw(j,k,n) + s(j,k,n)                                         ! forward boundary intensity
    end do; end do
END SUBROUTINE                                              ! 2d 13m 11a 1e ! 26f + 1e

!***********************************************************************
! Same as above, except with (:,:,iz) notation
!***********************************************************************
SUBROUTINE integral3b (n, dtau, S, Q, Irv0, Ifw1)
  implicit none
  integer n
  real, dimension(n,n,n) :: dtau, s, q, qfw, qre
  real, dimension(n,n)   :: Ifw1, Irv0
!
! The equations solved are Q' = Q - dS/dtau in the forward direction and
! Q' = -Q + dS/dtau in the reverse direction.  S is assumed to be quadratic,
! so dS/dtau is assumed to be linear in tau.  The analytic solution may
! be written as a linear combination of the formal solution for constant
! and linear source functions.
!
  real, dimension(n,n,n) :: ex0, ds1, d1s, d2s
  real, dimension(n,n)   :: ctau
  real ex1, ex2
  integer i
!-----------------------------------------------------------------------
!
  do i=2,n
    ex0(:,:,i) = exp(-dtau(:,:,i))                          !          1e
    ds1(:,:,i) = (s(:,:,i)-s(:,:,i-1))/dtau(:,:,i)          ! 1d    1a      ! half-point centered dS/dtau
  end do
!
!  dS/dtau and d2S/dtau2 (3a+5m+1d)
!
  do i=2,n-1
    ctau = 1./(dtau(:,:,i+1)+dtau(:,:,i))                   ! 1d    1a
    d1s(:,:,i) = &
      (ds1(:,:,i+1)*dtau(:,:,i)+ds1(:,:,i)*dtau(:,:,i+1))*ctau ! 3m 1a      ! dS/dtau, 2nd. order accurate
    d2s(:,:,i) = (ds1(:,:,i+1)-ds1(:,:,i))*2.*ctau          !    2m 1a      ! d2S/dtau2, 2nd. order accurate
  end do
!
  d2s(:,:,1) = d2s(:,:,2)                                                   ! assume constant d2S/dtau2
  d1s(:,:,1) = d1s(:,:,2) - dtau(:,:,2)*d2s(:,:,2)                          ! extrapolate to 1
  d2s(:,:,n) = d2s(:,:,n-1)                                                 ! assume constant d2S/dtau2
  d1s(:,:,n) = d1s(:,:,n-1) + dtau(:,:,n)*d2s(:,:,n)                        ! extrapolate to n
  qfw(:,:,1) = - s(:,:,1)                                                   ! initial forward Q
!
!  Forward rays (2a+3m)
!
  do i=2,n
    qfw(:,:,i) = qfw(:,:,i-1)*ex0(:,:,i) &
           - d1s(:,:,i)*(1.-ex0(:,:,i)) &
           + d2s(:,:,i)*(1.-ex0(:,:,i)*(1.+dtau(:,:,i)))    !    3m 2a      ! formal solution forward
  end do
    Ifw1(:,:) = qfw(:,:,n) + s(:,:,n)                                         ! forward boundary intensity
END SUBROUTINE                                              ! 2d 13m 11a 1e ! 26f + 1e

!***********************************************************************
SUBROUTINE feautrier1 (N, dtau, S, Q, Irv1, IfwN)
!
!  Solve the transfer equation, given optical depth increments dtau, 
!  source function S.
!
!  This version works with Q = P - S, where P is Feautriers P (the average 
!  of in- and out-going specific intensity) and S is the source function.
!  The equation solved is Q" = Q - S", with boundary conditions 
!  Q' - Q = S - S' - Ifw1 at the first point and Q' + Q = -(S' + S) + Irv1
!  at the last point
!
!  The choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (see below).
!
!  The routine returns the net heating contribution Q at each point, and 
!  the outgoing intensity at the first point.
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  Operation count: 3 divide + 9 multiply + 8 add = 20 flops
!
!***********************************************************************
!
  implicit none
  integer, intent(in)                 :: n
  real, intent(in) , dimension(n,n,n) :: dtau, s
  real, intent(out), dimension(n,n,n) :: q
  real, intent(out), dimension(n,n)   :: Irv1, IfwN
  real,              dimension(n,n)   :: a, dsdtau_1, dsdtau_2, d2sdtau2
!
  real, dimension(n,n,n)              :: a1, a2, a3, d
  integer                             :: j, k
!...............................................................................
!
! ,Boundary condition at k=n
!
  a2(:,:,1)  = +1. + dtau(:,:,2) + 0.5*dtau(:,:,2)**2
  a3(:,:,1)  = -1.
  dsdtau_1(:,:) = (s(:,:,2)-s(:,:,1))/dtau(:,:,2)
  dsdtau_2(:,:) = (s(:,:,3)-s(:,:,2))/dtau(:,:,3)
  d2sdtau2(:,:) = (dsdtau_2(:,:)-dsdtau_1(:,:))*2./(dtau(:,:,2)+dtau(:,:,3))
  q(:,:,1)   = - dtau(:,:,2)*(s(:,:,1) - dsdtau_1) + 0.5*dtau(:,:,2)**2*d2sdtau2
  a2(:,:,1)  = + dtau(:,:,2) + 0.5*dtau(:,:,2)**2
  d(:,:,2)   = -1./dtau(:,:,2)
!
!  Compute matrix elements and eliminate the sub-diagonal in one sweep
!
  do k=2,n-1
    a        = 2./(dtau(:,:,k+1)+dtau(:,:,k))                               ! 1d    1a
    d(:,:,k+1) = -1./dtau(:,:,k+1)                                          ! 1d
    a3(:,:,k)  = a*d(:,:,k+1)                                               !    1m
    a1(:,:,k)  = a*d(:,:,k)                                                 !    1m
    a2(:,:,k)  = 1.
    q(:,:,k)   = a1(:,:,k)*(s(:,:,k)-s(:,:,k-1)) + &
                 a3(:,:,k)*(s(:,:,k)-s(:,:,k+1))                            !    2m 3a
    a        = 1./(a2(:,:,k-1)-a3(:,:,k-1))                                 ! 1d    1a
    a1(:,:,k)  = a1(:,:,k)*a                                                !    1m
    q(:,:,k)   = q(:,:,k) - a1(:,:,k)*q(:,:,k-1)                            !    1m 1a
    a2(:,:,k)  = a2(:,:,k) - a1(:,:,k)*a2(:,:,k-1)                          !    1m 1a
    a2(:,:,k-1) = a
  end do
!
!  Boundary condition at k=n
!
   a2(:,:,n) = +1. + dtau(:,:,n) + 0.5*dtau(:,:,n)**2
   a1(:,:,n) = -1.
   dsdtau_1 = (s(:,:,n  )-s(:,:,n-1))/dtau(:,:,n  )
   dsdtau_2 = (s(:,:,n-1)-s(:,:,n-2))/dtau(:,:,n-1)
   d2sdtau2 = (dsdtau_1-dsdtau_2)*2./(dtau(:,:,n)+dtau(:,:,n-1))
   q(:,:,n)  = - dtau(:,:,n)*(s(:,:,n) + dsdtau_1) - 0.5*dtau(:,:,n)**2*d2sdtau2
   a2(:,:,n) = + dtau(:,:,n) + 0.5*dtau(:,:,n)**2
!
   a(:,:)    = 1./(a2(:,:,n-1)-a3(:,:,n-1))                                 ! 1d    1a
   a1(:,:,n) = a1(:,:,n)*a(:,:)                                             !    1m
   q(:,:,n)    = q(:,:,n) - a1(:,:,n)*q(:,:,n-1)                            !    1m 1a
   a2(:,:,n)   = a2(:,:,n) - a1(:,:,n)*a2(:,:,n-1)                          !    1m 1a
   a2(:,:,n-1) = a(:,:)
   q(:,:,n) = q(:,:,n)/a2(:,:,n)                                            ! solution at n
!
!  Backsubstitute
!
  do k=n-1,1,-1
    q(:,:,k) = (q(:,:,k)-a3(:,:,k)*q(:,:,k+1))*a2(:,:,k)                    !    2m 1a
  end do
!                                                                           ! 3d 9m 8a = 20f
!  Surface intensities
!
  Irv1(:,:) = 2.*(q(:,:,1) + s(:,:,1))                                      ! reverse surface intensity
  IfwN(:,:) = 2.*(q(:,:,n) + s(:,:,n))                                      ! forward surface intensity
END SUBROUTINE

!***********************************************************************
SUBROUTINE feautrier2 (N, dtau, S, Q, Irv1, IfwN)
!
!  Solve the transfer equation, given optical depth increments dtau, 
!  source function S.
!
!  This version works with Q = P - S, where P is Feautriers P (the average 
!  of in- and out-going specific intensity) and S is the source function.
!  The equation solved is Q" = Q - S", with boundary conditions 
!  Q' - Q = S - S' - Ifw1 at the first point and Q' + Q = -(S' + S) + Irv1
!  at the last point
!
!  The choice of equations produces a high precision in Q at large optical
!  depths (where P - S would have round off), while at the same time
!  retaining the precision at small optical depths (see below).
!
!  The routine returns the net heating contribution Q at each point, and 
!  the outgoing intensity at the first point.
!
!  This is a second order version.  For simulations, with rapidly
!  varying absorption coefficients and source functions, this is to be
!  preferred over spline and Hermitean versions because it is positive
!  definite, in the sense that a positive source function is guaranteed
!  to result in a positive average intensity p.  Also, the flux
!  divergence is exactly equal to q, for the conventional definition
!  of the flux.
!
!  "Steins trick" is used; storing the sum of the three elements in each
!  row of the tridiagonal equation system, instead of the diagonal ele-
!  ment.  This allows accurate solutions to be otained for arbitrarily
!  small optical depths.  Without this trick, round off errors become
!  noticeable already when the smallest optical depth is less than the
!  square root of the machine precisions.
!
!  Operation count: 3 divide + 9 multiply + 8 add = 20 flops
!
!***********************************************************************
!
  implicit none
  integer, intent(in)                 :: n
  real, intent(in) , dimension(n,n,n) :: dtau, s
  real, intent(out), dimension(n,n,n) :: q
  real, intent(out), dimension(n,n)   :: Irv1, IfwN
  real,              dimension(n,n)   :: a, dsdtau_1, dsdtau_2, d2sdtau2
!
  real, dimension(:,:,:), allocatable :: a1, a2, a3, d
  integer                             :: j, k
!...............................................................................
  allocate (a1(n,n,n), a2(n,n,n), a3(n,n,n), d(n,n,n))
!
! ,Boundary condition at k=n
!
  a2(:,:,1)  = +1. + dtau(:,:,2) + 0.5*dtau(:,:,2)**2
  a3(:,:,1)  = -1.
  dsdtau_1(:,:) = (s(:,:,2)-s(:,:,1))/dtau(:,:,2)
  dsdtau_2(:,:) = (s(:,:,3)-s(:,:,2))/dtau(:,:,3)
  d2sdtau2(:,:) = (dsdtau_2(:,:)-dsdtau_1(:,:))*2./(dtau(:,:,2)+dtau(:,:,3))
  q(:,:,1)   = - dtau(:,:,2)*(s(:,:,1) - dsdtau_1) + 0.5*dtau(:,:,2)**2*d2sdtau2
  a2(:,:,1)  = + dtau(:,:,2) + 0.5*dtau(:,:,2)**2
  d(:,:,2)   = -1./dtau(:,:,2)
!
!  Compute matrix elements and eliminate the sub-diagonal in one sweep
!
  do k=2,n-1
    a        = 2./(dtau(:,:,k+1)+dtau(:,:,k))                               ! 1d    1a
    d(:,:,k+1) = -1./dtau(:,:,k+1)                                          ! 1d
    a3(:,:,k)  = a*d(:,:,k+1)                                               !    1m
    a1(:,:,k)  = a*d(:,:,k)                                                 !    1m
    a2(:,:,k)  = 1.
    q(:,:,k)   = a1(:,:,k)*(s(:,:,k)-s(:,:,k-1)) + &
                 a3(:,:,k)*(s(:,:,k)-s(:,:,k+1))                            !    2m 3a
    a        = 1./(a2(:,:,k-1)-a3(:,:,k-1))                                 ! 1d    1a
    a1(:,:,k)  = a1(:,:,k)*a                                                !    1m
    q(:,:,k)   = q(:,:,k) - a1(:,:,k)*q(:,:,k-1)                            !    1m 1a
    a2(:,:,k)  = a2(:,:,k) - a1(:,:,k)*a2(:,:,k-1)                          !    1m 1a
    a2(:,:,k-1) = a
  end do
!
!  Boundary condition at k=n
!
   a2(:,:,n) = +1. + dtau(:,:,n) + 0.5*dtau(:,:,n)**2
   a1(:,:,n) = -1.
   dsdtau_1 = (s(:,:,n  )-s(:,:,n-1))/dtau(:,:,n  )
   dsdtau_2 = (s(:,:,n-1)-s(:,:,n-2))/dtau(:,:,n-1)
   d2sdtau2 = (dsdtau_1-dsdtau_2)*2./(dtau(:,:,n)+dtau(:,:,n-1))
   q(:,:,n)  = - dtau(:,:,n)*(s(:,:,n) + dsdtau_1) - 0.5*dtau(:,:,n)**2*d2sdtau2
   a2(:,:,n) = + dtau(:,:,n) + 0.5*dtau(:,:,n)**2
!
   a(:,:)    = 1./(a2(:,:,n-1)-a3(:,:,n-1))                                 ! 1d    1a
   a1(:,:,n) = a1(:,:,n)*a(:,:)                                             !    1m
   q(:,:,n)    = q(:,:,n) - a1(:,:,n)*q(:,:,n-1)                            !    1m 1a
   a2(:,:,n)   = a2(:,:,n) - a1(:,:,n)*a2(:,:,n-1)                          !    1m 1a
   a2(:,:,n-1) = a(:,:)
   q(:,:,n) = q(:,:,n)/a2(:,:,n)                                            ! solution at n
!
!  Backsubstitute
!
  do k=n-1,1,-1
    q(:,:,k) = (q(:,:,k)-a3(:,:,k)*q(:,:,k+1))*a2(:,:,k)                    !    2m 1a
  end do
!                                                                           ! 3d 9m 8a = 20f
!  Surface intensities
!
  Irv1(:,:) = 2.*(q(:,:,1) + s(:,:,1))                                      ! reverse surface intensity
  IfwN(:,:) = 2.*(q(:,:,n) + s(:,:,n))                                      ! forward surface intensity
  q = q+s
  deallocate (a1, a2, a3, d)
END SUBROUTINE
END MODULE
