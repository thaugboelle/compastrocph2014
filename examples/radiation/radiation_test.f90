USE mpi
USE radiation_mod
implicit none
integer ierr, n
real, dimension(:,:,:), pointer:: dtau, ss, ii
real, dimension(:,:), pointer:: irv, ifw
real(kind=8):: wc

read*,n
call MPI_Init (ierr)
allocate (dtau(n,n,n), ss(n,n,n), ii(n,n,n), ifw(n,n), irv(n,n))
dtau=10./n; ss=1.0

wc = MPI_Wtime()
call integral1x (n, dtau, ss, ii, ifw)
wc = MPI_Wtime() - wc
print 1,'integral1x',wc,wc/real(n)**3,sum(ii(n,:,:))/n**2
1 format(a,f7.3,' sec',9p,f8.2,' nanosec/point',0p,f9.4,' average')

wc = MPI_Wtime()
call integral1y (n, dtau, ss, ii, ifw)
wc = MPI_Wtime() - wc
print 1,'integral1y',wc,wc/real(n)**3,sum(ii(:,n,:))/n**2

wc = MPI_Wtime()
call integral1z (n, dtau, ss, ii, ifw)
wc = MPI_Wtime() - wc
print 1,'integral1z',wc,wc/real(n)**3,sum(ii(:,:,n))/n**2

wc = MPI_Wtime()
call integral2x (n, dtau, ss, ii)
wc = MPI_Wtime() - wc
print 1,'integral2x',wc,wc/real(n)**3,sum(ii(:,:,n))/n**2

wc = MPI_Wtime()
call integral3a (n, dtau, ss, ii, irv, ifw)
wc = MPI_Wtime() - wc
print 1,'integral3a',wc,wc/real(n)**3,sum(ifw(:,:))/n**2

wc = MPI_Wtime()
call integral3b (n, dtau, ss, ii, irv, ifw)
wc = MPI_Wtime() - wc
print 1,'integral3b',wc,wc/real(n)**3,sum(ifw(:,:))/n**2

wc = MPI_Wtime()
call feautrier1 (n, dtau, ss, ii, irv, ifw)
wc = MPI_Wtime() - wc
print 1,'feautrier1',wc,wc/real(n)**3,sum(ifw(:,:))/n**2

wc = MPI_Wtime()
call feautrier2 (n, dtau, ss, ii, irv, ifw)
wc = MPI_Wtime() - wc
print 1,'feautrier2',wc,wc/real(n)**3,sum(ifw(:,:))/n**2

deallocate (dtau, ss, ii)
call MPI_Finalize (ierr)

END
