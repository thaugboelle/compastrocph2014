! Compute radiation from a disk with a "star" at the center
PROGRAM disk_radiation
  USE mpi
  USE radiation_mod
  implicit none
  integer, parameter:: nlambda=39, n=100, data_unit=1
  real:: Tstar=3000., kappa=1e-2, tau_dust=2.0, ds
  real, dimension(nlambda):: lambda, f_lambda
  real, dimension(:,:,:), allocatable:: rho, tt, r, z, dtau, ss, ii
  real, dimension(n,n):: ii_obs, ii_fwd, ii_rev
  real(kind=8):: clock
  integer:: i, ierr, iz
!...............................................................................
  call MPI_Init (ierr)
  !-----------------------------------------------------------------------------
  ! To avoid the risk of running low on stack space (which gives obscure "seg
  ! fault" errors), we allocate the 3-D variables explicitly.
  !-----------------------------------------------------------------------------
  allocate (rho(n,n,n), tt(n,n,n), r(n,n,n), z(n,n,n), &
            dtau(n,n,n), ss(n,n,n), ii(n,n,n))
  ds=1./n                                              ! grid spacing
  !-----------------------------------------------------------------------------
  ! Setup a model disk, with relatively cold gas, and a partially obscured
  ! star at the center.
  !-----------------------------------------------------------------------------
  clock = MPI_Wtime()
  call disk_setup (n, rho, tt, r, z)
  dtau = rho*kappa*ds                                  ! optical depth incr
  tt(n/2,n/2,n/2) = Tstar                              ! add "star" at mid point
  tt(n/2,n/2,n/2-1) = Tstar*0.8                        ! add "star" perifery
  tt(n/2,n/2,n/2+1) = Tstar*0.8                        ! add "star" perifery
  dtau(n/2,n/2,n/2-4:n/2+3) = tau_dust                 ! make opaque
  clock = MPI_Wtime() - clock
  print'("setup time", f5.2," sec")',clock
  !-----------------------------------------------------------------------------
  ! Solve for the outgoing radiation, for a number of logarithmically spaced
  ! wavelenghts.
  !-----------------------------------------------------------------------------
  clock = MPI_Wtime()
  do i=1,nlambda
    lambda(i) = 10.**(-0.8+0.1*(i-1))                      ! lambda scale
  end do
  do i=1,nlambda
    call planck (n, lambda(i), tt, ss)                     ! source function
    call feautrier2 (n, dtau, ss, ii, ii_rev, ii_fwd)      ! resulting 3-D intensity
    !call integral1z (n, dtau, ss, ii, ii_fwd)              ! resulting 3-D intensity
    !call integral3b (n, dtau, ss, ii, ii_rev, ii_fwd)      ! resulting 3-D intensity
    f_lambda(i) = sum(ii_fwd)/n**2                         ! average emerging emmission
    if (i==9) then
      open (data_unit,file='star_ray.dat',status='unknown')
      do iz=1,n
        write (data_unit,'(1p,7e12.4)') dtau(n/2,n/2,iz), ss(n/2,n/2,iz), ii(n/2,n/2,iz), &
          tt(n/2,n/2,iz), rho(n/2,n/2,iz), r(n/2,n/2,iz), z(n/2,n/2,iz)
      end do
      close (data_unit)
      open (data_unit,file='disk_ray.dat',status='unknown')
      do iz=1,n
        write (data_unit,'(1p,7e12.4)') dtau(n/4,n/4,iz), ss(n/4,n/4,iz), ii(n/4,n/4,iz), &
          tt(n/4,n/4,iz), rho(n/4,n/4,iz), r(n/4,n/4,iz), z(n/4,n/4,iz)
      end do
      close (data_unit)
      print*,ii_fwd(n/2,n/2)
    end if
  end do
  clock = MPI_Wtime() - clock
  print'("radiative transfer time", f5.2," sec")',clock
  !-----------------------------------------------------------------------------
  ! Write out spectrum results in a file suitable for reading and plotting with
  ! Python or Matlab
  !-----------------------------------------------------------------------------
  open(data_unit,file='f_lambda.dat',status='unknown')
  do i=1,nlambda
    write(data_unit,*) lambda(i), f_lambda(i)
    write(*,*)         lambda(i), f_lambda(i)
  end do
  close (data_unit)
  deallocate (rho, tt, r, z, dtau, ss, ii)
  call MPI_Finalize (ierr)
END PROGRAM
