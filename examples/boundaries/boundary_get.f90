!*******************************************************************************
! This version, which is the one most similar to the non-MPI version,  uses an
! extremly simple -- albeit somewhat wasteful --approach:! Using MPI_Get it
! fetches the entire array from which we need to get the ghost zone values.
! This is not as expensive as one might think, because communication ! is fast
! on modern computer networks.  The approach is suitable for "quick and dirty"
! solutions, which can then be improved.
!*******************************************************************************
SUBROUTINE boundary (f)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  integer :: idim, ix, iy, iz, lb(3), ub(3), offset(3)
  integer:: win, n
  !
  ! Allocate a receive buffer, and open a window into the f array
  !
  allocate (g(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub))
  n = product(m%ub-m%lb+1)
  call win_create_mpi (win, f, n)
  !
  do idim=1,3
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    if (m(idim)%n==1) cycle
    !
    ! Data has to be fetched a box length away
    !
    offset = 0
    offset(idim) = m(idim)%n
    !
    ! Lower boundary
    !
    lb = m%lb
    ub = m%ub
    ub(idim) = -1 ! only update boundary zones in lower part
    call win_fence_mpi (win)
    call get_reals_mpi (win, g, nw, 0, mpi_dn(idim))
    call win_fence_mpi (win)
    !$omp parallel do private(ix,iy,iz)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      f(ix,iy,iz) = g(ix + offset(1), iy + offset(2), iz + offset(3))
    enddo
    enddo
    enddo
    !
    ! Lower boundary
    !
    ...
  end do
  !
  ! Freee the window, for possible use on another array
  !
  call win_free_mpi (win)
  deallocate (g)
  !
END SUBROUTINE boundary
