!*******************************************************************************
PROGRAM advection_test
  USE mpi_base
  USE mpi_coords
  USE mpi_io_mod
  USE solver
  implicit none
  character(len=mch), save:: id= &
    'main.f90 $Id$'
  integer :: nstep, istep, fout, iout
  real    :: courant, umax
!-------------------------------------------------------------------------------
! Initialize MPI with a cartesian MPI arrangement
!-------------------------------------------------------------------------------
  call init_mpi                                                                 ! start mpi
  call print_id(id)
  mpi_dims(1) = 1                                                               ! one ranks in x
  mpi_dims(2) = mpi_size/mpi_dims(1)                                            ! the rest in y
  mpi_dims(3) = 1                                                               ! no split in z
  gn= (/ 64, 64, 1 /)                                                           ! global grid size
  s = (/ 64.,64.,1./)                                                           ! physical size
  u = (/ 1., .5, 0./)                                                           ! advection vel
  nstep = 1000                                                                   ! nr of time steps
  fout  = 50                                                                     ! output frequency
  iout  = 0                                                                     ! next snapshot

  call init_mesh                                                                ! setup mesh + init condition

  courant = 0.1
  umax = sqrt(sum(u**2))
  dt = courant * maxval(m%d) / umax                                             ! dt = C dx/u

  call file_openw_mpi ('snapshot.dat', gn, n, n_offset)                         ! open snapshot.dat
  call write_rho(iout)                                                          ! dump data

  do istep = 1, nstep
    call solve                                                                  ! take one timestep
    if (modulo(istep,fout)==0) call write_rho(iout)                             ! dump data
  enddo

  call file_close_mpi                                                           ! close snapshot file
  call end_mpi                                                                  ! end mpi
END PROGRAM
!*******************************************************************************
SUBROUTINE write_rho(iout)
  USE mpi_base
  USE mpi_io_mod
  USE mpi_reduce
  USE solver
  implicit none
  !
  integer :: iout
  real, allocatable, dimension(:,:,:) :: rho_interior
  real    :: mn,av,mx

  allocate(rho_interior(n(1),n(2),n(3)))
  rho_interior = rho(0:n(1)-1,0:n(2)-1,0:n(3)-1) 
  call file_write_mpi (rho_interior, n, iout)                                   ! collective write
  iout = iout + 1                                                               ! inc snapshot cnt

  mn = minval(rho_interior); call min_real_mpi(mn)
  av = sum(rho_interior)/product(gn); call sum_real_mpi(av)
  mx = maxval(rho_interior); call max_real_mpi(mx)
  if (master) print '(a,i5,3e12.4)', 'snap, min, aver, max :', iout, mn, av, mx
END SUBROUTINE write_rho
