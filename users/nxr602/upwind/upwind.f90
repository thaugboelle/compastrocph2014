MODULE upwind
  USE mesh,       only: m
  USE boundaries, only: boundary
  implicit none
CONTAINS

!*******************************************************************************
! Upwind divergence operator -- uses 3rd order upwind finite difference
!*******************************************************************************
FUNCTION div_upwind(vx,vy,vz,ux,uy,uz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: div_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx,vy,vz  ! Quantity to take upwind from
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ux,uy,uz  ! Signal velocity - selects forward/backward
                                                                                ! derivative according to sign of u[xyz]
  div_upwind = ddx_upwind(vx,ux) + ddy_upwind(vy,uy) + ddz_upwind(vz,uz)
  call boundary (div_upwind)
END FUNCTION div_upwind

!*******************************************************************************
FUNCTION ddx_upwind(vx,ux)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddx_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vx, ux
  integer :: idim, ix, iy, iz
  real    :: ab,bb,cb,db,eb
  real    :: af,bf,cf,df,ef
  ! Backwards derivative coefficients
  ab = 0.
  bb =  2. / (6.*m(1)%d)
  cb =  3. / (6.*m(1)%d)
  db = -6. / (6.*m(1)%d)
  eb =  1. / (6.*m(1)%d)
  ! Forward derivative coefficients
  af = -1. / (6.*m(1)%d)
  bf =  6. / (6.*m(1)%d)
  cf = -3. / (6.*m(1)%d)
  df = -2. / (6.*m(1)%d)
  ef = 0.
  !
  if (m(1)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    !$omp parallel do private(ix,iy,iz)
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      if (ux(ix,iy,iz) > 0) then  ! backward derivative
        ddx_upwind(ix,iy,iz) = bb*vx(ix+1,iy,iz) + cb*vx(ix  ,iy,iz) &
                             + db*vx(ix-1,iy,iz) + eb*vx(ix-2,iy,iz)          ! d(vx)/dx
      else                        ! forward derivative
        ddx_upwind(ix,iy,iz) = af*vx(ix+2,iy,iz) + bf*vx(ix+1,iy,iz) &
                             + cf*vx(ix  ,iy,iz) + df*vx(ix-1,iy,iz)          ! d(vx)/dx
      endif
    end do
    end do
    end do
  else
    ddx_upwind = 0.0
  endif
END FUNCTION ddx_upwind

!*******************************************************************************
FUNCTION ddy_upwind(vy,uy)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddy_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vy, uy
  integer :: idim, ix, iy, iz
  real    :: ab,bb,cb,db,eb
  real    :: af,bf,cf,df,ef
  ! Backwards derivative coefficients
  ab = 0.
  bb =  2. / (6.*m(2)%d)
  cb =  3. / (6.*m(2)%d)
  db = -6. / (6.*m(2)%d)
  eb =  1. / (6.*m(2)%d)
  ! Forward derivative coefficients
  af = -1. / (6.*m(2)%d)
  bf =  6. / (6.*m(2)%d)
  cf = -3. / (6.*m(2)%d)
  df = -2. / (6.*m(2)%d)
  ef = 0.
  !
  if (m(2)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    !$omp parallel do private(ix,iy,iz)
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      if (uy(ix,iy,iz) > 0) then ! backward derivative
        ddy_upwind(ix,iy,iz) = bb*vy(ix,iy+1,iz) + cb*vy(ix,iy  ,iz) &
                             + db*vy(ix,iy-1,iz) + eb*vy(ix,iy-2,iz)          ! d(vy)/dy
      else                        ! forward derivative
        ddy_upwind(ix,iy,iz) = af*vy(ix,iy+2,iz) + bf*vy(ix,iy+1,iz) &
                             + cf*vy(ix  ,iy,iz) + df*vy(ix,iy-1,iz)          ! d(vx)/dx
      endif
    end do
    end do
    end do
  else
    ddy_upwind = 0.0
  endif
END FUNCTION ddy_upwind

!*******************************************************************************
FUNCTION ddz_upwind(vz,uz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: ddz_upwind
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: vz, uz
  integer :: idim, ix, iy, iz
  real    :: ab,bb,cb,db,eb
  real    :: af,bf,cf,df,ef
  ! Backwards derivative coefficients
  ab = 0.
  bb =  2. / (6.*m(3)%d)
  cb =  3. / (6.*m(3)%d)
  db = -6. / (6.*m(3)%d)
  eb =  1. / (6.*m(3)%d)
  ! Forward derivative coefficients
  af = -1. / (6.*m(3)%d)
  bf =  6. / (6.*m(3)%d)
  cf = -3. / (6.*m(3)%d)
  df = -2. / (6.*m(3)%d)
  ef = 0.
  !
  if (m(3)%n > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    !$omp parallel do private(ix,iy,iz)
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      if (uz(ix,iy,iz) > 0) then  ! backward derivative
        ddz_upwind(ix,iy,iz) = bb*vz(ix,iy,iz+1) + cb*vz(ix,iy,iz  ) &
                             + db*vz(ix,iy,iz-1) + eb*vz(ix,iy,iz-2)          ! d(vz)/dz
      else                        ! forward derivative
        ddz_upwind(ix,iy,iz) = af*vz(ix,iy,iz+2) + bf*vz(ix,iy,iz+1) &
                             + cf*vz(ix  ,iy,iz) + df*vz(ix,iy,iz-1)          ! d(vx)/dx
      endif
    end do
    end do
    end do
  else
    ddz_upwind = 0.0
  endif
END FUNCTION ddz_upwind

!*******************************************************************************
END MODULE upwind
