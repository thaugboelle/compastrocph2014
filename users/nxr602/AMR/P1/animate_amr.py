""" Based on a Mayavi example
"""
from os import stat
from os.path import exists
from numpy import fromfile, float32, prod, sqrt, floor
from matplotlib.pyplot import imshow, show, figure, title
from matplotlib.animation import FuncAnimation

taskid=0
def filename(i):
    return 'id={:0=7d}_{:0=5d}.dat'.format(taskid,i)
frames=1
while (exists(filename(frames))):
    frames=frames+1
variables=8; bytes_per_word=4
n=int(sqrt(stat(filename(1)).st_size/(variables*bytes_per_word)))
shape=(n,n)
i=1
def read_Bz():
    global i
    fd=open(filename(i),'rb')
    title(i)
    i=i+1
    for j in range(7):
        Bz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    return(Bz)
fig=figure()
im=imshow(read_Bz(),origin='lower')
show()
def update_fig(*args):
    im.set_data(read_Bz())
    return im
a=FuncAnimation(fig, update_fig, frames=frames-1, interval=10, blit=False)