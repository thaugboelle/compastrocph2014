!*******************************************************************************
MODULE task_mod
  USE mhd_mod,     only: mhd_t, allocate_mhd
  USE mpi_rma
  USE io
  implicit none
PRIVATE
  logical:: first_time=.true.
  integer, dimension(:), pointer:: lag              ! nondimensional lag
!...............................................................................
  type:: task_t                                         ! task type
    integer:: id                                        ! task id
    integer:: upload = -1                               ! upload id
    integer:: it, nt = 3                                ! time index
    integer:: fout, iout                                ! output frequence
    integer:: istep=0, nstep=10                         ! max steps
    real(8):: tout, tnext=0d0, tend=1e30                ! output times
    real(16):: pos(3)                                   ! position
    real(8):: size                                      ! size
    real(8):: t, dt                                     ! model time
    integer:: level                                     ! AMR level
    logical:: is_head = .true.                          ! set for head
    type(mhd_t) , dimension(:), pointer:: f             ! model states
    type(mhd_t) , dimension(:), pointer:: d             ! model states
    type(task_t), pointer:: prev, next                  ! linked list
  end type
  type:: task_io_t                                      ! task type for I/O
    integer:: id                                        ! task id
    integer:: it, nt                                    ! time index
    integer:: fout, iout                                ! output frequence
    integer:: istep, nstep                              ! max steps
    real(8):: tout, tnext, tend                         ! output times
    real(16):: pos(3)                                   ! position
    real(8):: size                                      ! size
    real(8):: t, dt                                     ! model time
    integer:: level                                     ! AMR level
  end type
  integer:: mtask=10000                                 ! max number of patches
  integer:: ntask = 0                                   ! total number of patches
  type(task_t), dimension(:), pointer:: task_table
  integer:: id(1)=-1
!...............................................................................
PUBLIC:: task_t, allocate_task, deallocate_task, task_table, ntask, write_task, get_global_id
CONTAINS

!*******************************************************************************
SUBROUTINE write_task (unit, task)
  implicit none
  integer:: unit
  type(task_t):: task
  type(task_io_t):: io
!...............................................................................
  call trace_begin ('write_task')
  io%id   = task%id    ;        io%level = task%level
  io%it   = task%it    ;        io%nt    = task%nt
  io%fout = task%fout  ;        io%iout  = task%iout
  io%istep= task%istep ;        io%nstep = task%nstep
  io%tout = task%tout  ;        io%tend  = task%tend
  io%pos  = task%pos   ;        io%size  = task%size
  io%t    = task%t     ;        io%dt    = task%dt
  write (unit) io
  call trace_end
END SUBROUTINE write_task

!*******************************************************************************
SUBROUTINE allocate_task (task)
  type(task_t), pointer:: task
  type(mhd_t), dimension(:), pointer:: f, d
  integer:: it, nt
!...............................................................................
  call trace_begin ('allocate_task')
  if (first_time) then
    allocate (task_table(mtask))
    print'(a,i6,a,f6.1,a)', &
    ' task_table size:',size(task_table),' entries =',1e-6*sizeof(task_table),' MB'
  end if
  ntask = ntask+1
  if (ntask>mtask) then
     if (master) print*,'We need to reallocate and make the task_table larger'
     call exit
  end if
  task => task_table(ntask)                     ! next free slot
  task%id = get_global_id()                     ! get a globally unique ID
  allocate (task%f(task%nt),task%d(task%nt))    ! f and d hold state variables
  do it=1,task%nt                               ! for all time slot
    call allocate_mhd (task%f(it))              ! allocate MHD vars
    call allocate_mhd (task%d(it))              ! allocate time derivatives
  end do
  f => task%f
  d => task%d
  task%it = 1                                   ! time step
  task%level = 1                                ! default level
  f%it = task%it                                ! default
  f%t  = 0.0                                    ! default
  f%dt = 1.0                                    ! default
  task%f => f                                   ! pointer to the variables
  task%d => d                                   ! pointer to the time derivatives
  first_time = .false.                          ! now the dipatcher has a head
  call trace_end
END SUBROUTINE allocate_task

!*******************************************************************************
SUBROUTINE deallocate_task(task)
  type(task_t), pointer:: task
!...............................................................................
  task%it = -1
  deallocate (task%f)                   ! deallocate the MHD states
  deallocate (task%d)                   ! deallocate the time derivatives
  deallocate (task)                     ! deallocate the task
END SUBROUTINE deallocate_task

!*******************************************************************************
FUNCTION get_global_id() RESULT(id1)
   implicit none
   integer:: win, rank, offset, id1
!...............................................................................
   call trace_begin ('get_global_id')
   call win_create_int_mpi (win, id, 1)         ! create a one word window to id
   rank=0; offset=0                             ! master is the holder of rights
   call win_lock_x_mpi (win, rank)              ! lock exclusive
   call get_ints_mpi (win, id, 1, offset, rank) ! get the id from master
   id(1) = id(1)+1                              ! increment it
   id1 = id(1)
   call put_ints_mpi (win, id, 1, offset, rank) ! put it back
   call win_unlock_mpi (win, rank)              ! unlock
   call win_free_mpi (win)                      ! free the window
   if (do_trace.and.master) print*,'new unique ID:', id1
   call trace_end
END FUNCTION get_global_id

END MODULE task_mod
