MODULE refine_mod
  USE amr_mod
  USE mhd_mod
  USE io
real, private :: lim = 1.0
logical, private :: first_time = .true.
CONTAINS
!*******************************************************************************
! Run through and create sons if the refine flag is set
!*******************************************************************************
SUBROUTINE refine (a)
  implicit none
  type(amr_t):: a
  integer id, ns
!...............................................................................
  call trace_begin('refine')
  id = 0
  if (first_time) call init_refine
  first_time = .false.
  do while (id < a%namr)
    id = id+1
    call refine_current_sheets(a%patch(id))
    if (a%patch(id)%refine) then
      if (.not.a%patch(id)%refined) call make_sons (a, id)
      a%patch(id)%refine = .false.
    end if
  end do
  call trace_end
END SUBROUTINE refine
!******************************************************************************
SUBROUTINE init_refine
    implicit none
    namelist /refine_params/ lim
    rewind (input); read (input,refine_params); if (master) write(*,refine_params)    

END SUBROUTINE init_refine
!******************************************************************************
SUBROUTINE refine_current_sheets(patch)
    implicit none 
    type(mhd_t), pointer :: f
    type(patch_t):: patch
    integer :: it0
    integer :: nx, ny, nz
    integer :: ix = 0, iy = 0, iz = 0
    real :: gradx, grady, gradz 
    logical :: run = .TRUE.
    it0 = patch%task%it
    f => patch%task%f(it0)
    nx = f%m(1)%n
    ny = f%m(2)%n
    nz = f%m(3)%n
    do while (run .and. ix.le.nx-1)
        iy = 0
        do while (run .and. iy.le.ny-1)
            iz = 0
            do while (run .and. iz.le.nz-1)
                gradx = (f%d(ix+1,iy,iz) - f%d(ix-1,iy,iz)) / 2
                grady = (f%d(ix,iy+1,iz) - f%d(ix,iy-1,iz)) / 2
                gradz = (f%d(ix,iy,iz+1) - f%d(ix,iy,iz-1)) / 2
                if (sqrt(gradx**2 + grady**2 + gradz**2) .ge. lim) then
                    run = .FALSE.
                    patch%refine = .TRUE.
                end if
                iz = iz + 1 
            end do
        iy = iy + 1
        end do
    ix = ix + 1
    end do
END SUBROUTINE refine_current_sheets
END MODULE refine_mod
