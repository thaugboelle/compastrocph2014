""" Based on a Mayavi example
"""
import time
import numpy as np
from scipy import special
from numpy import fromfile, float32, prod, size

# Open a figure
fig = mlab.figure(1, size=(800,800), bgcolor=(1,1,1), fgcolor=(0,0,0))

# Read the data (you may want to have a for loop here)
n=64
shape=(n,n)
taskid=0
for i in range(200):
    filename='id={:0=7d}_{:0=5d}.dat'.format(taskid,i)
    print(filename)
    fd=open(filename,'rb')
    dd=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    ds=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Ux=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uy=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bx=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    By=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bz=transpose(Bz)
    if (i==1):
        s,=imshow(Bz)
    s.set_data(Bz)

