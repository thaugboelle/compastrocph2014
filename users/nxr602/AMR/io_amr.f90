!*******************************************************************************
MODULE io_amr
  !USE amr_mod
  USE mpi_base  , only: master
  USE mhd_mod   , only: mhd_t
  USE vector_mod, only: vector_t
  USE task_mod  , only: task_t, write_task
  USE mesh_mod  , only: m
  !USE io        , only: input, do_trace, mch, file_openw_mpi, file_write_mpi, wallclock
  USE io
  !USE timestep
  implicit none
  integer:: iout = -1
  integer:: data_unit=2, rec=1
CONTAINS

!*******************************************************************************
SUBROUTINE amr_io
END SUBROUTINE amr_io

!*******************************************************************************
SUBROUTINE write_amr (task, datadir)
  !USE poisson_mod, only : selfgravity
  USE pde_mod, only : gamma
  implicit none
  type(task_t), pointer:: task
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: pg
  character(len=mch):: filename, datadir
  logical:: exists
  integer:: it
!...............................................................................
  call trace_begin ('amr_io')
  !
  if (task%t > task%tnext) then
    task%tnext = task%tnext + task%tout
    if (master) print*,'write_amr because t > tnext for task ID', task%id
  else if (mod(task%istep,task%fout) == 0) then
    if (master) print*,'write_amr because mod(istep,fout)==0 for task ID', task%id
  else
    if (do_trace) print *,'write_amr: ID, times, steps', task%id, task%t, task%tnext, task%istep, task%fout
    return
  end if
  !
  !write(filename,'(a,"/id=",i7.7,"_",i5.5,".task")')trim(datadir),task%id,task%iout
  !open (unit=data_unit,file=trim(filename),form='unformatted',status='unknown')
  !
  !write (data_unit) sizeof(task)
  !call write_task (data_unit, task)
  !close (data_unit)
  !
  write(filename,'(a,"/id=",i7.7,"_",i5.5,".dat")')trim(datadir),task%id,task%iout
  open (unit=data_unit,file=trim(filename),form='unformatted',status='unknown',&
        access='direct',recl=product(m%n)*4)
  task%iout = task%iout + 1                                                     ! increment snapshot
  rec=1
  call write_field (task, task%f(task%it)%d  , 'd' ,rec); rec=rec+1
  call write_field (task, task%f(task%it)%s  , 's' ,rec); rec=rec+1
  call write_field (task, task%f(task%it)%p%x, 'px',rec); rec=rec+1
  call write_field (task, task%f(task%it)%p%y, 'py',rec); rec=rec+1
  call write_field (task, task%f(task%it)%p%z, 'pz',rec); rec=rec+1
  call write_field (task, task%f(task%it)%B%x, 'Bx',rec); rec=rec+1
  call write_field (task, task%f(task%it)%B%y, 'By',rec); rec=rec+1
  call write_field (task, task%f(task%it)%B%z, 'Bz',rec); rec=rec+1
  !if (selfgravity) &
  !  call write_field (task, task%p(task%it)%phi, 'phi',rec); rec=rec+1
  pg =  task%f(task%it)%d**gamma*exp(task%f(task%it)%s/task%f(task%it)%d*(gamma-1))
  call write_field (task, pg, 'Pgas',rec); rec=rec+1
  close (data_unit)
  call trace_end
END SUBROUTINE write_amr

!*******************************************************************************
SUBROUTINE write_field (task, f, c, rec)
  USE mpi_reduce                                                                ! max, min, average
  implicit none
  type(task_t), pointer:: task
  integer:: unit, rec
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: f
  character(len=*):: c
  real, pointer, dimension(:,:,:) :: interior                               ! without ghost zones
  real:: mn, av, mx
!...............................................................................
  allocate(interior(m(1)%n,m(2)%n,m(3)%n))                                      ! buffer for I/O
  interior = f(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                                ! skip ghost zones
  write (data_unit,rec=rec) interior
  mn = minval(interior);              call min_real_mpi(mn)                     ! global min
  av = sum(interior)/product(m%gn);   call sum_real_mpi(av)                     ! global average
  mx = maxval(interior);              call max_real_mpi(mx)                     ! global max
  if (master) print '(i5,3x,a,1p,e11.4,3x,a,3e11.3,2x,a)', &
    task%iout, 't =',task%t,' min, aver, max :', mn, av, mx, c                   ! printout
  deallocate (interior)
END SUBROUTINE write_field
END MODULE io_amr
