!*******************************************************************************
! The task is to copy, for all directions idim=1,3, the ghost zone data from
! the next and previous domains to this domain.  As is clear from the sketch
! of the index space below, this means copying the index ranges
!
!  (  0,   1)    from the domain above to our index range   ( n, n+1)
!  (n-2, n-1)    from the domain below to our index range   (-2,  -1)
!
!   rcv+    snd-                                             snd+    rcv-
!  >===>   <===<                                            >===>   <===<
!  +...+...|...+...+........................................+...+...|...+
! -2  -1   0   1   2                                       n-2 n-1  n  n+1
! lb                                                                    ub
!
! Here >==> shows the range that is sent up and <==< the range sent down, 
! with snd+/rcv+ and snd-/rcv- being the corresponding buffers.
!
! To be explicit, the exmple is drawn for nghost=2, but the code below is ok
! for any nghost.
! 
!*******************************************************************************
MODULE boundaries
USE mpi_send
USE mpi_coords
USE mesh
implicit none
CONTAINS

SUBROUTINE init_boundary
implicit none
END SUBROUTINE

SUBROUTINE boundary (f)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  real, dimension(:,:,:), allocatable :: snd, rcv
  integer :: idim, ix, iy, iz, lb(3), ub(3), req(2), np
  !
  do idim=1,3
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    if (m(idim)%n==1) cycle
    !
    ! Allocate arrays to hold copies of ghostzones
    lb = m%lb; ub = m%ub                                                        ! general grid boudaries
    lb(idim) = 0                                                                ! first index of snd-
    ub(idim) = lb(idim) + m(idim)%nghost-1                                      ! last index of snd-
    np = product(ub-lb+1)                                                       ! number of points in buffer

    allocate(snd(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)), &                        ! buffers for send and recv
             rcv(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)))                          ! this is a cheap operation!
    !
    ! Upper boundary: copy the snd- range into send buffer, send & receive,
    ! and copy the receive buffer into the rcv- range
    !
    snd = f(lb(1):ub(1), lb(2):ub(2), lb(3):ub(3))                              ! snd- part
    call irecv_reals_mpi(rcv, np, mpi_up(idim), mpi_comm_world, req(1))         ! receive from mpi_up(idim)
    call isend_reals_mpi(snd, np, mpi_dn(idim), mpi_comm_world, req(2))         ! send to mpi_dn(idim)
    call waitall_mpi (req,2)                                                    ! wait for send/recv to finish
    lb(idim) = m(idim)%n                                                        ! first index in rcv-
    ub(idim) = m(idim)%ub                                                       ! last index in rcv-
    f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)) = rcv                                ! copy into rcv-
    !
    ! Lower boundary: copy the snd+ range into send buffer, send & receive,
    ! and copy the receive buffer into the rcv+ range
    !
    
    deallocate(snd,rcv)                                                         ! free the buffers
    lb(idim) = m(idim)%n - m(idim)%nghost                                                               ! first index of snd-
    ub(idim) = m(idim)%n
    np = product(ub-lb+1)   
    allocate(snd(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)), &                        ! buffers for send and recv
             rcv(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)))                          ! this is a cheap operation!
    snd = f(lb(1):ub(1), lb(2):ub(2), lb(3):ub(3))                              ! snd- part

     call irecv_reals_mpi(rcv, np, mpi_dn(idim), mpi_comm_world, req(1))         ! receive from mpi_up(idim)
     call isend_reals_mpi(snd, np, mpi_up(idim), mpi_comm_world, req(2))         ! send to mpi_dn(idim)
     call waitall_mpi (req,2)! wait for send/recv to finish
       lb(idim) = m(idim)%lb
       ub(idim) = lb(idim) + m(idim)%nghost-1
       f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)) = rcv                                ! copy into rcv-
       deallocate(snd,rcv)                                                         ! free the buffers
  end do
  !
END SUBROUTINE boundary
END MODULE boundaries
