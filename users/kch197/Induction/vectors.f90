
!*******************************************************************************
! Vector routines
!*******************************************************************************
MODULE vectors
  USE mesh
  USE boundaries
  USE operators, only: ddx, ddy, ddz
PUBLIC
  type :: vector
    real, dimension(:,:,:), allocatable :: x, y, z
    integer, dimension(3) :: dim
  end type
CONTAINS

!*******************************************************************************
SUBROUTINE allocate_vector (a)
    implicit none
  type(vector):: a
!...............................................................................
  allocate (a%x(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub), &
            a%y(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub), &
            a%z(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub))
  a%dim(1) = m(1)%ub - m(1)%lb + 1
  a%dim(2) = m(2)%ub - m(2)%lb + 1
  a%dim(3) = m(3)%ub - m(3)%lb + 1
END SUBROUTINE allocate_vector

!*******************************************************************************
SUBROUTINE deallocate_vector (a)
  implicit none
  type(vector):: a
!...............................................................................
  deallocate (a%x, a%y, a%z)
END SUBROUTINE deallocate_vector

!*******************************************************************************
SUBROUTINE boundary_vector (a)
  implicit none
  type(vector):: a
!...............................................................................
  call boundary(a%x)
  call boundary(a%y)
  call boundary(a%z)
END SUBROUTINE boundary_vector

!*******************************************************************************
FUNCTION cross(a,b) RESULT(c)
  implicit none
  type(vector):: a, b, c
  integer iz
!...............................................................................
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    c%x(:,:,iz) = a%y(:,:,iz)*b%z(:,:,iz) - a%z(:,:,iz)*b%y(:,:,iz)
    c%y(:,:,iz) = a%z(:,:,iz)*b%x(:,:,iz) - a%x(:,:,iz)*b%z(:,:,iz)
    c%z(:,:,iz) = a%x(:,:,iz)*b%y(:,:,iz) - a%y(:,:,iz)*b%x(:,:,iz)
  end do
END FUNCTION cross

!*******************************************************************************
SUBROUTINE cross_sub(a,b,c)
  implicit none
  type(vector):: a, b, c
  integer iz
!...............................................................................
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    c%x(:,:,iz) = a%y(:,:,iz)*b%z(:,:,iz) - a%z(:,:,iz)*b%y(:,:,iz)
    c%y(:,:,iz) = a%z(:,:,iz)*b%x(:,:,iz) - a%x(:,:,iz)*b%z(:,:,iz)
    c%z(:,:,iz) = a%x(:,:,iz)*b%y(:,:,iz) - a%y(:,:,iz)*b%x(:,:,iz)
  end do
END SUBROUTINE cross_sub

!*******************************************************************************
FUNCTION dot(a,b) RESULT(c)
  implicit none
  type(vector):: a, b
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: c
!...............................................................................
  integer iz
!...............................................................................
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    c(:,:,iz) = a%x(:,:,iz)*b%x(:,:,iz) &
              + a%y(:,:,iz)*b%y(:,:,iz) &
              + a%z(:,:,iz)*b%z(:,:,iz)
  end do
END FUNCTION dot

!*******************************************************************************
SUBROUTINE dot_sub(a,b,c)
  implicit none
  type(vector):: a, b
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: c
!...............................................................................
  integer iz
!...............................................................................
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    c(:,:,iz) = a%x(:,:,iz)*b%x(:,:,iz) &
              + a%y(:,:,iz)*b%y(:,:,iz) &
              + a%z(:,:,iz)*b%z(:,:,iz)
  end do
END SUBROUTINE dot_sub

!*******************************************************************************
FUNCTION curl (a) RESULT(b)
  implicit none
  type(vector):: a, b
!...............................................................................
  b%x = ddy(a%z) - ddz(a%y)
  b%y = ddz(a%x) - ddx(a%z)
  b%z = ddx(a%y) - ddy(a%x)
  call boundary_vector(b)
END FUNCTION curl

!*******************************************************************************
SUBROUTINE curl_sub (a,b)
  implicit none
  type(vector):: a, b
!...............................................................................
  b%x = ddy(a%z) - ddz(a%y)
  b%y = ddz(a%x) - ddx(a%z)
  b%z = ddx(a%y) - ddy(a%x)
  call boundary_vector(b)
END SUBROUTINE curl_sub

!*******************************************************************************
FUNCTION div (a) RESULT(b)
  USE mesh
  implicit none
  type(vector):: a
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: b
!...............................................................................
  b = ddx(a%x) + ddy(a%y) + ddz(a%z)
  call boundary(b)
END FUNCTION div

!*******************************************************************************
SUBROUTINE div_sub (a,b)
  USE mesh
  implicit none
  type(vector):: a
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: b
!...............................................................................
  b = ddx(a%x) + ddy(a%y) + ddz(a%z)
  call boundary(b)
END SUBROUTINE div_sub

END MODULE vectors
