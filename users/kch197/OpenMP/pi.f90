PROGRAM	pi
implicit none

integer, parameter:: n_steps=100000000
real, parameter:: pi_ref=3.14159265359
integer:: i,n
real, dimension(n_steps):: a
real:: pi_calc_p, pi_calc





!$omp parallel

!$omp single
pi_calc_p=0.
!$omp end single
pi_calc=0.

!$omp do
do i=0,n_steps
	a(i)= ((-1.)**i)/(2.*i+1.)
	!print *, 'step no. ', i
	!print *, 'calculated pi = ', pi_cal(i)
	pi_calc= pi_calc+a(i)
end do
!$omp end do

!$omp critical
pi_calc_p=pi_calc_p+pi_calc
!$omp end critical

!$omp end parallel





print *, 'print final calculated pi = ', pi_calc_p*4.
print *, 'steps used: ', n_steps
print *, 'reference pi = ', pi_ref

END PROGRAM pi