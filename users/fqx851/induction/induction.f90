!*******************************************************************************
PROGRAM induction_test
  USE mesh                                                                      ! mesh module
  USE operators, only : div                                                                 ! div() operator
  USE boundaries                                                                ! ghost zone handling
  USE velocity_fields
  USE mpi_coords                                                                ! MPI dimensions
  USE mpi_reduce                                                                ! max, min, average
  USE io                                                                        ! input/output module
  USE vectors
  implicit none
  character(len=mch), save:: id= &
    'main.f90 $Id$'
  integer :: istep, iout                                                        ! step and i/o counters
  type (vector) :: BB, dBBdt, minusEE                                           ! magnetic field, derivative and electric field
  real, allocatable, dimension(:,:,:) :: interior                               ! without ghost zones
  real:: dt                                                                     ! time step
  integer:: nstep=100                                                           ! number of time steps
  integer:: fout                                                                ! output frequency
  integer:: ix, iy, iz                                                          ! loop indices
  real:: u(3) = (/1.0,0.5,0.0/)                                                 ! velocity components
  type (vector) :: UU
  real:: width=10.0                                                             ! Gaussian width
  real:: courant=0.3                                                            ! Courant number
  logical:: split_mpi=.true.                                                    ! overlap MPI & operator
  namelist /induction/ u, width, courant, nstep, fout, split_mpi                ! input namelist
  real(8):: t                                                                   ! model time
  real(8):: wc                                                                  ! wall clock
!-------------------------------------------------------------------------------
! Initialize mesh
!-------------------------------------------------------------------------------
  call init_mpi                                                                 ! start mpi
  call print_id(id)                                                             ! identify version
  call init_io                                                                  ! setup input/output
  call init_mesh                                                                ! setup mesh
  call init_boundary                                                            ! setup domain boundaries
!-------------------------------------------------------------------------------
! Setup initial conditions
!-------------------------------------------------------------------------------
  rewind (input); read (input,induction); if (master) write(*,induction)        ! read namelist input
  call allocate_vector(UU)
  call allocate_vector(minusEE)
  call allocate_vector(BB)
  call allocate_vector(dBBdt)
  call init_velocity(UU,u)
  do iz=0,m(3)%n-1; do iy=0,m(2)%n-1; do ix=0,m(1)%n-1                          ! loop over 3-D
    BB%x(ix,iy,iz) = 0.                                                         ! Gaussian, centered in (x,y)
    BB%y(ix,iy,iz) = 0.                                                         ! Gaussian, centered in (x,y)
    BB%z(ix,iy,iz) = exp(-(m(1)%r(ix)**2+m(2)%r(iy)**2)/width**2)               ! Gaussian, centered in (x,y)

  end do; end do; end do
  call boundary(BB%x)
  call boundary(BB%y)
  call boundary(BB%z)

!-------------------------------------------------------------------------------
! Setup output
!-------------------------------------------------------------------------------
  allocate(interior(m(1)%n,m(2)%n,m(3)%n))                                      ! buffer for I/O
  iout = 0                                                                      ! zero based snapshot
  call file_openw_mpi ('snapshot.dat', m%gn, m%n, m%offset)                     ! open data file
  t = 0d0                                                                       ! initialize time
  call write_vector(BB)                                                          ! dump data
!-------------------------------------------------------------------------------
! Evolve the solution in time
!-------------------------------------------------------------------------------
  dt = courant*minval(m%d)/sqrt(sum(u**2))                                      ! dt = C dx/u
  wc = wallclock()                                                              ! start timer
  do istep = 1, nstep                                                           ! time step loop
    call cross_sub(UU,BB,minusEE)
    dBBdt = curl(minusEE)
    BB%x = BB%x + dt*dBBdt%x
    BB%y = BB%y + dt*dBBdt%y
    BB%z = BB%z + dt*dBBdt%z
    call boundary(BB%x)
    call boundary(BB%y)
    call boundary(BB%z)
    t = t + dt                                                                  ! increment model time
    if (modulo(istep,fout)==0) then
      call write_vector(BB)                                                          ! dump data
    end if
  enddo
  call deallocate_vector(UU)
  call deallocate_vector(BB)
  call deallocate_vector(dBBdt)
  call deallocate_vector(minusEE)
  wc = wallclock()-wc                                                           ! read off timer
  if (master) print'(a,f6.1,a,f6.1)', &                                         ! to get accurate results ...
    'time:',wc,' s,  ns/p:',wc*1e9/(nstep*product(real(m%n)))                   ! ... use nout > nstep
!-------------------------------------------------------------------------------
! Clean up
!-------------------------------------------------------------------------------
  call file_close_mpi                                                           ! close snapshot file
  call end_mpi                                                                  ! end mpi
CONTAINS

SUBROUTINE write_vector(f)
  implicit none
  type(vector) :: f
  call write_field(f%x,'x')
  call write_field(f%y,'y')
  call write_field(f%z,'z')
END SUBROUTINE write_vector

!*******************************************************************************
SUBROUTINE write_field(f,l)
  implicit none
  real f(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub)
  real:: mn, av, mx
  character :: l                                             ! Label
!...............................................................................
  interior = f(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                                ! skip ghost zones
  call file_write_mpi (interior, m%n, iout)                                     ! collective write
  mn = minval(interior); call min_real_mpi(mn)                                  ! global min
  av = sum(interior)/product(m%gn); call sum_real_mpi(av)                       ! global average
  mx = maxval(interior); call max_real_mpi(mx)                                  ! global max
  if (master) print '(a,i5,3x,a,1p,e11.4,a,3e12.4)', &                            ! write one-liner
    l, iout, 't =', t, 'snap, min, aver, max :', mn, av, mx                        ! printout
  iout = iout + 1                                                               ! increment snapshot
END SUBROUTINE write_field
END PROGRAM
