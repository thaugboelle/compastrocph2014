!*******************************************************************************
!
! OpenMP parallel version of the pi program. 
!
! Compile with
!   ifort -openmp openmp2.f90 -o openmp2.x
!   gfortran -fopenmp openmp2.f90 -o openmp2.x
!
! Run with
!
!   export OMP_NUM_THREADS=$num_threads; ./openmp2.x      # with bash shell
!
! On my macbook pro (intel core i7) with 2 cores x 2 hardware threads:
! Error: 0.00013 %
! Speedup from 1 to 4 threads: 268 %
!
! Troels:openmp troels$ export num_threads=1
! Troels:openmp troels$ export OMP_NUM_THREADS=$num_threads; time ./pi_omp.x
!  Reference pi   3.14159274
!  Calculated pi   3.14159679
!
! real  0m6.539s
! user  0m6.531s
! sys 0m0.004s
!
! Troels:openmp troels$ export num_threads=4
! Troels:openmp troels$ export OMP_NUM_THREADS=$num_threads; time ./pi_omp.x
!  Reference pi   3.14159274
!  Calculated pi   3.14159679
!
! real  0m2.435s
! user  0m9.321s
! sys 0m0.005s

PROGRAM pi
  implicit none
  integer, parameter :: n = 100000000
  real, parameter :: ref_pi = 3.14159265359
  integer k
  real calc_pi

  !$omp parallel default(none) &
  !$omp   private(k) shared(calc_pi)

  !$omp single
  calc_pi=0.
  !$omp end single

  !$omp do reduction(+: calc_pi)
  do k=0,n
    calc_pi = calc_pi + (-1.0)**k/(2.0*k+1.0)
  end do
  !$omp enddo
  !$omp end parallel

  calc_pi = calc_pi*4.
  print*, "Reference pi", ref_pi
  print*, "Calculated pi", calc_pi
END PROGRAM
