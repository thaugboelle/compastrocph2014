from math import *
from scipy.integrate import odeint
from random import random as rand
import numpy as np
import sys
import matplotlib.pyplot as pyplot
import random

######CONSTANTS######
spy = 365e0*24e0*3600e0 #seconds per year
idx_Oj = 0 #index O+
idx_H2 = 1 #index H2
idx_OHj = 2 #index OH+
idx_H = 3 #index H
idx_H2Oj = 4 #index H2O+
idx_H3Oj = 5 #index H3O+
idx_E = 6 #index E
idx_H2O = 7 #index H2O
idx_OH = 8 #index OH
idx_O = 9 #index O
number_of_species = 10 #number of species
number_of_reactions = 9 #number of reactions
species_labels = ["O+","H2","OH+","H+","H2O+","H3O+","E","H2O","OH","O"]
reactions_labels = [	"O+ + H2 -> OH+ + H", \
											"OH+ + H2 -> H2O+ + H", \
											"H2O+ + H2 -> H3O+ + H", \
											"H3O+ + E -> H2O + H", \
											"H2O+ + E -> OH + H", \
											"H2O+ + E -> O + H2", \
											"OH+ + E -> O + H", \
											"O + CR -> O+ + E",
											"H2O + CR -> H + OH" ]

######FUNCTIONS START HERE######
#returns rate coefficients
def get_coefficients(Tgas):
	ionizing_cosmic_ray_flux = 1.3e-17 #1/s
	k = [0e0 for x in range(number_of_reactions)] #initialize coefficients
	invsqrt = 1e0/sqrt(Tgas/3e2)
	k[0] = 1.6e-9 #O+ + H2 -> OH+ + H
	k[1] = 1.1e-9 #OH+ + H2 -> H2O+ + H
	k[2] = 6.1e-10 #H2O+ + H2 -> H3O+ + H
	k[3] = 1.1e-7*invsqrt #H3O+ + E -> H2O + H
	k[4] = 8.6e-8*invsqrt #H2O+ + E -> OH + H
	k[5] = 3.9e-8*invsqrt #H2O+ + E -> O + H2
	k[6] = 6.3e-9*(Tgas/3e2)**(-.48) #OH+ + E -> O + H
	k[7] = 2.8e0*ionizing_cosmic_ray_flux #O + CR -> O+ + E
	k[8] = 9.7e2*ionizing_cosmic_ray_flux #H2O + CR -> H + OH

	return map(lambda x: x*random_error,k)

#returns reaction rates k*n*n or k*n (cm-3/s)
def get_reaction_rates(n):
	k = get_coefficients(Tgas)

	return [k[0]*n[idx_Oj]*n[idx_H2],\
	 k[1]*n[idx_OHj]*n[idx_H2],\
	 k[2]*n[idx_H2Oj]*n[idx_H2],\
	 k[3]*n[idx_H3Oj]*n[idx_E],\
	 k[4]*n[idx_H2Oj]*n[idx_E],\
	 k[5]*n[idx_H2Oj]*n[idx_E],\
	 k[6]*n[idx_OHj]*n[idx_E],\
	 k[7]*n[idx_O],\
	 k[8]*n[idx_H2O]]

#returns differential equations (dn/dt)
def differential_equations(n,t):

	rates = get_reaction_rates(n)

	#initialize ODEs
	dn = [0e0 for x in range(number_of_species)]
	
	#ODEs
	dn[idx_Oj] = \
		-rates[0] \
		+rates[7]

	dn[idx_H2] = \
		-rates[0] \
		-rates[1] \
		-rates[2] \
		+rates[5]

	dn[idx_OHj] = \
		+rates[0] \
		-rates[1] \
		-rates[6]

	dn[idx_H] = \
		+rates[0] \
		+rates[1] \
		+rates[2] \
		+rates[3] \
		+rates[4] \
		+rates[6] \
		+rates[8]

	dn[idx_H2Oj] = \
		+rates[1] \
		-rates[2] \
		-rates[4] \
		-rates[5]

	dn[idx_H3Oj] = \
		+rates[2] \
		-rates[3]

	dn[idx_E] = \
		-rates[3] \
		-rates[4] \
		-rates[5] \
		-rates[6] \
		+rates[7]

	dn[idx_H2O] = \
		+rates[3]   \
		-rates[8]

	dn[idx_OH] = \
		+rates[4]  \
		+rates[8]

	dn[idx_O] = \
		+rates[5] \
		+rates[6] \
		-rates[7]

	return dn

def plot_time_series(time_series, title, labels=species_labels, show=True, fig=None, ax=None):
	nlabels = len(labels)
	if(fig==None): fig = pyplot.figure()
	if(ax==None): ax = fig.add_subplot(1,1,1)
	handles = [pyplot.plot(time_ts/spy, time_series[:,i],label=labels[i])[0] for i in xrange(nlabels)]
	pyplot.legend(handles=handles)
	ax.set_xscale('log')
	ax.set_yscale('log')
	ax.set_title(title)
	if(show): pyplot.show()

######FUNCTIONS END HERE######


######MAIN STARTS HERE######

fig = pyplot.figure()
ax = fig.add_subplot(1,1,1)

for i in xrange(50):       # Error analysis with 50 runs
	random_error = random.random()+.5          #Random error of +/- 50%

	time = 0e0 #initial total time (s)
	time_step = spy #initial time-step (s)
	time_end = 1e6*spy #end of simulation (s)

	#default number densities (cm-3)
	number_density = [1e-40 for i in range(number_of_species)]
	total_hydrogen_nuclei = 1e8 #cm-3
	#define species number densities as fractions of total H nuclei
	number_density[idx_H2] = 1e-1*total_hydrogen_nuclei #cm-3
	number_density[idx_O] = 1e-4*total_hydrogen_nuclei #cm-3

	#gas temperature
	Tgas = 5e1 #K

	#Time series for the time
	time_ts = []

	#Abundances time series 
	abundances_ts = []

	#Reaction rates time series
	reaction_rates_ts = []

	#infinite loop (breaks when time_end reached)
	while(True):
		#compute electrons number density (sum over ions)
		number_density[idx_E] = number_density[idx_Oj] + number_density[idx_OHj] + number_density[idx_H2Oj] + number_density[idx_H3Oj]
		time_grid = np.linspace(0, time_step, 2) #prepare time grid (2 value = 1 step)
		results = odeint(differential_equations, number_density, time_grid, rtol=1e-6, atol=1e-20) #solve
		time += time_step #increase time (s)
		time_step = time_step * 1.1e0 #increase time-step (for output purposes)
		number_density = results[1][:] #results are new initial conditions (mind results structure!)
		time_ts.append(time)
		abundances_ts.append(number_density)
		reaction_rates_ts.append(get_reaction_rates(number_density))
		#break when reach the end time
		if(time>time_end): break

	time_ts = np.array(time_ts)
	abundances_ts = np.array(abundances_ts)
	reaction_rates_ts = np.array(reaction_rates_ts)
	#print abundances_ts
	print "done!"

	plot_time_series(abundances_ts, "Abundances w. coeff. error +/- 50%", show=False, fig=fig, ax=ax)
######MAIN ENDS HERE######
