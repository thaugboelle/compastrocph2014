import numpy as np
import matplotlib.pyplot as pyplot

data = np.fromfile("fout.dat", sep=' ')
data.shape = (121,2)
pyplot.xscale(u'log')
pyplot.yscale(u'log')
pyplot.title("Cooling controlled temperature evolution")
pyplot.xlabel("time/yr)")
pyplot.ylabel("Tgas/K")
pyplot.plot(data[:,0],data[:,1])
pyplot.show()