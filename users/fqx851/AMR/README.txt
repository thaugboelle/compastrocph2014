This README.txt is here mostly for planning purposes, while evolving the code
structure.  Later, we can turn it into part of the documentation.

Hierarchy:

main progam
  dispatch              dispatch.f90      dispatch module, sets up tasks
    [ task_t ]          task_mod.f90      task data type
    time_step           timestep.f90      time integration
      [ state_t ]       state_mod.f90     state data type (for interpolation in time)
      mhd               mhd.f90           MHD equations
      [ pde_t ]         pde_mod.f90       PDE data type (passable to methods)
        [ vector_t ]    array_mod.f90     3-D vector array
                        array_mod.f90     3-D scalar array (just arrays, not a derived type)

Note the naming convention, with _mod.f90 on files containing general data type
definitions and related methods.  None of these should depend on specific solvers,
which should instead use these methods, and only have a minimum of private 
mehods and derived data types.  On the other hand files containing routines 
that merely use the derived data types, and do not define general methods, do
not have a _mod ending.
                        
Time advance methods can be anything that can advance a state data type in time.  
For example a central difference FD method, such as the one we have used until 
now, or a FV method, such as the upwind method from the MHD week, or a Godunov 
method with a multitude of optional solver types (ala RAMSES).

A method has two parts (which in some methods are tightly linked): A timestep
part, and a spatial differencing part (FD, FV, Godunov, ...).  To implement a
RAMSES-like Godunov solver in this setup, it should simply present a callable
routine "time_step (state = [f,dfdt,t,dt])", which advances the solution f 
forward from time t to time t+dt.  The time step 'dt' is not imposed, but is 
a return value. Also, the time derivative is an optional return value, not an
input value.  If it exists it makes it possible to do spline interpolation in 
the time domain.

The argumen to "time_step" is a state data type (state_t), which is what the 
intepolation routines, which can interpolate in time and space, rely upon.

In addition, we need a 'patch' data type (patch_t), which is what we 'get' and
'put' to handle boundaries and upload/download (restriction/prolongation).  But
patches are just pieces of state structures, and have all the same properties
(including spatial extent and time extent), so it may be enough to have a flag
inside the data type.    

Data structure:
--------------------------------------------------------------------------------
type:: amr_t
! Holds globale info
  integer:: levelmin, levelmax      ! min and max refinement
  logical:: is_allocated
  integer:: ntime                   ! number of time steps to retain in PDE
  integer:: maxamr, namr            ! max and actual number of amr patches
  type(patch_t), dimension(:), allocatable:: patch    ! global need-to-know
end type

type:: patch
! Holds amr related info about patches; sync'ed on all ranks (72 bytes per patch)
  ..
  integer:: level = 1
  integer:: father = 1
  integer:: son(2,2,2) = 0
  integer:: neighbor(3,2) = 1
  real(kind=8):: size = 1.0
  real(kind=16):: pos(3) = 0.5
  ...
  type(task_t), pointer:: task
end type

type:: task
! Holds physical state of a patch, at several times (and patch positions)
  ...
  real(kind=8), dimension(:), allocatable:: t, dt            ! several times
  type(mhd_t) , dimension(:), allocatable, pointer:: f, d    ! several states
  type(task_t):: head, tail, next, prev                      ! links for list
end type

type:: mhd_t
! Holds the actual MHD variables, and their time derivatives, at one time
  ...
  real,           dimension(:,:,:), allocatable:: d, s       ! density, entropy
  type(vector_t), dimension(:,:,:), allocatable:: p, bb      ! momenta, B-field
  type(mesh_t):: m(3)                                        ! mesh
end type

type:: mesh_t
! Holds info about the mesh
  ...
  real(16):: pos                                             ! position
  real(8)::  size                                            ! local patch size
  integer::  n                                               ! local patch size
  real(8)::  d                                               ! cell size dr=s/n
  integer::  offset                                          ! integer offset = pos/d
  integer::  nghost                                          ! number of ghost zones
  integer::  lb, ub                                          ! lower & upper bundaries
  real, dimension(:), allocatable:: r                        ! cell centers
end type

The dispatcer selects a task from the task_list, assigns a new time slot, and 
calls the time_step method to advance it in time.
