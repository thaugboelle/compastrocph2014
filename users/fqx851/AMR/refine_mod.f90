MODULE refine_mod
  USE amr_mod
  USE io
  USE mhd_mod, only : mhd_t

  real:: limit_d = 1.0
CONTAINS

!*******************************************************************************
! Initialize refinement
!*******************************************************************************
SUBROUTINE init_refine
  implicit none
  namelist /refine_params/ limit_d
  rewind(input); read(input,refine_params); if(master) write(*,refine_params)
END SUBROUTINE init_refine

!*******************************************************************************
! Do all refinements on the grid
!*******************************************************************************
SUBROUTINE refinement
  implicit none
  logical, save :: first_run = .true.
  if(first_run) then
    call init_refine
    first_run = .false.
  end if
  call flag (amr)
  call refine (amr)
END SUBROUTINE refinement

!*******************************************************************************
! Flag cells for refinement according to refinement criteria
!*******************************************************************************
SUBROUTINE flag (a)
  implicit none
  type(amr_t):: a
  integer:: id, ns, nr
  type(task_t):: task
  type(mhd_t):: f
  call trace_begin('flag_refine')
  id = 0
  nr = 0
  do while (id < a%namr)
    id = id+1
    task = a%patch(id)%task
    f = task%f(task%it)
    a%patch(id)%refine = should_refine(f)
    if(a%patch(id)%refine) nr = nr + 1
  end do
  call trace_end
END SUBROUTINE flag

!*******************************************************************************
! Returns true iff f satisfies the refinement criteria
!*******************************************************************************
FUNCTION should_refine (f)
  implicit none
  logical:: should_refine
  type(mhd_t):: f
  integer:: dir
  real:: maxdiff, limit
  real:: s(f%m(1)%lb:f%m(1)%ub,f%m(2)%lb:f%m(2)%ub,f%m(3)%lb:f%m(3)%ub)
  maxdiff = 0.
  s = f%d                                    ! State variable to test
  limit = limit_d                            ! Limit that triggers refinement
  do dir=1,3
    maxdiff = max(maxdiff, maxval(log(2.*abs(s - cshift(s, 1, dir)))/log(s + cshift(s, 1, dir))))
  end do
  should_refine = (maxdiff > limit)
END FUNCTION should_refine

!*******************************************************************************
! Run through and create sons if the refine flag is set
!*******************************************************************************
SUBROUTINE refine (a)
  implicit none
  type(amr_t):: a
  integer:: id, ns, nr
!...............................................................................
  call trace_begin('refine')
  id = 0
  do while (id < a%namr)
    id = id+1
    if (a%patch(id)%refine) then
      if (.not.a%patch(id)%refined) call make_sons (a, id)
      a%patch(id)%refine = .false.
    end if
  end do
  call trace_end
END SUBROUTINE refine
!*******************************************************************************
END MODULE refine_mod
