!*******************************************************************************
! We arrive here with data structures in place, and with f a pointer to the
! MHD variables we should fill in
!*******************************************************************************
SUBROUTINE startup (task)
  USE state_mod,       only: state_t
  USE scalar_mod,      only: scalar_stats
  USE vector_mod,      only: vector_t, boundary_vector       ! vector operators
  USE velocity_fields, only: init_velocity                   ! initial velocities
  USE mesh_mod,        only: mesh_t
  USE task_mod,        only: task_t
  USE io
  implicit none
  type(task_t):: task
  type(state_t), pointer:: f                                 ! variables
  type(vector_t), pointer:: p
  character(len=mch), save:: id= &
    'amr_code.f90 $Id$'
  integer:: ix, iy, iz, iv, lb(3), ub(3)                     ! loop indices
  real:: width=10.0, density=2.0, entropy=-1.0               ! Gaussian, density, entropy
  real::  u(3) = (/1.0,0.5,0.0/)                             ! velocity components
  real:: pos(3)
  character(len=16):: type='induction'
  integer:: uploadid
  real:: sumf
  namelist /init_params/ width, pos, density, entropy, u, uploadid, type
!-------------------------------------------------------------------------------
! Initialize
!-------------------------------------------------------------------------------
  print*,'task%id',task%id
  call trace_begin ('startup')
  call print_id(id)
!-------------------------------------------------------------------------------
! Setup initial conditions
!-------------------------------------------------------------------------------
  rewind (input); read (input,init_params); if (master) write(*,init_params)
  call trace('1')
  print*,'task%id',task%id,task%m%lb,task%m%ub
  call init_velocity  (task%f%p, u-task%m%u, type, task%m, pos)
  task%f%d   = density                                    ! density
  task%f%s   = density*entropy                            ! entropy per unit vol
  task%f%p%x = density*task%f%p%x                         ! momentum
  task%f%p%y = density*task%f%p%y
  task%f%p%z = density*task%f%p%z
  call init_mfield (task%f%B, task%m, width, pos)
  do iv=1,task%nv
    call scalar_stats (task%m, task%mem(:,:,:,iv,task%it,1), 'f')
  end do
  call trace('3')
  task%t  = 0.0                                           ! model time
  task%dt = 1.0                                           ! model dt
  call trace('4')
  if (do_trace) print'(i8,a,3f10.6,2f12.6,i4)', &
    mpi_rank, ' made initial condition for patch at pos, size =', &
    task%m%p, task%m(1)%s, task%m(1)%r(task%m(1)%lb), task%m(1)%lb
  call trace_end
END SUBROUTINE startup

!*******************************************************************************
SUBROUTINE init_mfield (B, m, width, pos)
  USE vector_mod, only: vector_t
  USE mesh_mod,   only: mesh_t
  implicit none
  type(vector_t):: B
  type(mesh_t):: m(3)
  real:: width, pos(3)
  integer:: ix, iy,iz
!...............................................................................
  do iz=m(3)%lb,m(3)%ub; do iy=m(2)%lb,m(2)%ub; do ix=m(1)%lb,m(1)%ub
    B%x(ix,iy,iz) = 0.0
    B%y(ix,iy,iz) = 0.0
    B%z(ix,iy,iz) = exp(-((m(1)%r(ix)+m(1)%p-pos(1))**2 + &
                          (m(2)%r(iy)+m(1)%p-pos(2))**2)/width**2)  ! Gaussian Bz(x,y), centered
  end do; end do; end do
END SUBROUTINE init_mfield
