!*******************************************************************************
! DISPATCH main module, which maintains a number of tasks that this MPI-rank
! handles -- possibly using OpenMP to distribute over cores / threads.  When
! we arrive here the code has already initialized MPI, so we are one rank out
! of many (possibly millions).  However, we only need to stay in contact with
! essentially 7 other ranks; our 6 neighbors in (dir,pos) space (3x2), and our
! father, one AMR level above us (the convention in this code is that levels
! increase downwards, as in an ancestor tree, and as in the gravitational 
! potential around the stars and planets we simulate.
!
! Our tasks are to 1) allocate the AMR structure table, making it large enough
! to hold the conceivable number of patches we could imagine to need, and then
! 2) create the actual number of patches that we are to handle here, locally,
! based on the value of levelmin, then 3) to fill these patches with initial
! values, and then start to evolve them.
!*******************************************************************************
MODULE dispatch_mod
  USE mpi_reduce,   only: max_real8_mpi
  USE state_mod,    only: state_t, init_state
  USE timestep,     only: time_step
  USE amr_mod,      only: init_amr
  USE mesh_mod,     only: mesh_t
  USE io_amr,       only: write_amr
  USE patch_mod,    only: patch_t, read_patches, sync_patches
  USE task_mod,     only: task_t, task_table, ntask, init_task, &
                          allocate_task_table, allocate_task, init_states
  USE radiation_mod, only: init_radiation
  USE download_mod, only: download
  USE scalar_mod,   only: scalar_stats
  USE load_balance_mod
  USE io
  implicit none
PRIVATE
  logical:: alive=.true.
  real(8):: t_ref = 0.                              ! reference time (rabbit)
  integer, dimension(:), pointer:: lag              ! nondimensional lag
!...............................................................................
PUBLIC:: dispatch, alive
CONTAINS

!*******************************************************************************
SUBROUTINE dispatch
  implicit none
  type(patch_t), pointer:: patch
  type(task_t), pointer:: task
  integer:: patch_unit=3, iostat=0, ip, it
!...............................................................................
  call trace_begin ('dispatch')
  call init_io                                  ! setup I/O
  call init_radiation                           ! read radiation namelist
  call init_amr                                 ! allocate patch table etc
  call allocate_task_table                      ! allocate task structures
  call read_patches                             ! read patch definitions
  call init_load_balance                        ! assign to ranks
  !
  !$omp parallel                                ! Start of parallel region
  !$omp single                                  ! One thread takes ownership of while-loop
  do while (alive)                              ! call die() to stop
    call dispatcher
  end do
  !$omp end single
  !$omp end parallel
  call trace_end
END SUBROUTINE dispatch

!*******************************************************************************
SUBROUTINE dispatcher
  implicit none
  integer:: i, it, iv, loc(1), lvl
  real(8):: t, dt, lag, maxlag
  type(task_t), pointer:: task, choice
  character(len=16):: name='dispatcher: '
  integer:: verbose=0                                                           ! set to debug
!...............................................................................
  call trace_begin ('dispatcher')
  maxlag = -99.0
  do i=1,ntask
    task => task_table(i)
    it = task%it                                                                ! time index
    call init_states (task)                                                     ! setup task%f
    !call courant (task%m, task%f, task%dt(it))                                  ! compute estimate of dt
    if (it < 0) cycle                                                           ! skip dead entries
    lag = (t_ref-task%t(it)) !!!!!!/task%dt(it)                                          ! dimensionless lag
    if (verbose>1) print'(a," task",i5,"  time:slot,t,lag=",i3,f8.4,f6.2)', &
                          trim(name), task%id, task%it, task%t(it), lag
    if (lag > maxlag) then                                                      ! find the largest
      maxlag = lag
      choice => task                                                            ! save task pointer
    end if
  end do
  if (do_trace.or.verbose>0) print'(a,i8,a,i5,a,i5,1p,e12.5)', &
    trim(name), mpi_rank, ' is calling worker with task ID', choice%id, &
   ' at time', choice%it, choice%t(choice%it)
  !
  call trace ('call worker')
  !
  !$omp task
  do iv=1,choice%nv
    call scalar_stats (choice%m, choice%mem(:,:,:,iv,choice%it,1), 'f')
  end do
  call worker (choice)
  if (verbose>1.or.choice%id==1) print &
    '(a," task",i5," advanced slot",i3," to t =",f8.4," with dt=",f8.4)', &
        trim(name),choice%id, choice%it, choice%t(choice%it), choice%dt(choice%it)
  !$omp end task                                                                ! end of region for the task
  !
  call write_amr (choice)
  !
  call trace_end
END SUBROUTINE dispatcher

!*******************************************************************************
SUBROUTINE worker (tsk)
  implicit none
  integer:: it0, it1, level
  real(8):: t_new, t, dt
  type(task_t), pointer:: task, tsk
  real, dimension(:,:,:,:), pointer:: v, w
!...............................................................................
  call trace_begin ('worker')
  task => tsk                                   ! make private pointer copy
  !
  it0 = task%it                                 ! previous slot
  it1 = mod(it0,task%nt)+1                      ! new slot
  if (do_trace) print'(i8,a,2i3)', &
    mpi_rank,' is copying time slots:',it0,it1
  task%mem(:,:,:,:,it1,:) = &                   ! initialize this time slice ..
  task%mem(:,:,:,:,it0,:)                       ! .. from the previous one
  task%t (it1) = task%t (it0)
  task%dt(it1) = task%dt(it0)
  task%pos(:,it1) = task%pos(:,it0)             ! copy position
  task%it      = it1                            ! increment timestep
  !
  call init_states (task)                       ! setup task%f
  call time_step (task, it1)                    ! step from it0
  !
  t_ref = max(t_ref, task%t(it1))               ! update the reference time
  call max_real8_mpi (t_ref)                    ! global rabbit
  patch_table(task%id)%pos = task%pos             ! update patch position
  call sync_patches                             ! MPI sync of patch info
  !
  task%istep = task%istep+1
  if (task%istep  > task%nstep) call die('istep > nstep')
  if (task%t(it1) > task%tend ) call die('t > tend')
  call trace_end
END SUBROUTINE worker

!*******************************************************************************
END MODULE dispatch_mod

!*******************************************************************************
! Call this routine from anywhere, to cause the dispatcher to stop
!*******************************************************************************
SUBROUTINE die (reason)
  USE dispatch_mod
  USE io
  implicit none
  character(len=*):: reason
!...............................................................................
  if (master) print'(a)','stopping because '//trim(reason)
  alive = .false.
END SUBROUTINE die
