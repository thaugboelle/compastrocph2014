!*******************************************************************************
MODULE task_mod
  USE state_mod, only: state_t, init_state
  USE mesh_mod,  only: mesh_t, init_mesh
  USE mpi_rma
  USE io
  USE radiation_mod, only: nwavelengths
  implicit none
PRIVATE
  integer, parameter:: ntime=3
  integer, dimension(:), pointer:: lag                  ! nondimensional lag
!...............................................................................
  type:: task_t                                         ! task type
    integer:: id                                        ! task id
    integer:: upload = -1                               ! upload id
    integer:: rank                                      ! MPI rank
    integer:: n(3)                                      ! task dimensions
    integer:: it=1, nt=3, nv=8                          ! time index and sizes
    integer:: fout, iout                                ! output frequence
    integer:: istep=1, nstep=10                         ! max steps
    real(8):: tout, tnext=0d0, tend=1e30                ! output times
    real(16):: pos(3,ntime) = 0.0_16                    ! position
    real(8):: size                                      ! size
    real(8):: t(ntime), dt(ntime), pt                   ! model time
    real(4):: u(3)                                      ! bulk speed
    type(mesh_t):: m(3)                                 ! meshes  
    integer:: level = 0                                 ! AMR level
    real:: quality                                      ! comprehensive quality
    logical:: is_head = .true.                          ! set for head
    integer:: nmem                                      ! number of words in mem
    real, dimension(:,:,:,:,:,:), pointer:: mem         ! contiguous memory
    type(state_t):: f, d                                ! state structures
    type(task_t), pointer:: prev, next                  ! linked list
    logical:: is_allocated = .false.                    ! has been allocated
    logical:: refine = .false.                          ! flag to trigger refine
    logical:: passive = .false.                         ! marks passive copy
  end type
  type:: task_io_t                                      ! task type for I/O
    integer:: id                                        ! task id
    integer:: it, nt                                    ! time index
    integer:: fout, iout                                ! output frequence
    integer:: istep, nstep                              ! max steps
    real(8):: tout, tnext, tend                         ! output times
    real(16):: pos(3,ntime)                             ! position
    real(8):: size                                      ! size
    real(8):: t(ntime), dt(ntime)                       ! model time
    integer:: level                                     ! AMR level
  end type
  integer:: mtask=10000                                 ! max number of tasks
  integer:: ntask = 0                                   ! actual number of tasks
  type(task_t), dimension(:), pointer:: task_table      ! table of local tasks
  integer:: id(1)=-1
  character(len=mch):: datadir='.'
!...............................................................................
PUBLIC:: task_t, allocate_task_table, allocate_task, deallocate_task, task_table, &
         ntask, write_task, get_global_id, init_task, datadir, init_states
CONTAINS

!*******************************************************************************
SUBROUTINE write_task (unit, task)
  implicit none
  integer:: unit
  type(task_t):: task
  type(task_io_t):: io
!...............................................................................
  call trace_begin ('write_task')
  io%id   = task%id    ;        io%level = task%level
  io%it   = task%it    ;        io%nt    = task%nt
  io%fout = task%fout  ;        io%iout  = task%iout
  io%istep= task%istep ;        io%nstep = task%nstep
  io%tout = task%tout  ;        io%tend  = task%tend
  io%pos  = task%pos   ;        io%size  = task%size
  io%t    = task%t     ;        io%dt    = task%dt
  write (unit) io
  call trace_end
END SUBROUTINE write_task

!*******************************************************************************
SUBROUTINE allocate_task_table
  implicit none
  allocate (task_table(mtask))
  print'(a,i6,a,f6.1,a)', &
    ' task_table size:',size(task_table),' entries =',1e-6*sizeof(task_table),' MB'
END SUBROUTINE allocate_task_table

!*******************************************************************************
SUBROUTINE allocate_task (task)
  type(task_t):: task
  integer:: n(3)
  integer:: it, nt, nv, l(3), u(3)
  real, dimension(:,:,:,:), pointer:: p
!...............................................................................
  call trace_begin ('allocate_task')
  print *,'ntask',ntask
  nv = task%nv                                  ! number of variables
  nt = task%nt                                  ! number of time slices
  l  = task%m%lb
  u  = task%m%ub
  it = 1
  allocate (task%mem(l(1):u(1), &
                     l(2):u(2), &
                     l(3):u(3),nv,nt,2))        ! allocate contiguous memory
  task%mem(1,1,1,1,1,1) = 127. + task%id
  task%nmem = product(u-l+1)                    ! number of words in task%mem
  if (do_trace) print'(a,2(2x,3i5),i9)', &
    ' allocate_task:',l,u,task%nmem
  !
  task%it = it                                  ! time step
  task%quality = task%level                     ! to become more comprehensive                            
  task%t = 0.0                                  ! task times
  task%dt = 1.0                                 ! default step
  task%is_allocated = .true.                    ! allocation flag
  call init_mesh (task%m)                       ! initialize mesh
  call init_states (task)
  call trace_end
END SUBROUTINE allocate_task

!*******************************************************************************
SUBROUTINE init_states (task)
  implicit none
  type(task_t):: task
  real, dimension(:,:,:,:), pointer:: v, w
!...............................................................................
  v => task%mem(:,:,:,:,task%it,1)              ! 4-D state array
  w => task%mem(:,:,:,:,task%it,2)              ! 4-D state array
  call init_state (v, task%f)                   ! initialize state structure
  call init_state (w, task%d)                   ! initialize state structure
END SUBROUTINE init_states

!*******************************************************************************
SUBROUTINE init_task (task)
  implicit none
  type(task_t):: task
  real:: tout=1e30, tend=1e30
  integer:: nstep=10, fout=10
  namelist /run_params/ nstep, fout, tout, tend, datadir
!...............................................................................
  rewind(input); read(input,run_params); if(master)write(*,run_params)          ! run control
  !
  task%nv = 8 + nwavelengths
  call allocate_task (task)                                                     ! main tasks
  task%tout=tout; task%fout=fout; task%nstep=nstep; task%tend=tend              ! run params
  call startup (task)                                                           ! initialize with ICs
  do while (task%tnext < task%t(task%it))                                       ! next output in the past?
    task%tnext = task%tnext + task%tout                                         ! fast forward
    print*,'tnext, tout', task%tout, task%tnext
  end do
END SUBROUTINE init_task

!*******************************************************************************
SUBROUTINE deallocate_task(task)
  type(task_t), pointer:: task
!...............................................................................
  task%it = -1
  deallocate (task%mem)                         ! deallocate the task memory
  task%is_allocated = .false.                   ! allocation flag
END SUBROUTINE deallocate_task

!*******************************************************************************
FUNCTION get_global_id() RESULT(id1)
   implicit none
   integer:: win, rank, offset, id1
!...............................................................................
   call trace_begin ('get_global_id')
   call win_create_int_mpi (win, id, 1)         ! create a one word window to id
   rank=0; offset=0                             ! master is the holder of rights
   call win_lock_x_mpi (win, rank)              ! lock exclusive
   call get_ints_mpi (win, id, 1, offset, rank) ! get the id from master
   id(1) = id(1)+1                              ! increment it
   id1 = id(1)
   call put_ints_mpi (win, id, 1, offset, rank) ! put it back
   call win_unlock_mpi (win, rank)              ! unlock
   call win_free_mpi (win)                      ! free the window
   if (do_trace) print*,'new unique ID:', id1
   call trace_end
END FUNCTION get_global_id

END MODULE task_mod
