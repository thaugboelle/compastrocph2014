!*******************************************************************************
MODULE interpol_mod
  USE mesh_mod,   only: mesh_t, m
  USE vector_mod, only: vector_t
  USE io
  implicit none
PRIVATE

PUBLIC:: prolong_scalar, restrict_scalar, prolong_vector, restrict_vector, interpolate_scalar, interpolate_vector
CONTAINS

!*******************************************************************************
! Interpolate to new position with offset measured in grid units
!*******************************************************************************
SUBROUTINE interpolate_scalar (a, a2, px, py, pz)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: a
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: a2
  integer:: ix,iy,iz,ix1,iy1,iz1
  real:: px, py, pz, qx, qy, qz
!...............................................................................
  call trace_begin ('interpolate_scalar')
  qx=1.0-px
  qy=1.0-py
  qz=1.0-pz
  !$omp parallel do default(shared) private(ix,iy,iz,ix1,iy1,iz1)
  do iz=0,m(3)%n-1
    iz1 = min(iz+1,m(3)%n-1)
    do iy=0,m(2)%n-1
      iy1 = min(iy+1,m(2)%n-1)
      do ix=0,m(1)%n-1
        ix1 = min(ix+1,m(1)%n-1)
        a2(ix,iy,iz) = qz*(qy*(qx*a(ix  ,iy ,iz   ) + px*a(ix1,iy ,iz ))  + &
                     +     py*(qx*a(ix  ,iy1,iz   ) + px*a(ix1,iy1,iz ))) + &
                     + pz*(qy*(qx*a(ix  ,iy1,iz1  ) + px*a(ix1,iy1,iz1))  + &
                     +     py*(qx*a(ix  ,iy1,iz   ) + px*a(ix1,iy1,iz )))
      end do
    end do
  end do
  call trace_end
END SUBROUTINE interpolate_scalar
!*******************************************************************************
SUBROUTINE interpolate_vector (v, v2, px, py, pz)
  implicit none
  type(vector_t):: v, v2
  real:: px, py, pz
!...............................................................................
  call trace_begin ('interpolate_vector')
  call interpolate_scalar (v%x, v2%x, px, py, pz)
  call interpolate_scalar (v%y, v2%y, px, py, pz)
  call interpolate_scalar (v%z, v2%z, px, py, pz)
  call trace_end
END SUBROUTINE interpolate_vector

!*******************************************************************************
! Shift to new position with offset measured in grid units
!*******************************************************************************
SUBROUTINE shift_scalar (a1, a2, shift, m)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: a1
  real, dimension(m(1)%l2:m(1)%u2, m(2)%l2:m(2)%u2, m(3)%l2:m(3)%u2):: a2
  integer:: ix, iy,iz, shift(3)
!...............................................................................
  call trace_begin ('shift_scalar')
  !$omp parallel do default(shared) private(ix,iy,iz)
  do iz=0,m(3)%n
    do iy=0,m(2)%n
      do ix=0,m(1)%n
        a2(ix,iy,iz) = a1(modulo(ix+shift(1),m(1)%n), &
                          modulo(iy+shift(2),m(2)%n), &
                          modulo(iz+shift(3),m(3)%n))
      end do
    end do
  end do
  call trace_end
END SUBROUTINE shift_scalar
!*******************************************************************************
SUBROUTINE shift_vector (v, v2, shift, m)
  implicit none
  type(mesh_t):: m(3)
  type(vector_t):: v, v2
  integer:: shift(3)
!...............................................................................
  call trace_begin ('shift_vector')
  call shift_scalar (v%x, v2%x, shift, m)
  call shift_scalar (v%y, v2%y, shift, m)
  call shift_scalar (v%z, v2%z, shift, m)
  call trace_end
END SUBROUTINE shift_vector

!*******************************************************************************
! Make a double resolution array
!*******************************************************************************
SUBROUTINE prolong_scalar (a, a2, m)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: a
  real, dimension(m(1)%l2:m(1)%u2, m(2)%l2:m(2)%u2, m(3)%l2:m(3)%u2):: a2
  integer:: ix,iy,iz,jx,jy,jz
  real:: px,py,pz,qx,qy,qz
!...............................................................................
  call trace_begin ('prolong_scalar')
  !$omp parallel do default(private) shared(a,a2)
  do iz=0,m(3)%n
   do jz=0,1
    px = 0.5*jx; qx=1.0-px
    do jy=0,1
     py = 0.5*jy; qy=1.0-py
     do jx=0,1
      pz = 0.5*jz; qz=1.0-pz
      do iy=0,m(2)%n
       do ix=0,m(1)%n
         a2(ix*2+jx,iy*2+jy,iz*2+jz) = qz*(qy*(qx*a(ix  ,iy  ,iz  ) + px*a(ix+1,iy  ,iz  ))   &
                                     +     py*(qx*a(ix  ,iy+1,iz  ) + px*a(ix+1,iy+1,iz  )))  &
                                     + pz*(qy*(qx*a(ix  ,iy+1,iz+1) + px*a(ix+1,iy+1,iz+1))   &
                                     +     py*(qx*a(ix  ,iy+1,iz  ) + px*a(ix+1,iy+1,iz  )))
       end do
      end do
     end do
    end do
   end do
  end do
  call trace_end
END SUBROUTINE prolong_scalar

!*******************************************************************************
SUBROUTINE prolong_vector (v, v2, m)
  implicit none
  type(vector_t):: v, v2
  type(mesh_t):: m(3)
!...............................................................................
  call trace_begin ('prolong_vector')
  call prolong_scalar (v%x, v2%x, m)
  call prolong_scalar (v%y, v2%y, m)
  call prolong_scalar (v%z, v2%z, m)
  call trace_end
END SUBROUTINE prolong_vector

!*******************************************************************************
! Make a half resolution array
!*******************************************************************************
SUBROUTINE restrict_scalar (a, a1, m)
  implicit none
  type(mesh_t):: m(3)
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub):: a
  real, dimension(m(1)%l1:m(1)%u1, m(2)%l1:m(2)%u1, m(3)%l1:m(3)%u1):: a1
  integer:: ix,iy,iz
!...............................................................................
  call trace_begin ('restrict_scalar')
  !$omp parallel do default(private) shared(a,a1)
  do iz=0,m(3)%n/2
    do iy=0,m(2)%n/2
       do ix=0,m(1)%n/2
         a1(ix,iy,iz) = a(ix*2,iy*2,iz*2)
       end do
     end do
  end do
END SUBROUTINE restrict_scalar
!*******************************************************************************
SUBROUTINE restrict_vector (v, v2, m)
  implicit none
  type(vector_t):: v, v2
  type(mesh_t):: m(3)
!...............................................................................
  call trace_begin ('restrict_vector')
  call restrict_scalar (v%x, v2%x, m)
  call restrict_scalar (v%y, v2%y, m)
  call restrict_scalar (v%z, v2%z, m)
  call trace_end
END SUBROUTINE restrict_vector

!*******************************************************************************
END MODULE interpol_mod
