!*******************************************************************************
! The task is to copy, for all directions idim=1,3, the ghost zone data from
! the next and previous domains to the domain.  As is clear from the sketch of
! the index space below, this means copying the index ranges
!
!  (  0,   1)    from the domain above to our index range   ( n, n+1)
!  (n-2, n-1)    from the domain below to our index range   (-2,  -1)
!
!   rcv+    snd-                                             snd+    rcv-
!  >===>   <===<                                            >===>   <===<
!  +...+...|...+...+........................................+...+...|...+
! -2  -1   0   1   2                                       n-2 n-1  n  n+1
! lb                                                                    ub
!
! Here >==> shows the range that is sent up and <==< the range sent down, 
! with snd+/rcv+ and snd-/rcv- being the corresponding buffers.
!
! To be explicit, the exmple is drawn for nghost=2, but the code below is ok
! for any nghost.
! 
!*******************************************************************************
MODULE boundaries
  USE mesh_mod
  implicit none
  real, dimension(:,:,:), allocatable, private:: snd1, rcv1, snd2, rcv2
  integer, private:: req(4), lb(3), ub(3), np
  integer, private:: type=0
PUBLIC 
CONTAINS

!*******************************************************************************
SUBROUTINE init_boundary
  implicit none
  namelist /boundary_params/ type
  rewind(input); read(input,boundary_params); if (master) write(*,boundary_params)
END SUBROUTINE init_boundary

!*******************************************************************************
SUBROUTINE boundary (f)
  USE mpi_base
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  integer :: idim
  real(8) :: wc
  !
  wc = wallclock()
  if (type==1) then
    call boundary_get (f)
  else if (type==2) then
    call boundary_get_opt (f)
  else if (type==3) then
    call boundary_send (f)
  else if (type==4) then
    call boundary_allocate (f)
  else
    do idim=1,3
      call boundary_begin (f, idim)
      call boundary_end (f, idim)
    end do
  end if
  !if (master) print'(a,1p,i3,e13.6)','boundary:',type,wallclock()-wc
END SUBROUTINE boundary

!*******************************************************************************
! This part begins the send and receive operation, but is non-blocking, so 
! we can do part of the computations at the same time as the communicatoin is
! ongoing
!*******************************************************************************
SUBROUTINE boundary_begin (f, idim)
  USE mpi_send
  USE mpi_coords
  implicit none
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: f
  integer :: idim
  !---------------------------------------------------------------------------
  ! If only a single mesh point in this direction it is passive; return
  !---------------------------------------------------------------------------
  if (m(idim)%n==1) return
  !---------------------------------------------------------------------------
  ! Allocate arrays to hold copies of ghostzones
  !---------------------------------------------------------------------------
  lb = m%lb; ub = m%ub                                                        ! general grid boudaries
  lb(idim) = 0                                                                ! first index of snd-
  ub(idim) = lb(idim) + m(idim)%nghost-1                                      ! last index of snd-
  np = product(ub-lb+1)                                                       ! number of points in buffer
  allocate(snd1(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)), &                       ! buffers for send and recv
           rcv1(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)), &                       ! this is a cheap operation!
           snd2(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)), &
           rcv2(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)))
  !---------------------------------------------------------------------------
  ! Upper boundary: copy the snd- range into send buffer, start send & receive 
  !---------------------------------------------------------------------------
  snd1 = f(lb(1):ub(1), lb(2):ub(2), lb(3):ub(3))                             ! snd- part
  call irecv_reals_mpi (rcv1, np, mpi_up(idim), mpi_comm_world, req(1))       ! receive from mpi_up(idim)
  call issend_reals_mpi (snd1, np, mpi_dn(idim), mpi_comm_world, req(2))       ! send to mpi_dn(idim)
  !---------------------------------------------------------------------------
  ! Lower boundary: copy the snd+ range into send buffer, start send & receive
  !---------------------------------------------------------------------------
  lb(idim) = m(idim)%n - m(idim)%nghost                                       ! first index in snd+
  ub(idim) = m(idim)%n - 1                                                    ! last index in snd+
  snd2 = f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3))                               ! zone to be send to lower
  call irecv_reals_mpi (rcv2, np, mpi_dn(idim), mpi_comm_world, req(3))       ! receive up
  call issend_reals_mpi (snd2, np, mpi_up(idim), mpi_comm_world, req(4))       ! send down
  !
END SUBROUTINE boundary_begin

!*******************************************************************************
! This part finishes the send / receive operation
!*******************************************************************************
SUBROUTINE boundary_end (f, idim)
  USE mpi_send
  USE mpi_coords
  implicit none
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: f
  integer :: idim
  !---------------------------------------------------------------------------
  if (m(idim)%n==1) return
  !---------------------------------------------------------------------------
  ! Upper boundary: copy the receive buffer into the rcv- range
  !---------------------------------------------------------------------------
  call waitall_mpi (req,4)                                                    ! wait for send/recv to finish
  lb(idim) = m(idim)%n                                                        ! first index in rcv-
  ub(idim) = m(idim)%ub                                                       ! last index in rcv-
  f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)) = rcv1                               ! copy into rcv-
  !---------------------------------------------------------------------------
  ! Lower boundary: copy the receive buffer into the rcv+ range
  !---------------------------------------------------------------------------
  lb(idim) = -m(idim)%nghost                                                  ! lower index of rcv+
  ub(idim) = -1                                                               ! upper index of rcv+
  f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)) = rcv2                               ! copy into rcv+
  !
  deallocate(snd1,rcv1,snd2,rcv2)                                             ! free the buffers
  !
END SUBROUTINE boundary_end

!*******************************************************************************
! Boundary conditions -- assumed periodic
!*******************************************************************************
SUBROUTINE boundary_get (f)
  USE mpi_rma
  USE mpi_coords
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  real, dimension(:,:,:), allocatable :: g
  integer :: idim, ix, iy, iz, lb(3), ub(3), offset(3)
  integer:: win, nw
  !
  if (.not. allocated(g)) &
    allocate (g(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub))
  nw = product(m%ub-m%lb+1)
  call win_create_mpi (win, f, nw)
  do idim=1,3
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    if (m(idim)%n==1) cycle

    ! Wrapping offset for periodic boundaries
    ! Data has to be fetched a box length away
    offset = 0
    offset(idim) = m(idim)%n
    !
    ! Lower boundary
    !
    lb = m%lb
    ub = m%ub
    ub(idim) = -1 ! only update boundary zones in lower part
    call win_fence_mpi (win)
    call get_reals_mpi (win, g, nw, 0, mpi_dn(idim))
    call win_fence_mpi (win)
    !$omp parallel do private(ix,iy,iz)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      f(ix,iy,iz) = g(ix + offset(1), iy + offset(2), iz + offset(3))
    enddo
    enddo
    enddo
    !
    ! Upper boundary
    !
    lb = m%lb
    ub = m%ub
    lb(idim) = m(idim)%n ! only update boundary zones in upper part
    call win_fence_mpi (win)
    call get_reals_mpi (win, g, nw, 0, mpi_up(idim))
    call win_fence_mpi (win)
    !$omp parallel do private(ix,iy,iz)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      f(ix,iy,iz) = g(ix - offset(1), iy - offset(2), iz - offset(3))
    enddo
    enddo
    enddo
  end do
  call win_free_mpi (win)
  !
END SUBROUTINE boundary_get

!*******************************************************************************
! This version uses an optimized approach based on MPI_Isend/Irecv.  A loop as
! the one filling the boundary zones is first used to store the values that
! we need to send into a snd buffer, where they can be picked up with MPI_Irecv
! Correspondingly, we get that buffer int rcv, and unpack it in the original loop.
!*******************************************************************************
SUBROUTINE boundary_send (f)
  USE mpi_send
  USE mpi_coords
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  real, dimension(:), allocatable :: snd, rcv
  integer :: idim, ix, iy, iz, lb(3), ub(3), offset(3), n, nx, nxy
  !
  ! Allocate large enough buffers
  n = max((m(1)%ub-m(1)%lb+1)*(m(2)%ub-m(2)%lb+1)*m(3)%nghost, &
          (m(2)%ub-m(2)%lb+1)*(m(3)%ub-m(3)%lb+1)*m(1)%nghost, &
          (m(3)%ub-m(3)%lb+1)*(m(1)%ub-m(1)%lb+1)*m(2)%nghost)
  allocate (snd(n), rcv(n))

  do idim=1,3
    !
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    !
    if (m(idim)%n==1) cycle
    !
    ! Data has to be fetched a box length away in the idim direction
    !
    offset = 0
    offset(idim) = m(idim)%n
    !
    ! Lower boundary
    !
    lb = m%lb
    ub = m%ub
    ub(idim) = -1 ! only update boundary zones in lower part
    nx = ub(1)-lb(1)+1
    nxy = nx*(ub(2)-lb(2)+1)
    !$omp parallel do private(ix,iy,iz,n)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      snd(n) = f(ix+offset(1), iy+offset(2), iz+offset(3))
    enddo
    enddo
    enddo
    call irecv_reals_mpi (rcv, n, mpi_dn(idim), mpi_comm_world, req(1))
    call isend_reals_mpi (snd, n, mpi_up(idim), mpi_comm_world, req(2))
    call waitall_mpi (req, 2)
    !$omp parallel do private(ix,iy,iz,n)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      f(ix,iy,iz) = rcv(n)
    enddo
    enddo
    enddo
    !
    ! Upper boundary
    !
    lb = m%lb
    ub = m%ub
    lb(idim) = m(idim)%n ! only update boundary zones in upper part
    !$omp parallel do private(ix,iy,iz,n)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      snd(n) = f(ix-offset(1), iy-offset(2), iz-offset(3))
    enddo
    enddo
    enddo
    call irecv_reals_mpi (rcv, n, mpi_up(idim), mpi_comm_world, req(1))
    call isend_reals_mpi (snd, n, mpi_dn(idim), mpi_comm_world, req(2))
    call waitall_mpi (req, 2)
    !$omp parallel do private(ix,iy,iz,n)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      f(ix,iy,iz) = rcv(n)
    enddo
    enddo
    enddo
  end do
  !
END SUBROUTINE boundary_send

!*******************************************************************************
! The task is to copy, for all directions idim=1,3, the ghost zone data from
! the next and previous domains to the domain.  As is clear from the sketch of
! the index space below, this means copying the index ranges
!
!  (  0,   1)    from the domain above to our index range   ( n, n+1)
!  (n-2, n-1)    from the domain below to our index range   (-2,  -1)
!
!   rcv+    snd-                                             snd+    rcv-
!  >===>   <===<                                            >===>   <===<
!  +...+...|...+...+........................................+...+...|...+
! -2  -1   0   1   2                                       n-2 n-1  n  n+1
! lb                                                                    ub
!
! Here >==> shows the range that is sent up and <==< the range sent down, 
! with snd+/rcv+ and snd-/rcv- being the corresponding buffers.
!
! To be explicit, the exmple is drawn for nghost=2, but the code below is ok
! for any nghost.
! 
!*******************************************************************************
SUBROUTINE boundary_allocate (f)
  USE mpi_send
  USE mpi_coords
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  real, dimension(:,:,:), allocatable :: snd, rcv
  integer :: idim, ix, iy, iz, lb(3), ub(3), req(2), np
  !
  do idim=1,3
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    if (m(idim)%n==1) cycle
    !
    ! Allocate arrays to hold copies of ghostzones
    lb = m%lb; ub = m%ub                                                        ! general grid boudaries
    lb(idim) = 0                                                                ! first index of snd-
    ub(idim) = lb(idim) + m(idim)%nghost-1                                      ! last index of snd-
    np = product(ub-lb+1)                                                       ! number of points in buffer

    allocate(snd(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)), &                        ! buffers for send and recv
             rcv(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)))                          ! this is a cheap operation!
    !
    ! Upper boundary: copy the snd- range into send buffer, send & receive,
    ! and copy the receive buffer into the rcv- range
    !
    snd = f(lb(1):ub(1), lb(2):ub(2), lb(3):ub(3))                              ! snd- part
    call irecv_reals_mpi(snd, np, mpi_up(idim), mpi_comm_world, req(1))         ! receive from mpi_up(idim)
    call isend_reals_mpi(rcv, np, mpi_dn(idim), mpi_comm_world, req(2))         ! send to mpi_dn(idim)
    call waitall_mpi (req,2)                                                    ! wait for send/recv to finish
    lb(idim) = m(idim)%n                                                        ! first index in rcv-
    ub(idim) = m(idim)%ub                                                       ! last index in rcv-
    f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)) = rcv                                ! copy into rcv-
    !
    ! Lower boundary: copy the snd+ range into send buffer, send & receive,
    ! and copy the receive buffer into the rcv+ range
    !
    lb(idim) = m(idim)%n - m(idim)%nghost                                       ! first index in snd+
    ub(idim) = m(idim)%n - 1                                                    ! last index in snd+
    snd = f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3))                                ! zone to be send to lower
    call irecv_reals_mpi(rcv,np,mpi_up(idim),mpi_comm_world,req(1))             ! receive up
    call isend_reals_mpi(snd,np,mpi_dn(idim),mpi_comm_world,req(2))             ! send down
    call waitall_mpi (req,2)                                                    ! wait for send/recv to finish
    lb(idim) = -m(idim)%nghost                                                  ! lower index of rcv+
    ub(idim) = -1                                                               ! upper index of rcv+
    f(lb(1):ub(1),lb(2):ub(2),lb(3):ub(3)) = rcv                                ! copy into rcv+

    deallocate(snd,rcv)                                                         ! free the buffers
  end do
  !
END SUBROUTINE boundary_allocate

!*******************************************************************************
! This version uses an optimized approach based on MPI_Get.  A loop identical
! to the one filling the boundary zones is first used to store our values, which
! we need to send, into a snd buffer where they can be picked up with MPI_Get.
! Correspondingly, we get that buffer int rcv, and unpack it in the original loop.
!*******************************************************************************
SUBROUTINE boundary_get_opt (f)
  USE mpi_rma
  USE mpi_coords
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  real, dimension(:), allocatable :: snd, rcv
  integer :: idim, ix, iy, iz, lb(3), ub(3), offset(3), n, win, nx, nxy
  !
  ! Allocate send and receive buffers and open a window into the send one
  !
  n = max((m(1)%ub-m(1)%lb+1)*(m(2)%ub-m(2)%lb+1)*m(3)%nghost, &
          (m(2)%ub-m(2)%lb+1)*(m(3)%ub-m(3)%lb+1)*m(1)%nghost, &
          (m(3)%ub-m(3)%lb+1)*(m(1)%ub-m(1)%lb+1)*m(2)%nghost)
  allocate (snd(n), rcv(n))
  call win_create_mpi (win, snd, n)
  !
  do idim=1,3
    !
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    !
    if (m(idim)%n==1) cycle
    !
    ! Data has to be fetched a box length away
    !
    offset = 0
    offset(idim) = m(idim)%n
    !
    ! Lower boundary
    !
    lb = m%lb
    ub = m%ub
    ub(idim) = -1 ! only update boundary zones in lower part
    nx = ub(1)-lb(1)+1
    nxy = nx*(ub(2)-lb(2)+1)
    !$omp parallel do private(ix,iy,iz)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      snd(n) = f(ix+offset(1), iy+offset(2), iz+offset(3))
    enddo
    enddo
    enddo
    call win_fence_mpi (win)
    call get_reals_mpi (win, rcv, n, 0, mpi_dn(idim))
    call win_fence_mpi (win)
    !$omp parallel do private(ix,iy,iz)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      f(ix,iy,iz) = rcv(n)
    enddo
    enddo
    enddo
    !
    ! Upper boundary
    !
    lb = m%lb
    ub = m%ub
    lb(idim) = m(idim)%n ! only update boundary zones in upper part
    !$omp parallel do private(ix,iy,iz)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      snd(n) = f(ix-offset(1), iy-offset(2), iz-offset(3))
    enddo
    enddo
    enddo
    call win_fence_mpi (win)
    call get_reals_mpi (win, rcv, n, 0, mpi_up(idim))
    call win_fence_mpi (win)
    !$omp parallel do private(ix,iy,iz)
    do iz=lb(3),ub(3)
    do iy=lb(2),ub(2)
    do ix=lb(1),ub(1)
      n = 1 + ix-lb(1) + (iy-lb(2))*nx + (iz-lb(3))*nxy
      f(ix,iy,iz) = rcv(n)
    enddo
    enddo
    enddo
  end do
  !
  call win_free_mpi (win)
  deallocate (snd, rcv)
  !
END SUBROUTINE boundary_get_opt
END MODULE boundaries
