!*******************************************************************************
! Data type that describes a patch, which is the units we compute on.  This
! data type is the only one shared (synced) btw all ranks, at leat initially.
!*******************************************************************************
MODULE patch_mod
  USE task_mod, only: task_t
  USE io
  implicit none
!-------------------------------------------------------------------------------
  type:: patch_t
    integer:: id = 1                    ! unique id
    integer:: upload = -1               ! ID to upload to
    integer:: rank = 0                  ! MPI rank
    integer:: father = 1                ! father id
    integer:: level = 1                 ! refinement level
    real(kind=4):: size = 1.0           ! real(4) is exact up more than 100 levels!
    real(kind=16):: pos(3) = 0.5        ! real(16), sincecan be any position 
    integer:: son(2,2,2) = 0            ! son's ids
    integer:: neighbor(3,2) = 1         ! neighbors ids
    integer:: foreign = 0               ! number of foreigners (different rank)
    logical:: refine = .false.          ! flag to cause refinement
    logical:: refined = .false.         ! false only for leaf cells
    real   :: load = 1.0                ! relative cost of advancing this patch
    integer:: ncell = 8                 ! patch cell size (suggestion)
    integer:: nborder = 2               ! number of ghost zones
    type(task_t), pointer:: task        ! point to the task handling this patch
  end type
!-------------------------------------------------------------------------------
PUBLIC:: patch_t, make_son
CONTAINS

!*******************************************************************************
! Make a son.  He inherits the rank, has one level more refinement and thus
! half the size, and initially no sons.
!*******************************************************************************
FUNCTION make_son (ix, iy, iz, id, father)   RESULT (my)
  implicit none
  type(patch_t):: my, father
  integer:: ix, iy, iz, id, dir, pos, mypos(3)
!...............................................................................
  my%id = id
  my%rank = father%rank
  my%father = father%id
  my%level = father%level + 1
  my%size = father%size/2.0
  my%load = father%load*2.0
  my%son = 0
  my%refine = .false.
  mypos = (/ix,iy,iz/)
  my%neighbor = father%id          ! initial values -- to be updated
  my%foreign = 0                   ! no peers on the same level yet
  do dir=1,3
    my%pos(dir) = father%pos(dir) + my%size*((2*mypos(dir)-3)*0.5) 
  end do
END FUNCTION make_son

!*******************************************************************************
END MODULE patch_mod
