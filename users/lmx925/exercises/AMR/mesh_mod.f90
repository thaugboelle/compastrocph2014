!*******************************************************************************
! Setup mesh, coordinate axes, and allocate memory
!*******************************************************************************
MODULE mesh_mod
  USE mpi_base,   only: master, end_mpi
  USE mpi_coords, only: mpi_dims, mpi_coord, cart_create_mpi
  USE io, only: input, trace_begin, trace_end
  implicit none
PUBLIC
  type, public :: mesh_t
    integer :: gn                                                               ! Global mesh size
    integer :: n                                                                ! Local mesh size
    integer :: offset                                                           ! offset of local mesh patch
    real(8) :: s                                                                ! Physical box size
    real(8) :: d                                                                ! cell size
    real(8), allocatable, dimension(:) :: r                                     ! coordinate axis
    integer :: nghost                                                           ! Number of ghost-cells
    integer :: lb                                                               ! lower bdry of mesh incl ghost zones                
    integer :: ub                                                               ! upper bdry of mesh incl ghost zones     
    integer :: l1                                                               ! lower bdry of mesh incl ghost zones                
    integer :: u1                                                               ! upper bdry of mesh incl ghost zones     
    integer :: l2                                                               ! lower bdry of mesh incl ghost zones                
    integer :: u2                                                               ! upper bdry of mesh incl ghost zones     
    integer :: n_mpi                                                            ! number of MPI splits           
    real(16):: pos                                                              ! position
  end type
  type(mesh_t), target:: m(3)
  type(mesh_t), pointer:: mp
CONTAINS

!*******************************************************************************
SUBROUTINE init_mesh
  implicit none
  real, pointer, dimension(:):: rp
  real,    dimension(3):: s     = (/64.,64.,64./)
  integer, dimension(3):: gn    = (/ 64, 64,  1/)
  integer, dimension(3):: n_mpi = (/  1,  1,  1/)
  integer :: idim, i, ix, iy, iz
  namelist /mesh_params/ s, gn, n_mpi
  !-----------------------------------------------------------------------------
  ! Setup MPI with a cartesian MPI arrangement
  !-----------------------------------------------------------------------------
  rewind(input); read(input,mesh_params); if (master) write(*,mesh_params)
  call cart_create_mpi (n_mpi, gn)                                              ! make cartesian MPI
  m%n_mpi = mpi_dims                                                            ! in case it's been modified
  m%s     = s                                                                   ! global size
  m%gn    = gn                                                                  ! global dimension
  m%n     = gn/mpi_dims                                                         ! local domain size
  if (any(m%n*mpi_dims .ne. gn)) then                                           ! check consistency
    if (master) then
      print *, 'ERROR mpi_dims has to be a divisor in global mesh size' 
      print *, 'Global mesh size :', m%gn
      print *, 'Local mesh size  :', m%n
      print *, 'MPI dimensions   :', m%n_mpi
      print *, 'n * mpi_dims     :', m%n*m%n_mpi
    endif
    call end_mpi
  endif
  m%offset = mpi_coord*m%n                                                      ! offset global mesh
  !----------------------------------------------------------------------------
  ! Set up initial mesh. The mesh point position is at the center of the cell
  ! Point interval is [-nghost : n + nghost - 1], physical interval is [0:n-1]
  !----------------------------------------------------------------------------
  do idim=1,3
    mp => m(idim)                                                               ! cache one direction
    mp%nghost = 2
    mp%lb = -mp%nghost
    mp%ub = mp%n + mp%nghost - 1
    mp%l1 = -mp%nghost
    mp%u1 = mp%n/2 + mp%nghost - 1
    mp%l2 = -mp%nghost
    mp%u2 = mp%n*2 + mp%nghost - 1
    if (mp%n==1) then
      mp%lb = 0
      mp%ub = 0
    end if
    allocate(mp%r(mp%lb:mp%ub))                                                 ! allocate coordinate axis
    mp%d = mp%s/mp%gn                                                           ! cell size
    do i=0,mp%n-1
      mp%r(i) = mp%d * real((i-mp%gn/2+mp%offset+0.5),kind=16)                  ! coordinates of cell center
    enddo
  enddo
  m%pos  = m%offset*m%d
END SUBROUTINE init_mesh

!*******************************************************************************
END MODULE mesh_mod
