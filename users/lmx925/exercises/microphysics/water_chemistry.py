from math import *
from scipy.integrate import odeint
import random as rand
import numpy as np
import sys
import matplotlib.pyplot as plt

######CONSTANTS######
spy = 365e0*24e0*3600e0 #seconds per year
idx_Oj = 0 #index O+
idx_H2 = 1 #index H2
idx_OHj = 2 #index OH+
idx_H = 3 #index H
idx_H2Oj = 4 #index H2O+
idx_H3Oj = 5 #index H3O+
idx_E = 6 #index E
idx_H2O = 7 #index H2O
idx_OH = 8 #index OH
idx_O = 9 #index O
number_of_species = 10 #number of species
number_of_reactions = 9 #number of reactions
random = "NO"
CRP    = "YES"

factor = np.ones(number_of_reactions)
if random == "YES":
    for i in range(number_of_reactions):
        factor[i] = rand.uniform(0.5,1.5)

######FUNCTIONS START HERE######
#returns rate coefficients
def get_coefficients(Tgas,factor):
	ionizing_cosmic_ray_flux = 1.3e-17 #1/s
	k = [0e0 for x in range(number_of_reactions)] #initialize coefficients
	invsqrt = 1e0/sqrt(Tgas/3e2)
	k[0] = 1.6e-9 #O+ + H2 -> OH+ + H
	k[1] = 1.1e-9 #OH+ + H2 -> H2O+ + H
	k[2] = 6.1e-10 #H2O+ + H2 -> H3O+ + H
	k[3] = 1.1e-7*invsqrt #H3O+ + E -> H2O + H
	k[4] = 8.6e-8*invsqrt #H2O+ + E -> OH + H
	k[5] = 3.9e-8*invsqrt #H2O+ + E -> O + H2
	k[6] = 6.3e-9*(Tgas/3e2)**(-.48) #OH+ + E -> O + H
	k[7] = 2.8e0*ionizing_cosmic_ray_flux #O + CR -> O+ + E
	k[8] = 0.0
	if CRP == "YES":
	   k[8] = 9.7e2*ionizing_cosmic_ray_flux #H2O + CRP -> H + OH

	return k

#returns reaction rates k*n*n or k*n (cm-3/s)
def get_reaction_rates(n):
	k = get_coefficients(Tgas,factor)

	return [k[0]*n[idx_Oj]*n[idx_H2],\
	 k[1]*n[idx_OHj]*n[idx_H2],\
	 k[2]*n[idx_H2Oj]*n[idx_H2],\
	 k[3]*n[idx_H3Oj]*n[idx_E],\
	 k[4]*n[idx_H2Oj]*n[idx_E],\
	 k[5]*n[idx_H2Oj]*n[idx_E],\
	 k[6]*n[idx_OHj]*n[idx_E],\
	 k[7]*n[idx_O],k[8]*n[idx_H2O]]

#returns differential equations (dn/dt)
def differential_equations(n,t):

	rates = get_reaction_rates(n)

	#initialize ODEs
	dn = [0e0 for x in range(number_of_species)]
	
	#ODEs
	dn[idx_Oj] = \
		-rates[0] \
		+rates[7]

	dn[idx_H2] = \
		-rates[0] \
		-rates[1] \
		-rates[2] \
		+rates[5]

	dn[idx_OHj] = \
		+rates[0] \
		-rates[1] \
		-rates[6]

	dn[idx_H] = \
		+rates[0] \
		+rates[1] \
		+rates[2] \
		+rates[3] \
		+rates[4] \
		+rates[6] \
		+rates[8]

	dn[idx_H2Oj] = \
		+rates[1] \
		-rates[2] \
		-rates[4] \
		-rates[5]

	dn[idx_H3Oj] = \
		+rates[2] \
		-rates[3]

	dn[idx_E] = \
		-rates[3] \
		-rates[4] \
		-rates[5] \
		-rates[6] \
		+rates[7]

	dn[idx_H2O] = \
		+rates[3]\
		-rates[8] # destruction by cosmic rays

	dn[idx_OH] = \
		+rates[4]\
		+rates[8]

	dn[idx_O] = \
		+rates[5] \
		+rates[6] \
		-rates[7]

	return dn
######FUNCTIONS END HERE######


######MAIN STARTS HERE######

time = 0e0 #initial total time (s)
time_step = spy #initial time-step (s)
time_end = 1e6*spy #end of simulation (s)

#default number densities (cm-3)
number_density = [1e-40 for i in range(number_of_species)]
total_hydrogen_nuclei = 1e8 #cm-3
#define species number densities as fractions of total H nuclei
number_density[idx_H2] = 1e-1*total_hydrogen_nuclei #cm-3
number_density[idx_O] = 1e-4*total_hydrogen_nuclei #cm-3

#gas temperature
Tgas = 5e1 #K

#infinite loop (breaks when time_end reached)
ii = 0
nH2O = np.zeros(1000000)
nOH = np.zeros(1000000)
Rates = np.zeros((1000000,number_of_reactions))
t = np.zeros(1000000)

while(True):
        # Output old values
        nH2O[ii] = number_density[idx_H2O]
        nOH[ii] = number_density[idx_OH]
        Rates[ii][:] = get_reaction_rates(number_density)
        t[ii]    = time/spy
        ii += 1
	#compute electrons number density (sum over ions)
	number_density[idx_E] = number_density[idx_Oj] + number_density[idx_OHj] + number_density[idx_H2Oj] + number_density[idx_H3Oj]
	time_grid = np.linspace(0, time_step, 2) #prepare time grid (2 value = 1 step)
	results = odeint(differential_equations, number_density, time_grid, rtol=1e-6, atol=1e-20) #solve
	time += time_step #increase time (s)
	time_step = time_step * 1.1e0 #increase time-step (for output purposes)
	number_density = results[1][:] #results are new initial conditions (mind results structure!)
	#break when reach the end time
	

	if(time>time_end): break
print "done!"
######MAIN ENDS HERE######

###### Plotting starts here ########

plt.figure(1)
plt.loglog(t,nH2O,'--',label="H$_2$O")
plt.loglog(t,nOH,'--',label="OH")
plt.legend(loc="lower right")
plt.ylabel(r"Abundance [cm$^-3$]")
plt.xlabel(r"time [yr]")
plt.ylim(1e-18,1e2)
#plt.savefig("Abundance_extra.png")
plt.show()

#Rates#
NUM_COLORS = number_of_reactions
cm = plt.get_cmap('gist_rainbow')
fig = plt.figure(2)
ax = fig.add_subplot(111)
ax.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
for i in range(NUM_COLORS):
    color = cm(1.*i/NUM_COLORS)  # color will now be an RGBA tuple
    y = Rates[:,i]
    ax.loglog(t,y,label=r"R["+str(i)+"]")
plt.xlabel(r"time [yr]")
plt.ylabel(r"Rates [s$^-1$]")
plt.legend(loc="lower right")
plt.ylim(1e-24,1e-12)
#plt.savefig("Rates_3.png")
plt.show()

# 
plt.figure(3)
plt.loglog(t,nH2O,'--',label="H$_2$O")
#plt.loglog(t,nOH,'--',label="OH")
plt.legend(loc="lower right")
plt.ylabel(r"Abundance [cm$^-3$]")
plt.xlabel(r"time [yr]")
plt.ylim(1e-18,1e2)
#plt.savefig("Abundance_extra.png")
plt.show()