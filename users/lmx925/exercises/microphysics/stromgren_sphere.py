from math import *
from scipy.integrate import odeint
from random import random as rand
import numpy as np
import sys
import matplotlib.pyplot as plt

######CONSTANTS######
pi = 3.14159265359e0 #pi
spy = 365e0*24e0*3600e0 #seconds per year
boltzmann_constant = 8.6173324e-5 #eV/K
planck_constant = 4.135667516e-15 #eV*s
speed_of_light = 2.9979245800e10 #cm/s
solar_radius = 6.955e10 #solar radius cm
pc2cm = 3.08567758e18 #pc->cm
idx_H = 0 #index H2
idx_Hj = 1 #index O+
idx_O = 2 #index H2
idx_Oj = 3 #index O+
idx_E = 4 #index free electrons
number_of_species = 5 #number of species
number_of_reactions = 4 #number of reactions
number_of_grid_points = 300 #number of grid points in space
number_of_frequency_bins = number_of_energy_bins = 10 #number of frequency/energy bins

######FUNCTIONS START HERE######

#black body spectral radiance at Tbb in eV/cm2/sr, Ebb in eV, Tbb in K
def black_body_radiance(Ebb,Tbb):
	frequency = Ebb/planck_constant
	xexp = Ebb/boltzmann_constant/Tbb
	#avoid under/overflows and save useless calculations
	if(xexp>1e2):
		fbb = 0e0
	else:
		fbb = 2e0*planck_constant*frequency**3/speed_of_light**2/(exp(xexp)-1e0)
	return fbb

#cross section fit from Verner+96 (http://www.pa.uky.edu/~verner/atom.html)
def sigma_v96(energy_eV,E0,sigma_0,ya,P,yw,y0,y1):
    x = energy_eV/E0 - y0
    y = sqrt(x**2 + y1**2)
    Fy = ((x - 1e0)**2 + yw**2) *  y**(0.5*P-5.5) * (1e0+sqrt(y/ya))**(-P)
    return 1e-18 * sigma_0 * Fy #cm2

#hydrogen photoionization cross section, energy in eV
def photo_cross_section_H(energy):
	if(energy<13.6e0): return 0e0 #energy threshold
	#compute xsec by using verner+96 fit with parameters for H
	return sigma_v96(energy, 4.298e-1, 5.475e4, 3.288e1, 2.963e0, 0e0, 0e0, 0e0)

#Oxygen photoionization cross section, energy in eV
def photo_cross_section_O(energy):
	if(energy<13.6e0): return 0e0 #energy threshold, should be different for O?
	#compute xsec by using verner+96 fit with parameters for O
	return sigma_v96(energy, 1.240e0, 1.745e3, 3.784e0, 1.764e1, 7.589e-2, 8.698e0, 1.271e-1)

#spectral radiance emitted by the star, eV/cm2/s 
def radiation_flux(energy):
	temperature_star = 3e4 #K
	flux0 = black_body_radiance(energy, temperature_star)
	return flux0 #eV/cm2/sr

#compute photorate as an integral over the frequencies (warning: rough integration!)
def photorate(flux, xsec):
	rate = 0e0 #1/s
	for j in range(number_of_frequency_bins):
		energy = energy_bins[j]
		rate += global_scaling_factor[j]*xsec(energy)*flux(energy)*delta_energy/planck_constant/energy
	return rate

#returns the opacity at a given energy for an element of size cell_size with the chemical composition n
def get_opacity(energy, cell_size, n):
	hydrogen_opacity = n[idx_H] * cell_size * photo_cross_section_H(energy)
	oxygen_opacity   = n[idx_O] * cell_size * photo_cross_section_O(energy)
	return hydrogen_opacity + oxygen_opacity

#returns rate coefficients
def get_coefficients(Tgas):
	k = [0e0 for x in range(number_of_reactions)] #initialize coefficients
	k[0] = photorate(radiation_flux, photo_cross_section_H) #H + photon -> H+ + e
	k[1] = 3.5e-12*(Tgas/1e4)**(-0.7)  #H+ + e -> H + photon
	k[2] = photorate(radiation_flux, photo_cross_section_O) #O + photon -> O+ + e
	k[3] = 3.4e-12*(Tgas/3e2)**(-0.63) #O+ + e -> O + photon
	return k

#returns reaction rates k*n*n or k*n (cm-3/s)
def get_reaction_rates(n,Tgas):

	k = get_coefficients(Tgas)

	return [k[0]*n[idx_H],\
	 k[1]*n[idx_Hj]*n[idx_E],\
	 k[2]*n[idx_O],\
	 k[3]*n[idx_Oj]*n[idx_E]]

#returns differential equations (dn/dt)
def differential_equations(n,t):

	#get the reaction rates
	rates = get_reaction_rates(n,Tgas)
	

	#initialize ODEs
	dn = [0e0 for x in range(number_of_species)]
	
	#ODEs
	dn[idx_H] = \
		-rates[0] \
		+rates[1]

	dn[idx_Hj] = \
		+rates[0] \
		-rates[1]

	dn[idx_O] = \
		-rates[2] \
		+rates[3]

	dn[idx_Oj] = \
		+rates[2] \
		-rates[3]	

	dn[idx_E] = \
		+rates[0] \
		-rates[1]

	return dn

######FUNCTIONS END HERE######


######MAIN STARTS HERE######

######PARAMETERS########
star_radius = 3e1*solar_radius #radius of the photon source, cm
minimum_distance = 1e2*pc2cm #beginning of optically thick region, cm
maximum_distance = 6e3*pc2cm #end of simulation box, cm
minimum_energy = 5e0 #minumim energy of SED, eV
maximum_energy = 2e1 #maximum energy of SED, eV
delta_energy = (maximum_energy-minimum_energy)/(number_of_frequency_bins-1) #energy interval, eV

#compute size of a cell (equallyspaced)
grid_cell_size = (maximum_distance-minimum_distance) / (number_of_grid_points-1) #cm

time = 0e0 #initial total time (s)
time_step = spy #initial time-step (s)
time_end = 3e7*spy #end of simulation (s)

#default for scaling of radiation flux
global_scaling_factor = [1e0 for i in range(number_of_frequency_bins)]

#init energy bins, eV
energy_bins = [(minimum_energy + delta_energy*i) for i in range(number_of_frequency_bins)]

#default opacity
opacity = [1e0 for i in range(number_of_frequency_bins)]

#initialize grid with default number densities
grid = [[1e-40 for i in range(number_of_species)] for j in range(number_of_grid_points)]

#set non-default values for different species
total_hydrogen_nuclei = 1e-3 #cm-3
ionization_fraction = 1.2e-3
#define species number densities as fractions of total H nuclei
for i in range(number_of_grid_points):
	grid[i][idx_H] = total_hydrogen_nuclei * (1e0 - ionization_fraction) #cm-3
	grid[i][idx_Hj] = total_hydrogen_nuclei * ionization_fraction #cm-3
	grid[i][idx_E] = grid[i][idx_Hj] #cm-3
	grid[i][idx_O] = grid[i][idx_H]*1e-5
	grid[i][idx_Oj] = grid[i][idx_O]*1e-5 

#gas temperature
Tgas = 1e4 #K

output_grid = np.zeros((100000,number_of_grid_points,number_of_species))
t = np.zeros(100000)

print "it could take a while..."
kk = 0
#infinite loop on time (breaks when time_end reached)
while(True):
	#default total opacity	
	total_opacity = [1e0 for i in range(number_of_frequency_bins)]
	#loop on space (grid point by grid point)
	for i in range(number_of_grid_points):
		number_densities = grid[i][:] #copy number density of the given grid point
		grid[i][idx_E] = grid[i][idx_Oj] + grid[i][idx_Hj] #compute electrons
		distance_from_star = i*grid_cell_size + minimum_distance #distance of the grid point from source, cm
		#loop on the frequency bins
		for j in range(number_of_frequency_bins):
			#get local opacity with the given chemical composition
			opacity[j] = get_opacity(energy_bins[j], grid_cell_size, number_densities)
			#total opacity is the product of all the opacities of the grid points hit by the photon 
			total_opacity[j] *= exp(-opacity[j])
			#flux scaling factor contains geometrical decay and opacity
			global_scaling_factor[j] = pi*star_radius**2/distance_from_star**2 * total_opacity[j]
		#prepare and do chemical calculation
		time_grid = np.linspace(0, time_step, 2) #prepare time grid (2 value = 1 step)
		results = odeint(differential_equations, number_densities, time_grid, rtol=1e-8, atol=1e-20) #solve
		grid[i] = results[1][:] #results are new initial conditions (mind results structure!)
		#write some output		
	output_grid[kk] = grid
	t[kk] = time
	print "kk",kk
	kk += 1
	time_step = time_step * 1.2e0 #increase time-step (for output purposes)
	time += time_step #increases time (s)
	#breaks when reach the end time
	if(time>time_end): break
print "done!"
######MAIN ENDS HERE######

plt.figure(1)
for k in range(84):
        if k == 0:
	   plt.semilogy(output_grid[k,:,idx_Hj],'g',label="H+")
	   plt.semilogy(output_grid[k,:,idx_H],'r',label="H")
	else:
	   plt.semilogy(output_grid[k,:,idx_Hj],'g')
	   plt.semilogy(output_grid[k,:,idx_H],'r')
	    
plt.ylim(1e-8,1e-3)

plt.legend(loc="upper right")
plt.ylabel(r"n [cm$^{-3}$]")
plt.xlabel(r"distance")
plt.show()
plt.savefig("HandHplus.png")

plt.figure(2)
for k in range(84):
        if k == 0:
	   plt.semilogy(output_grid[k,:,idx_Hj],'g',label="H+")
	   plt.semilogy(output_grid[k,:,idx_H],'r',label="H")
	   plt.semilogy(output_grid[k,:,idx_Oj],'m',label="O+")
	   plt.semilogy(output_grid[k,:,idx_O],'b',label="O")
	else:
	   plt.semilogy(output_grid[k,:,idx_Hj],'g')
	   plt.semilogy(output_grid[k,:,idx_H],'r')
	   plt.semilogy(output_grid[k,:,idx_Oj],'m')
	   plt.semilogy(output_grid[k,:,idx_O],'b')
	    
plt.ylim(1e-14,1e-2)

plt.legend(loc="upper right")
plt.ylabel(r"n [cm$^{-3}$]")
plt.xlabel(r"distance")
plt.show()
plt.savefig("OandOplus.png")

