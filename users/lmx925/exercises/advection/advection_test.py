import matplotlib.pyplot as pl
import numpy as np

def initial_condition(n,w,time):
    """ --------------------------------------------------------------------
        The initial condition is a density which is a Gaussian of width w
        --------------------------------------------------------------------
    """
    x=np.zeros((n,n))
    y=np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            y[i,j]=i-n/2
            x[i,j]=j-n/2
    ux=1.0; uy=0.5;

    class ic:
        ux = 1.0; uy = 0.5; dx=1.0; t = time;
        xt = ((x-ux*time) + n/2 ) % n - n/2 # calculate position at a certain
        yt = ((y-uy*time) + n/2 ) % n - n/2 # time and apply periodic boundaries.
        rho=np.exp(-(xt**2+yt**2)/w**2)
        
    return ic

def dd(f,axis):
    """ --------------------------------------------------------------------
        Derivatives evaluated with central differences
        --------------------------------------------------------------------
    """
    a=8./12.; b=-1./12.
    return (np.roll(f,-1,axis=axis)-np.roll(f,1,axis=axis))*a + \
           (np.roll(f,-2,axis=axis)-np.roll(f,2,axis=axis))*b

def time_step(f,courant):
    global umax
    """ --------------------------------------------------------------------
        Simplest possible time step -- first order in time
        --------------------------------------------------------------------
    """
    umax=np.sqrt(np.max(f.ux**2+f.uy**2))
    dt=courant*f.dx/umax
    f.rho=f.rho-(dt/f.dx)*(dd(f.rho*f.ux,1)+dd(f.rho*f.uy,0))
    f.t = f.t + dt
    return f

def advection_test(n_step,courant):
    """ --------------------------------------------------------------------
        This test should just move the Gaussian, without changing the shape, 
        and w/o adding any erroneous new features.  Run it first for 100 steps, 
        then 300, then 500 steps.  Vary the Courant number (the fraction of 
        a mesh the profiles is moved per step), and try to move the shape as 
        far as possible (without worrying about the number of time steps needed).
        --------------------------------------------------------------------
    """
    
    f=initial_condition(64,10.0,0.0)
    im=pl.imshow(f.rho,origin='lower')
    pl.show()
    for i in range(n_step):
        f=time_step(f,courant)
        if i % (n_step/20) == 0: # plots 20 snapshots for each run
            im.set_data(f.rho)
            pl.draw()
    final = initial_condition(64,10.0,f.t) # final.rho is expected value
    error = np.amax((f.rho-final.rho))     # L1-error norm
    print "Number of steps is : ", n_step
    print "Courant : ", courant
    print "The L1- error is", error
    print ""
    return error

print "Advection test by lmx925"
print """I use the L1-error norm instead of a deviation in percentage of 10 %.  
It does not make sence to use a percentage as a measure of deviation because
it leads to divisions by 0. Instead I limit the L1 error to be less than
0.1. """

print "The three suggested runs: "
#advection_test(100,0.2) # run with n = 100
#advection_test(300,0.2) # run with n = 300
#advection_test(500,0.2) # run with n = 500

print "Testing which nstep and courant to use"
print ""
advection_test(800,0.1) # error of 0.091, distance of 80
# advection_test(100000,0.008) # error of 0.07119, distance of 800, takes a while

# Basically, one can increase the number of steps and decrease the time step
# in order to advect the profile further and further. This becomes computationally
# expensive so I stopped at a distance of 800.
# In the limit of nstep -> \infty, C -> 0,  cumulative round-off
# errors should be a problem.




   