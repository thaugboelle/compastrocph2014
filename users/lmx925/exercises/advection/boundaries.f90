MODULE boundaries
  USE mesh
  USE mpi_coords
  USE mpi_send
  implicit none
CONTAINS

SUBROUTINE init_boundary
  implicit none
END SUBROUTINE init_boundary

!*******************************************************************************
! Boundary conditions -- assumed periodic
!*******************************************************************************
SUBROUTINE boundary (f)
  implicit none

  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: f
  real, dimension(m(1)%lb:m(1)%ub,2) :: a, b
  integer :: idim, ix, iy, iz, lb(3), ub(3), offset(3),req(2),up,dn,j,nghost
  nghost = 2

    ! Wrapping offset for periodic boundaries
    ! Data has to be fetched a box length away
    offset = 0
    offset(:) = m(:)%n
    !
    ! Lower boundary
    !
    lb = m%lb
    ub = m%ub
   
    
    ! Lower boundary - y
    do ix = lb(1), ub(1)
    do j  = 1,nghost 
       a(ix,j) = f(ix,ub(2)-2*nghost+j, ub(3))
    enddo
    enddo
    
    dn = mpi_dn(2)
    up = mpi_up(2)
    call barrier_mpi('init')
    call isend_reals_mpi (a  , (ub(1)-lb(1)+1)*2, up, mpi_comm_world, req(1))   ! <-  NO change to ..
    call irecv_reals_mpi (b  , (ub(1)-lb(1)+1)*2, dn, mpi_comm_world, req(2))   !   | .. a in this ..
    call waitall_mpi (req, 2)                                                     ! <-  .. interval 
    do ix = lb(1), ub(1)
    do j  = 1,nghost
       f(ix,lb(2)+j-1,lb(3)) = b(ix,j)
    enddo
    enddo

    ! Upper boundary - y
    do ix = lb(1), ub(1)
    do j  = 1,nghost 
       a(ix,j) = f(ix,lb(2)+1+j, ub(3))
    enddo
    enddo
    !ub(idim) = -1 ! only update boundary zones in lower part
    call barrier_mpi('init')
    call isend_reals_mpi (a  , (ub(1)-lb(1)+1)*2, dn, mpi_comm_world, req(1))   ! <-  NO change to ..
    call irecv_reals_mpi (b  , (ub(1)-lb(1)+1)*2, up, mpi_comm_world, req(2))   !   | .. a in this ..
    call waitall_mpi (req, 2)                                                     ! <-  .. interval 
    do ix = lb(1), ub(1)
    do j  = 1,nghost
       f(ix,ub(2)-nghost+j,lb(3)) = b(ix,j)
    enddo
    enddo
    
     
    ! Upper boundary - x
    do iy = lb(2), ub(2)
    do j  = 1,nghost 
       a(iy,j) = f(lb(1)+1+j,iy, ub(3))
    enddo
    enddo
    !ub(idim) = -1 ! only update boundary zones in lower part
    call barrier_mpi('init')
    call isend_reals_mpi (a  , (ub(2)-lb(2)+1)*2, mpi_dn(1), mpi_comm_world, req(1))   ! <-  NO change to ..
    call irecv_reals_mpi (b  , (ub(2)-lb(2)+1)*2, mpi_up(1), mpi_comm_world, req(2))   !   | .. a in this ..
    call waitall_mpi (req, 2)                                                     ! <-  .. interval 
    do iy = lb(2), ub(2)
    do j  = 1,nghost
       f(ub(1)-nghost+j,iy,lb(3)) = b(iy,j)
    enddo
    enddo
    !
    
    ! Lower boundary - x
    do iy = lb(2), ub(2)
    do j  = 1,nghost 
       a(iy,j) = f(ub(2)-2*nghost+j,iy, ub(3))
    enddo
    enddo
    
    call barrier_mpi('init')
    call isend_reals_mpi (a  , (ub(2)-lb(2)+1)*2, mpi_up(1), mpi_comm_world, req(1))   ! <-  NO change to ..
    call irecv_reals_mpi (b  , (ub(2)-lb(2)+1)*2, mpi_dn(1), mpi_comm_world, req(2))   !   | .. a in this ..
    call waitall_mpi (req, 2)                                                     ! <-  .. interval 
    do iy = lb(2), ub(2)
    do j  = 1,nghost
       f(lb(2)+j-1,iy,lb(3)) = b(iy,j)
    enddo
    enddo
  !
END SUBROUTINE boundary
END MODULE boundaries
