!*******************************************************************************
! Module with skeleton data type for self-gravity
!*******************************************************************************
MODULE poisson_mod
  USE mesh_mod,    only: m, mesh_t
  USE vector_mod
  USE io
PRIVATE
  logical      :: first_time=.true.
  logical      :: selfgravity=.false.
  integer      :: itmax = 1000
  real(kind=8) :: tolerance = 1e-5
  real, parameter :: fourpi = 12.56637061435917225
  character(len=100) :: solver='jacobi'                                          ! Solver for finding phi
  !
  integer, parameter             :: n_neighbor=10                                ! max nr of neighbors to consider
  !
  ! error_distance: The patch is ready, if the error is below the tolerance and in
  !   addition neighbour patches have errors below tolerance*error_distance^(ndist),
  !   where ndist is how many patches away a neighbor patch is.
  real(kind=8) :: error_distance = 1.1
  !
  ! max_lag: max number of iterations a patch can be out of sync with neighbors. Each number
  !   indicates how many iterations we can be out of sync, for a patch that is that many patches
  !   away.
  integer, dimension(n_neighbor) :: p_max_lag = (/2, 4, 6, 8, 10, 14, 18, 22, 28, 36 /)
  !
  real         :: G=0.                                                           ! Strength of gravity
  type poisson_t
    real, dimension(:,:,:), pointer :: phi => null()                             ! gravitational potential
    real, dimension(:,:,:), pointer :: p_cg => null(), r_cg => null(), &
                                       w_cg => null(), x_cg => null()            ! scratch arrays for CG method
    integer, dimension(0:n_neighbor) :: it                                       ! iteration count; for neighbors too
    real,    dimension(0:n_neighbor) :: error                                    ! error in iterative method
    real    :: max_rho, gamma_cg
    logical :: ready
  end type
  !
PUBLIC :: G, selfgravity, poisson_t, &
          init_selfgravity, selfgravity_phi, selfgravity_force, &
          allocate_poisson, deallocate_poisson

CONTAINS

!************************************************************************
SUBROUTINE init_selfgravity
  implicit none
  namelist /selfgravity_params/ G, tolerance, selfgravity, itmax, solver
!........................................................................
  call trace_begin ('init_selfgravity')
  rewind(input); read(input,selfgravity_params); if(master) write(*,selfgravity_params)
  first_time = .false.
  call trace_end
END SUBROUTINE init_selfgravity

!*******************************************************************************
SUBROUTINE allocate_poisson (a)
  implicit none
  type(poisson_t) :: a
!...............................................................................
  call allocate_scalar (a%phi)
  a%ready = .false.
  a%it = 0
  a%error = huge(0.)
END SUBROUTINE allocate_poisson

!*******************************************************************************
SUBROUTINE deallocate_poisson (a)
  implicit none
  type(poisson_t) :: a
!...............................................................................
  call deallocate_scalar (a%phi)
END SUBROUTINE deallocate_poisson

!*******************************************************************************
SUBROUTINE selfgravity_phi(p,f)
  USE mhd_mod,   only : mhd_t
  USE operators, only : scalar_stats, laplace
  implicit none
  type(poisson_t) :: p
  type(mhd_t)     :: f
  real(kind=8), dimension(0:n_neighbor) :: fudge
  integer         :: i
  real            :: aver_rho
!...............................................................................  
  if (.not. selfgravity) return                                                 ! Only enter if gravity is active
  
  fudge(0) = tolerance                                                          ! Setup cumulative error array
  do i=1,n_neighbor
    fudge(i) = fudge(i-1)*error_distance
  enddo

  aver_rho = sum(f%d(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)) / product(m%n)          ! ORG this should realy be average rho in box, and not on patch

  do while (.not. p%ready)
    call pull_neighbor_error(p)                                                 ! Upd the error and iteration count seen on close-by patches.
                                                                                ! This is done recursively, and we only need to pull from nearest neighbors
    if (any(p%error > fudge) .and. &
        all(p%it(0) <= p%it(1:n_neighbor)+p_max_lag)) then                      ! check if it is ok to iterate

      call iterate_selfgravity_on_patch(p,f%d,aver_rho)                         ! iterate selfgravity and update error
    else
      ! if we are ahead of other patches, suspend task
      !$omp taskyield                                                           
    endif

    p%ready = all(p%error < fudge) .or. p%it(0) > itmax                         ! upd ready switch
  enddo

  print *, "Gravity: Error, It: ", p%error(0), p%it(0)

  call cleanup_solver(p)
  call boundary(p%phi)

  !call scalar_stats (p%phi, 'SG:phi')
  !call scalar_stats (f%d,   'SG:rho')
  !call scalar_stats (laplace(p%phi) - fourpi*G*f%d, 'SG:res')
END SUBROUTINE selfgravity_phi

!*******************************************************************************
SUBROUTINE cleanup_solver(p)
  implicit none
  type(poisson_t) :: p
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: rho
!...............................................................................  
  select case (trim(solver))
  case('cg')
    p%phi(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1) = p%x_cg                            ! copy over phi from iterative solution
    call pull_selfgravity_boundaries(p)                                         ! update boundaries of phi, so that force can use it

    deallocate(p%r_cg,p%x_cg,p%w_cg)                                            ! do not cleanup p, could be in use at neigboring patches

    p%gamma_cg = 0
    p%error = 0.0
  end select  

END SUBROUTINE cleanup_solver

!*******************************************************************************
SUBROUTINE iterate_selfgravity_on_patch(p,rho,aver_rho)
  implicit none
  type(poisson_t) :: p
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: rho
  real    :: aver_rho
!...............................................................................  
  ! Based on the solver variable, select which solver to call
  select case (trim(solver))
  case('jacobi')
    call jacobi(p,rho,aver_rho)
  case('sor')
    call sor(p,rho,aver_rho)
  !case('cg')
  !  if (p%it(0)==1 .and. p%it(1)==0) then                                       ! we have to make sure p%p_cg exists on neighbor tasks
  !    !$omp taskyield
  !  else  
  !    call cg  (p,rho,aver_rho)
  !  endif
  case default
    call die('unknown gravity solver. Use either jacobi or sor in selfgravity_params namelist.')
  end select

END SUBROUTINE iterate_selfgravity_on_patch

!*******************************************************************************
SUBROUTINE sor(p,rho,aver_rho)
  implicit none
  type(poisson_t) :: p
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: rho
  real, dimension(:,:,:), allocatable :: phi_guess
  real :: a, b, aver_rho, max_rho, w
  integer :: ix,iy,iz
!...............................................................................  
  ! Jacobi solver; beware! Very slow.
  !
  ! Solve iteratively
  !
  ! (f^+x - 2 f + f^-x) / dx^2 + (f^+y - 2 f + f^-y) / dy^2 + (f^+z - 2 f + f^-z) / dz^2 = rho
  !
  call pull_selfgravity_boundaries(p)                                          ! upd bndrys now that we need them

  allocate(phi_guess(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1))

  w = 1. / ( 1. + sin(3.14159265359/(minval(m%n)+1.)) )

  ! 1. / Coefficient for central value of phi; each active dimension counts with 2 / ds^2
  a = 0.
  if (m(1)%n > 1) a = a - 2. / m(1)%d**2
  if (m(2)%n > 1) a = a - 2. / m(2)%d**2
  if (m(3)%n > 1) a = a - 2. / m(3)%d**2
  a = 1. / a

  ! Set values for phi_guess == left hand side of eq
  b = a*fourpi*G
  max_rho = 1e-20
  do iz=0,m(3)%n-1
  do iy=0,m(2)%n-1
  do ix=0,m(1)%n-1
    phi_guess(ix,iy,iz) = b*(rho(ix,iy,iz)-aver_rho)
    max_rho = max(max_rho, abs(b*(rho(ix,iy,iz)-aver_rho)))
  end do
  end do
  end do

  ! X-derivative
  if (m(1)%n > 1) then 
    b = - a / (m(1)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      phi_guess(ix,iy,iz) = phi_guess(ix,iy,iz) +  b*(p%phi(ix+1,iy,iz) + p%phi(ix-1,iy,iz))
    end do
    end do
    end do
  endif

  ! Y-derivative
  if (m(2)%n > 1) then 
    b = - a / (m(2)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      phi_guess(ix,iy,iz) = phi_guess(ix,iy,iz) +  b*(p%phi(ix,iy+1,iz) + p%phi(ix,iy-1,iz))
    end do
    end do
    end do
  endif

  ! Z-derivative
  if (m(3)%n > 1) then 
    b = - a / (m(3)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      phi_guess(ix,iy,iz) = phi_guess(ix,iy,iz) +  b*(p%phi(ix,iy,iz+1) + p%phi(ix,iy,iz-1))
    end do
    end do
    end do
  endif

  ! Calculate error and copy solution to p%phi
  p%error(0) = 0.0 
  do iz=0,m(3)%n-1
  do iy=0,m(2)%n-1
  do ix=0,m(1)%n-1
    p%error(0) = max(p%error(0), abs(p%phi(ix,iy,iz) - phi_guess(ix,iy,iz)))
    p%phi(ix,iy,iz) = p%phi(ix,iy,iz)*(1.-w) + w * phi_guess(ix,iy,iz)
  end do
  end do
  end do

  p%error(0) = p%error(0) / abs(max_rho+1e-20)
  
  p%it(0) = p%it(0) + 1

END SUBROUTINE sor

!*******************************************************************************
SUBROUTINE jacobi(p,rho,aver_rho)
  implicit none
  type(poisson_t) :: p
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: rho
  real, dimension(:,:,:), allocatable :: phi_guess
  real :: a, b, aver_rho, max_rho
  integer :: ix,iy,iz
!...............................................................................  
  ! Jacobi solver; beware! Very slow.
  !
  ! Solve iteratively
  !
  ! (f^+x - 2 f + f^-x) / dx^2 + (f^+y - 2 f + f^-y) / dy^2 + (f^+z - 2 f + f^-z) / dz^2 = rho
  !
  call pull_selfgravity_boundaries(p)                                          ! upd bndrys now that we need them

  allocate(phi_guess(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1))

  ! 1. / Coefficient for central value of phi; each active dimension counts with 2 / ds^2
  a = 0.
  if (m(1)%n > 1) a = a - 2. / m(1)%d**2
  if (m(2)%n > 1) a = a - 2. / m(2)%d**2
  if (m(3)%n > 1) a = a - 2. / m(3)%d**2
  a = 1. / a

  ! Set values for phi_guess == left hand side of eq
  b = a*fourpi*G
  max_rho = 1e-20
  do iz=0,m(3)%n-1
  do iy=0,m(2)%n-1
  do ix=0,m(1)%n-1
    phi_guess(ix,iy,iz) = b*(rho(ix,iy,iz)-aver_rho)
    max_rho = max(max_rho, abs(b*(rho(ix,iy,iz)-aver_rho)))
  end do
  end do
  end do

  ! X-derivative
  if (m(1)%n > 1) then 
    b = - a / (m(1)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      phi_guess(ix,iy,iz) = phi_guess(ix,iy,iz) +  b*(p%phi(ix+1,iy,iz) + p%phi(ix-1,iy,iz))
    end do
    end do
    end do
  endif

  ! Y-derivative
  if (m(2)%n > 1) then 
    b = - a / (m(2)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      phi_guess(ix,iy,iz) = phi_guess(ix,iy,iz) +  b*(p%phi(ix,iy+1,iz) + p%phi(ix,iy-1,iz))
    end do
    end do
    end do
  endif

  ! Z-derivative
  if (m(3)%n > 1) then 
    b = - a / (m(3)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      phi_guess(ix,iy,iz) = phi_guess(ix,iy,iz) +  b*(p%phi(ix,iy,iz+1) + p%phi(ix,iy,iz-1))
    end do
    end do
    end do
  endif

  ! Calculate error and copy solution to p%phi
  p%error(0) = 0.0 
  do iz=0,m(3)%n-1
  do iy=0,m(2)%n-1
  do ix=0,m(1)%n-1
    p%error(0) = max(p%error(0), abs(p%phi(ix,iy,iz) - phi_guess(ix,iy,iz)))
    p%phi(ix,iy,iz) = phi_guess(ix,iy,iz)
  end do
  end do
  end do

  p%error(0) = p%error(0) / abs(max_rho+1e-20)
  
  p%it(0) = p%it(0) + 1

END SUBROUTINE jacobi

!*******************************************************************************
SUBROUTINE cg(p,rho,aver_rho)
  implicit none
  type(poisson_t) :: p
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: rho
  real :: aver_rho
  real :: a, alpha, beta, gamma_new
!...............................................................................  
  ! Conjugate Gridient Method. 
  !
  ! Do one iteration in iterative CG solver of the linear equation
  !
  ! Ax - B = 0, 
  !
  ! where B = 4 pi G rho, and A is the matrix corresponding to the laplace operator.
  !
  ! In both cases we rescale with -1/(2/dx^2 + 2/dy^2 + 2/dz^2)
  !
  ! The algorithm is:
  !   x_0: initial guess for solution
  !   (v,w) = v^T w == inner product for a set of vectors
  !
  !   r_0 = b - A x_0
  !   p_0 = r_0
  !   gamma_0 = (r_0,r_0)
  ! 
  !   while (error > tolerance)
  !     w = A p_k
  !     alpha = (r_k,r_k) / (p_k,w)
  !     x_k+1 = x_k + alpha p_k
  !     r_k+1 = r_k - alpha w
  !     gamma_k+1 = (r_k+1, r_k+1)
  !     beta = gamma_k+1 / gamma_k
  !     p_k+1 = r_k+1 + beta * p_k
  !     error = max(|r|) / max(|b|)
  !   endwhile
  !
  !  The error is chosen simply as the ratio between the max element of the
  !  residual r and of b, because we have renormalized A to be 1 on the diagonal
  !
  
  ! If iteration count is zero, initialize solution and return
  if (p%it(0)==0) then
    if (associated(p%p_cg)) deallocate(p%p_cg)
    allocate(p%r_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1), &
             p%x_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1), &
             p%w_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1), &
             p%p_cg(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) &
            )
    call pull_selfgravity_boundaries(p)                                        ! upd bndrys now that we need them
    p%x_cg = p%phi(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                           ! x0 = phi
    call spVM_A_set(p%phi,p%w_cg)                                              ! w = A x_0

    ! 1. / Coefficient for central value of phi; each active dimension counts with 2 / ds^2
    ! This way matrix has 1 in the diagonal
    a = 0.
    if (m(1)%n > 1) a = a - 2. / m(1)%d**2
    if (m(2)%n > 1) a = a - 2. / m(2)%d**2
    if (m(3)%n > 1) a = a - 2. / m(3)%d**2
    a = 1. / a

    p%r_cg = a * fourpi * G * (rho(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)-aver_rho) ! r = b
    p%max_rho = maxval(p%r_cg)                                                 ! store max(b) for error estimate
    p%r_cg = p%r_cg - p%w_cg                                                   ! r = b - Ax_0
    p%p_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1) = p%r_cg                          ! store inner part
    p%gamma_cg = sum(p%r_cg*p%r_cg)                                            ! gamma = (r,r)

    p%it(0) = p%it(0) + 1                                                      ! increase iteration count to signal everything is done

    ! CALL UPDATE_WINDOW(p%it(0), p%p_cg)                                      ! make sure remote view of p and it(0) are updated

    return                                                                     ! everything is setup; wait for neighbors to calc p
  endif

  if (abs(p%gamma_cg)<1e-20) then                                              ! gamma=0 implies alpha=0, i.e. we have the correct solution
    p%error(0) = 0.
    p%it(0) = p%it(0) + 1
    return
  endif
  
  call boundary(p%p_cg)                                                        ! Update boundaries for p

  call spVM_A_set(p%p_cg,p%w_cg)                                               ! w = A p
  alpha = p%gamma_cg / sum(p%p_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1) * p%w_cg)  ! alpha = gamma / (p, w)
  p%x_cg =  p%x_cg + alpha * p%p_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)          ! x = x + alpha * p
  p%r_cg =  p%r_cg - alpha * p%w_cg                                            ! r = r - alpha * w
  gamma_new = sum(p%r_cg*p%r_cg)                                               ! gamma = (r,r)
  beta = gamma_new / p%gamma_cg                                                ! beta = gamma / gamma_old
  p%gamma_cg = gamma_new                                                       ! upd gamma to new value
  p%p_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1) = p%r_cg + p%p_cg(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1) ! p = r + beta*p

  p%error(0) = maxval(abs(p%r_cg)) / abs(p%max_rho+1e-20)                      ! The residual is r = b - Ax (computed recursively), compare to max(b)
  
  p%it(0) = p%it(0) + 1

  !print *, "CG :", p%it(0), p%error(0), maxval(abs(p%r_cg)), abs(p%max_rho+1e-20), sqrt(p%gamma_cg / product(m%n))

  ! CALL UPDATE_WINDOW(p%it(0), p%p_cg)                                        ! make sure remote view of p and it(0) are updated

END SUBROUTINE cg
! Apply the (renormalized) laplace operator to a set of points
!
! Let x = (a Nabla) y, where a is chosen such that the diagonal is one
!*******************************************************************************
SUBROUTINE spVM_A_set(x,y)
  implicit none
  real, dimension(m(1)%lb:m(1)%ub,m(2)%lb:m(2)%ub,m(3)%lb:m(3)%ub) :: x
  real, dimension(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                :: y
  !
  real    :: a, b
  integer :: ix, iy, iz
  !
  ! 1. / Coefficient for central value of phi; each active dimension counts with 2 / ds^2
  a = 0.
  if (m(1)%n > 1) a = a - 2. / m(1)%d**2
  if (m(2)%n > 1) a = a - 2. / m(2)%d**2
  if (m(3)%n > 1) a = a - 2. / m(3)%d**2
  a = 1. / a

  ! Set central value
  do iz=0,m(3)%n-1
  do iy=0,m(2)%n-1
  do ix=0,m(1)%n-1
    y(ix,iy,iz) = x(ix,iy,iz)
  end do
  end do
  end do

  ! X-derivative
  if (m(1)%n > 1) then 
    b = a / (m(1)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      y(ix,iy,iz) = y(ix,iy,iz) +  b*(x(ix+1,iy,iz) + x(ix-1,iy,iz))
    end do
    end do
    end do
  endif

  ! Y-derivative
  if (m(2)%n > 1) then 
    b = a / (m(2)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      y(ix,iy,iz) = y(ix,iy,iz) +  b*(x(ix,iy+1,iz) + x(ix,iy-1,iz))
    end do
    end do
    end do
  endif

  ! Z-derivative
  if (m(3)%n > 1) then 
    b = a / (m(3)%d)**2
    do iz=0,m(3)%n-1
    do iy=0,m(2)%n-1
    do ix=0,m(1)%n-1
      y(ix,iy,iz) = y(ix,iy,iz) +  b*(x(ix,iy,iz+1) + x(ix,iy,iz-1))
    end do
    end do
    end do
  endif
  
END SUBROUTINE spVM_A_set

!*******************************************************************************
SUBROUTINE pull_neighbor_error(p)
  implicit none
  type(poisson_t) :: p
  integer, dimension(0:n_neighbor) :: it_neighbor
  real,    dimension(0:n_neighbor) :: error_n                                   ! max error from n_neighbor patches away
!...............................................................................  
  ! Update error count from nearby patches:
  !
  ! 1) MPI_get it_neighbor and error_n from all neigbor patches
  ! 2) chop off last index, and update local versions
  !
  ! NOT IMPLEMENTED YET - USE LOCAL VALUES
  !
  p%it(1:n_neighbor)    = p%it(0)
  p%error(1:n_neighbor) = p%error(0)
END SUBROUTINE pull_neighbor_error

!*******************************************************************************
SUBROUTINE pull_selfgravity_boundaries(p)
  USE boundaries, only : boundary
  implicit none
  type(poisson_t) :: p
!...............................................................................  
  ! Update boundaries for phi
  call boundary(p%phi)
END SUBROUTINE pull_selfgravity_boundaries

!*******************************************************************************
SUBROUTINE selfgravity_force (f,d,p)
  USE mhd_mod,     only : mhd_t
  USE operators,   only : ddx,ddy,ddz,scalar_stats
  implicit none
  type(mhd_t), pointer:: f, d
  type(poisson_t), pointer:: p
!...............................................................................
  if (.not. selfgravity) return
  call scalar_stats (f%d,   'force:rho')
  call scalar_stats (p%phi, 'force:phi')
  ! the graviational acceleration is given as  - rho * grad(phi)
  call scalar_stats (d%p%x, 'force:px')
  call scalar_stats (d%p%y, 'force:py')
  call scalar_stats (d%p%z, 'force:pz')
  d%p%x = d%p%x - f%d * ddx(p%phi)
  d%p%y = d%p%y - f%d * ddy(p%phi)
  d%p%z = d%p%z - f%d * ddz(p%phi)
  ! no call to boundary update; done in the PDE
END SUBROUTINE selfgravity_force

END MODULE poisson_mod
