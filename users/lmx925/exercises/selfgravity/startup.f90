!*******************************************************************************
! We arrive here with data structures in place, and with f a pointer to the
! MHD variables we should fill in
!*******************************************************************************
SUBROUTINE startup (f, uploadid)
  USE mhd_mod,         only: mhd_t
  USE vector_mod,      only: vector_t, boundary_vector                          ! vector operators
  USE boundaries,      only: init_boundary, boundary                            ! ghost zone handling
  USE velocity_fields, only: init_velocity                                      ! initial velocities
  USE mesh_mod,        only: m, init_mesh
  USE pde_mod,         only: gamma
  USE io
  implicit none
  type(mhd_t):: f                                                               ! variables
  character(len=mch), save:: id= &
    'amr_code.f90 $Id$'
  integer:: ix, iy, iz                                                          ! loop indices
  real:: width=10.0, density=1.0, pgas=1.0, entropy=-1.0                        ! Gaussian, density, entropy
  real:: omega=0.0, be_density=10.
  real::  u(3) = (/1.0,0.5,0.0/)                                                ! velocity components
  real:: u0(3) = (/0.0,0.0,0.0/)                                                ! patch speed
  character(len=16):: type='advect', dtype='gauss'
  integer:: uploadid
  namelist /init_params/ width, density, entropy, u, u0, uploadid, type, dtype, omega, be_density, pgas  ! input namelist
!-------------------------------------------------------------------------------
! Initialize
!-------------------------------------------------------------------------------
  call trace_begin ('startup')
  call print_id(id)                                                             ! identify version
!-------------------------------------------------------------------------------
! Setup initial conditions
!-------------------------------------------------------------------------------
  rewind (input); read (input,init_params); if (master) write(*,init_params)    ! read namelist input
  call trace('1')
  f%u0 = u0
  call init_velocity  (f%p, u-u0, type)
  select case (trim(dtype))
  case('BE')
    f%d = density
    do iz=0,m(3)%n-1; do iy=0,m(2)%n-1; do ix=0,m(1)%n-1                          ! loop over 3-D
        if ((m(1)%r(ix)**2+m(2)%r(iy)**2+m(3)%r(iz)**2)/width**2<=1.) f%d(ix,iy,iz) = be_density
        f%s(ix,iy,iz) = f%d(ix,iy,iz)*entropy                                          ! entropy per unit vol
        f%s(ix,iy,iz) = f%d(ix,iy,iz)*alog(pgas / f%d(ix,iy,iz)**(gamma-1)) / (gamma-1)    ! constant pressure
        f%p%x(ix,iy,iz) = f%d(ix,iy,iz)*f%p%x(ix,iy,iz)                           ! momentum
        f%p%y(ix,iy,iz) = f%d(ix,iy,iz)*f%p%y(ix,iy,iz)
        f%p%z(ix,iy,iz) = f%d(ix,iy,iz)*f%p%z(ix,iy,iz)
    end do; end do; end do
  case('gauss')
    do iz=0,m(3)%n-1; do iy=0,m(2)%n-1; do ix=0,m(1)%n-1                          ! loop over 3-D
        f%d(ix,iy,iz) = density*exp(-(m(1)%r(ix)**2+m(2)%r(iy)**2+m(3)%r(iz)**2)/width**2) ! gaussian profile
        f%s(ix,iy,iz) = f%d(ix,iy,iz)*alog(pgas / f%d(ix,iy,iz)**gamma) / (gamma-1)    ! constant pressure
        f%s(ix,iy,iz) = f%d(ix,iy,iz)*entropy                                          ! entropy per unit vol
      f%p%x(ix,iy,iz) = f%d(ix,iy,iz)*f%p%x(ix,iy,iz)                                   ! momentum
      f%p%y(ix,iy,iz) = f%d(ix,iy,iz)*f%p%y(ix,iy,iz)
      f%p%z(ix,iy,iz) = f%d(ix,iy,iz)*f%p%z(ix,iy,iz)
    end do; end do; end do
  case('truelove')
    do iz=0,m(3)%n-1; do iy=0,m(2)%n-1; do ix=0,m(1)%n-1                          ! loop over 3-D
        f%d(ix,iy,iz) = density*exp(-(m(1)%r(ix)**2+m(2)%r(iy)**2+m(3)%r(iz)**2)/width**2) !* & ! gauus + perturbation
           !(1. + 0.1 * cos(2. * atan2(m(2)%r(iy), m(1)%r(ix))))
        f%s(ix,iy,iz) = f%d(ix,iy,iz)*entropy                                          ! entropy per unit vol
        f%s(ix,iy,iz) = f%d(ix,iy,iz)*alog(pgas / f%d(ix,iy,iz)**gamma) / (gamma-1)    ! constant pressure
      f%p%x(ix,iy,iz) = f%d(ix,iy,iz)*(- omega * m(2)%r(iy))                    ! add rotation
      f%p%y(ix,iy,iz) = f%d(ix,iy,iz)*(  omega * m(1)%r(ix))
      f%p%z(ix,iy,iz) = 0.
    end do; end do; end do
  case default
    print*,'unknown density type',type
  end select
  do iz=0,m(3)%n-1; do iy=0,m(2)%n-1; do ix=0,m(1)%n-1                          ! loop over 3-D
    f%B%z(ix,iy,iz) = 0.0              ! Gaussian Bz(x,y), centered
  end do; end do; end do
  call trace('3')
  call boundary       (f%d)
  call boundary       (f%s)
  call boundary_vector(f%p)
  call boundary_vector(f%B)
  call trace('4')
  f%it = 0                                                                      ! time step
  f%t  = 0.0                                                                    ! model time
  f%dt = 1.0                                                                    ! model dt
  f%m  = m                                                                      ! model mesh
  call trace('5')
  if (do_trace) print'(i8,a,3f10.6,f12.6)', &
    mpi_rank, ' made initial condition for patch at pos, size =', &
    m%pos, m(1)%s
  call trace_end
END SUBROUTINE startup
