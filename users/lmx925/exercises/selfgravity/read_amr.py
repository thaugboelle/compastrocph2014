""" Based on a Mayavi example
"""
import time
import numpy as np
from scipy import special
from numpy import fromfile, float32, prod, size
#from mayavi import mlab
import matplotlib.pyplot as plt
from matplotlib import cm

# Open a figure
#fig = mlab.figure(1, size=(800,800), bgcolor=(1,1,1), fgcolor=(0,0,0))

# Read the data (you may want to have a for loop here)
n=64
shape=(n,n,n)
taskid=0
j = 1
for i in range(52):
    filename='id={:0=7d}_{:0=5d}.dat'.format(taskid,i)
    print(filename)
    fd=open(filename,'rb')
    print "Reading dd"
    dd=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    print "Reading ds"
    ds=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Ux=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uy=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bx=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    By=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Phi=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Pgas=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    dd=np.transpose(dd)
    im = np.log(dd[:,:,32])
    # Show a central slice of log(density)
    
    if (i==0):
        fig, axes = plt.subplots(2,3)
        cax = axes[0,0].imshow(im)
        cbar = plt.colorbar(cax)
        axes[0,0].set_title('Initial - log(density)')

    if i==10:
        cax = axes[0,1].imshow(im)
        axes[0,1].set_title('Step ' +str(i) )
    
    if i==20:
        cax = axes[0,2].imshow(im)
        axes[0,2].set_title('Step ' +str(i) )
    
    if i==30:
        cax = axes[1,0].imshow(im)
        axes[1,0].set_title('Step ' +str(i) )
    
    if i==40:
        cax = axes[1,1].imshow(im)
        axes[1,1].set_title('Step ' +str(i) )    
        
    if i==50:
        cax = axes[1,2].imshow(im)
        axes[1,2].set_title('Step ' +str(i) )
    #cax.set_data(im)
    

    # Add colorbar, make sure to specify tick locations to match desired ticklabels
    
    #cbar.ax.set_yticklabels(['< -1', '0', '> 1'])# vertically oriented colorbar
    plt.savefig('Test.png')
    plt.show()
    #if (i==1):
    #    s,=imshow(log(dd[:,:,32]))
    #s.set_data(log(dd[:,:,32]))
