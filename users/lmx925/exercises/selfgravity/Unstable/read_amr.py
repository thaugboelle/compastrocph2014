""" Based on a Mayavi example
"""
import time
import numpy as np
from scipy import special
from numpy import fromfile, float32, prod, size
from matplotlib.pyplot import imshow, show, figure, title
import matplotlib.animation as animation

# Read the data (you may want to have a for loop here)
n=64
shape=(n,n,n)
taskid=0

def read(filename, shape):
    fd=open(filename,'rb')
    dd=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    ds=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Ux=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uy=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Uz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bx=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    By=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Bz=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Phi=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    Pgas=fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
    dd=np.transpose(dd)
    im = np.log(dd[:,:,32])
    return im

def update(*args):
    global image, i
    i = i + 1
    if(i>51): return
    filename='id={:0=7d}_{:0=5d}.dat'.format(taskid,i)
    im = read(filename, shape)
    image.set_data(im)
i=0
filename='id={:0=7d}_{:0=5d}.dat'.format(taskid,i)
fig = figure()
im = read(filename, shape)
# Show a central slice of log(density)
image = imshow(im)
fig.colorbar(image)
title("Bonnor Ebert sphere - log(density)")
ani = animation.FuncAnimation(fig, update, interval=20, blit=False)
show()
