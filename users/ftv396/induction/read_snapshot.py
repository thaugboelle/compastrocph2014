""" read and show advection data """
from numpy import reshape, fromfile, float32, prod
from matplotlib.pyplot import imshow, show, figure, close
import matplotlib.animation as animation
import os

close('all')

shape = (64,64)
fd = open('snapshot.dat', 'rb')

fig=figure()

data = fromfile(file=fd, dtype=float32, count=prod(shape)).reshape(shape)
im = imshow(data,origin='lower')

def update(*args):
  global im
  data = fromfile(file=fd, dtype=float32, count=prod(shape))
  if(data.shape==(0,)): return
  data = data.reshape(shape)
  im.set_data(data)

ani = animation.FuncAnimation(fig, update, interval=1, blit=False)
show()