!**********************************
! error/deviation from reference value: 0.00013 % 
! speedup from 1 thread to 4 threads: 276 %
!*******************************************************************************
PROGRAM pi
  implicit none
  integer i 
  integer, parameter :: n_steps = 100000000
  real, parameter :: pi_ref = 3.14159265359
  real :: pi_cal

!-------------------------------
! The following loop will calculate pi
!-------------------------------

!$omp parallel default(none) &
!$omp shared(pi_cal) private(i)

!$omp single
pi_cal = 0
!$omp end single

!$omp do reduction(+: pi_cal)
do i=0,n_steps
		pi_cal = pi_cal + 4.*(-1.)**i/(2.*i+1.)
end do
!$omp end do
!$omp end parallel
 
print *, 'number of steps = ', n_steps
print *, 'calculated pi = ', pi_cal
print *, 'reference pi = ', pi_ref



END PROGRAM
