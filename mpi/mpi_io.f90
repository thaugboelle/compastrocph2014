! $Id$
!*******************************************************************************
MODULE io
  USE mpi_base
  implicit none
PUBLIC
  integer, private:: handle                                                     ! file handle
  integer, private:: filetype                                                   ! file type
  integer, private:: status(MPI_STATUS_SIZE)                                    ! file status
  integer(kind=MPI_OFFSET_KIND), private:: pos                                  ! position offset into file
  integer(kind=MPI_OFFSET_KIND), private:: bytesize                             ! bytes per record
  character(len=mch), dimension(30), private:: tracing                          ! active routine
  integer, private:: indent=4                                                   ! trace indent
  integer, private:: level=1                                                    ! trace level
  character(len=mch), private:: fmt                                             ! trace format
!PUBCLIC PART
  character(len=mch):: inputfile                                                ! trace format
  integer:: input=1                                                             ! input file unit
  logical:: do_trace=.false.                                                    ! trace execution
  logical:: do_debug=.false.                                                    ! debug execution
CONTAINS

!*******************************************************************************
SUBROUTINE init_io
  call getarg(1,inputfile)                                                      ! 1st command line argument
  if (inputfile==' ') inputfile = 'input.nml'                                   ! default = input.nml
  open (input, file=inputfile, form='formatted', status='old')                  ! open for reading
  if (do_debug)    do_trace=.true.
  if (.not.master) do_trace=.false.
END SUBROUTINE init_io

!*******************************************************************************
SUBROUTINE print_id (id)
  implicit none
  character(len=mch) id
  character(len=mch), save:: hl= &
    '--------------------------------------------------------------------------------'
!..............................................................................
  if (id .ne. ' ' .or. do_trace .or. do_debug) then
    if (master) then
      print'(a)',trim(hl)
      print'(1x,a,f12.2)', trim(id), wallclock()
    end if
    if (.not.do_trace) id = ' '
  end if
END SUBROUTINE print_id

!*******************************************************************************
SUBROUTINE trace_begin (id)
  implicit none
  character(len=*) id
  if (do_trace .or. do_debug) then
    if (master) then
      write(fmt,'(a,i3.3,a)') '("trace:",f12.2,',indent,'x,a)'
      write(*,fmt) wallclock(), trim(id)//' begin'
      tracing(level) = trim(id)
      indent = indent + 3
      level = level+1
    end if
  end if
END SUBROUTINE trace_begin

!*******************************************************************************
SUBROUTINE trace (id)
  implicit none
  character(len=*) id
  if (do_trace .or. do_debug) then
    if (master) then
      write(fmt,'(a,i3.3,a)') '("trace:",f12.2,',indent,'x,a)'
      write(*,fmt) wallclock(), trim(tracing(level-1))//' '//trim(id)
    end if
  end if
END SUBROUTINE trace

!*******************************************************************************
SUBROUTINE trace_end
  implicit none
  integer i
  if (do_trace .or. do_debug) then
    if (master) then
      level  = max(level-1,1)
      indent = max(indent-3,1)
      write(fmt,'(a,i3.3,a)') '("trace:",f12.2,',indent,'x,a)'
      write(*,fmt) wallclock(), trim(tracing(level))//' end'
    end if
  end if
END SUBROUTINE trace_end

!*******************************************************************************
SUBROUTINE file_open_mpi (filename, dd, d, offset, mode)
  implicit none
  integer:: mode
  character(len=*):: filename
  integer:: d(3)                                                                ! local dimensions
  integer:: dd(3)                                                               ! global dimensions
  integer:: offset(3)                                                           ! offset into array
!...............................................................................
  bytesize = 4_8*product(int(dd,kind=8))                                        ! record size in bytes
  CALL MPI_File_open (mpi_comm, filename, mode, MPI_INFO_NULL, handle, mpi_err) ! open file
  CALL MPI_Type_create_subarray (3, dd, d, offset, MPI_ORDER_FORTRAN, &         ! make filetype
                                 MPI_REAL, filetype, mpi_err)   
  CALL MPI_Type_commit (filetype, mpi_err)                                      ! commit filetype
END SUBROUTINE file_open_mpi

!*******************************************************************************
SUBROUTINE file_openw_mpi (filename, dd, d, offset)
  implicit none
  integer, dimension(3):: dd, d, offset
  character(len=*) filename
!...............................................................................
  call file_open_mpi (filename, dd, d, offset, MPI_MODE_CREATE+MPI_MODE_RDWR)   ! open read-write
END SUBROUTINE file_openw_mpi

!*******************************************************************************
SUBROUTINE file_openr_mpi (filename, dd, d, offset)
  implicit none
  integer, dimension(3):: dd, d, offset
  character(len=*) filename
!...............................................................................
  call file_open_mpi (filename, dd, d, offset, MPI_MODE_RDONLY)                 ! open read-only
END SUBROUTINE file_openr_mpi

!*******************************************************************************
SUBROUTINE file_write_mpi (f, d, rec)
  implicit none
  integer:: d(3), rec
  real, dimension(d(1),d(2),d(3)):: f
!...............................................................................
  pos = rec*bytesize                                                            ! position in file
  CALL MPI_File_set_view (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  CALL MPI_File_write_all (handle, f , product(d), MPI_REAL, status, mpi_err)   ! collective write
END SUBROUTINE file_write_mpi

!*******************************************************************************
SUBROUTINE file_read_mpi (f, d, rec)
  implicit none
  integer:: d(3), rec
  real, dimension(d(1),d(2),d(3)):: f
!...............................................................................
  pos = rec*bytesize                                                            ! position in file
  CALL MPI_FILE_SET_VIEW (handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, mpi_err)   
  CALL MPI_FILE_READ_ALL(handle, f , product(d), MPI_REAL, status, mpi_err)     ! collective read
END SUBROUTINE file_read_mpi

!*******************************************************************************
SUBROUTINE file_close_mpi
  implicit none
!...............................................................................
  CALL MPI_File_close (handle, mpi_err)                                         ! close the file
  CALL MPI_Type_free  (filetype, mpi_err)                                       ! commit filetype
END SUBROUTINE file_close_mpi

END MODULE io
