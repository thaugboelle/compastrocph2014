
# Any variable that is assigned to or changed inside a function is local, independent of 
# whether it is defined when the function is defined.  A variable that is only referenced 
# is considered global, even if not declared global.  It needs only to be define before 
# the function CALL (not definition).  For increased clarity it is advisable to always 
# declare global variable, or use function arguments (but they cannot change the calling
# value; that requires using "return"
i=0
def f(j):
    global i
    print s
    i=i+1
    j=j+2
    return(j)
s="I'm global by default, but declaring me global makes the code more clear"
k=1
n=f(k)
print i,k,n             # prints "1 1 3"
