!*******************************************************************************
! Vector routines
!*******************************************************************************
MODULE vector_mod
  USE mesh_mod,   only: m
  USE boundaries, only: boundary
  USE operators,  only: ddx, ddy, ddz, ddx_add, ddy_add, ddz_add
PUBLIC
  type :: vector_t
    logical:: is_allocated =.false.
    real, dimension(:,:,:), pointer :: x, y, z
    integer, dimension(3) :: dim
  end type
CONTAINS
!*******************************************************************************
SUBROUTINE allocate_scalar (a)
  implicit none
  real, dimension(:,:,:), pointer:: a
!...............................................................................
  !call trace_begin('allocate_scalar')
  allocate (a(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub))
  a = 0.0
  !call trace_end
END SUBROUTINE allocate_scalar
!*******************************************************************************
SUBROUTINE deallocate_scalar (a)
  real, dimension(:,:,:), pointer:: a
  deallocate (a)
END SUBROUTINE deallocate_scalar
!*******************************************************************************
SUBROUTINE allocate_vector (a)
  implicit none
  type(vector_t):: a
!...............................................................................
  if (a%is_allocated) return
  !call trace_begin('allocate_vector')
  call allocate_scalar (a%x)
  call allocate_scalar (a%y)
  call allocate_scalar (a%z)
  a%dim(1) = m(1)%ub - m(1)%lb + 1
  a%dim(2) = m(2)%ub - m(2)%lb + 1
  a%dim(3) = m(3)%ub - m(3)%lb + 1
  a%is_allocated = .true.
  !call trace_end
END SUBROUTINE allocate_vector
!*******************************************************************************
SUBROUTINE deallocate_vector (a)
  implicit none
  type(vector_t):: a
!...............................................................................
  if (.not. a%is_allocated) return
  deallocate (a%x, a%y, a%z)
  a%is_allocated = .false.
END SUBROUTINE deallocate_vector
!*******************************************************************************
SUBROUTINE boundary_vector (a)
  implicit none
  type(vector_t):: a
!...............................................................................
  call boundary(a%x)
  call boundary(a%y)
  call boundary(a%z)
END SUBROUTINE boundary_vector
!*******************************************************************************
SUBROUTINE cross_sub(a,b,c)
  implicit none
  type(vector_t):: a, b, c
  integer iz
!...............................................................................
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    c%x(:,:,iz) = a%y(:,:,iz)*b%z(:,:,iz) - a%z(:,:,iz)*b%y(:,:,iz)
    c%y(:,:,iz) = a%z(:,:,iz)*b%x(:,:,iz) - a%x(:,:,iz)*b%z(:,:,iz)
    c%z(:,:,iz) = a%x(:,:,iz)*b%y(:,:,iz) - a%y(:,:,iz)*b%x(:,:,iz)
  end do
END SUBROUTINE cross_sub
!*******************************************************************************
FUNCTION dot(a,b) RESULT(c)
  implicit none
  type(vector_t):: a, b
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: c
!...............................................................................
  integer iz
!...............................................................................
  !$omp parallel do private(iz)
  do iz=m(3)%lb,m(3)%ub
    c(:,:,iz) = a%x(:,:,iz)*b%x(:,:,iz) &
              + a%y(:,:,iz)*b%y(:,:,iz) &
              + a%z(:,:,iz)*b%z(:,:,iz)
  end do
END FUNCTION dot
!*******************************************************************************
SUBROUTINE curl_minus (a,b)
  implicit none
  type(vector_t):: a, b
!...............................................................................
  call ddy_add (a%z, b%x, -1.0); call ddz_add (a%y, b%x, +1.0)
  call ddz_add (a%x, b%y, -1.0); call ddx_add (a%z, b%y, +1.0)
  call ddx_add (a%y, b%z, -1.0); call ddy_add (a%x, b%z, +1.0)
  call boundary_vector(b)
END SUBROUTINE curl_minus
!*******************************************************************************
SUBROUTINE div_minus (a,b)
  implicit none
  type(vector_t):: a
  real, dimension(m(1)%lb:m(1)%ub, m(2)%lb:m(2)%ub, m(3)%lb:m(3)%ub) :: b
!...............................................................................
  call ddx_add (a%x, b, -1.0)
  call ddy_add (a%y, b, -1.0)
  call ddz_add (a%z, b, -1.0)
  call boundary(b)
END SUBROUTINE div_minus
END MODULE vector_mod
