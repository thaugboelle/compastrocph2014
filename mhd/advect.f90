!*******************************************************************************
MODULE solver
  implicit none
  private
  !----------------------------------------------------------------------------
  ! Default values for the setup
  !----------------------------------------------------------------------------
  integer, dimension(3), public :: gn = (/ 32, 32, 32 /)                       ! Global mesh size
  integer, dimension(3), public :: n                                           ! Local mesh size
  integer, dimension(3), public :: n_offset                                    ! offset of local mesh patch
  real,    dimension(3), public :: s = (/ 1., 1., 1. /)                        ! Physical box size
  real,    dimension(3), public :: u = (/ .5, 0., 0. /)                        ! Advection velocity
  real,                  public :: dt= 0.1                                     ! Timestep

  real, pointer, dimension(:,:,:), public :: rho                           ! density

  type, public :: mesh_t
    real  :: d                                                                 ! cell size
    real, pointer, dimension(:) :: r                                       ! coordinate axis
  end type
  type(mesh_t), dimension(3), public :: m                                      ! mesh point coordinates 
  public  :: init_mesh, solve
  !
  integer :: nghost=2                                                          ! Number of ghost-cells
  integer :: nlb(3), nub(3)                                                    ! Boundaries of mesh incl ghost zones                
CONTAINS
! Setup mesh, coordinate axes, and allocate memory
!*******************************************************************************
SUBROUTINE init_mesh
  USE mpi_coords
  implicit none
  !----------------------------------------------------------------------------
  ! Default values for the setup
  !----------------------------------------------------------------------------
  real    :: rho0 = 1.0                                                        ! Average density
  real    :: width= 10.                                                        ! width of gaussian
  !
  real, parameter :: pi = 3.141592653589793
  integer, dimension(3) :: mpi_dim
  integer :: idim, i, ix, iy, iz

  !-----------------------------------------------------------------------------
  ! Setup MPI with a cartesian MPI arrangement
  !-----------------------------------------------------------------------------
  n = gn / mpi_dims
  if (any(n*mpi_dims .ne. gn)) then
    if (master) then
      print *, 'ERROR mpi_dims has to be a divisor in global mesh size' 
      print *, 'Global mesh size :', gn
      print *, 'Local mesh size  :', n
      print *, 'MPI dimensions   :', mpi_dims
      print *, 'n * mpi_dims     :', mpi_dims*n
    endif
    call end_mpi
  else
    call cart_create_mpi (mpi_dims, gn)                                         ! make cartesian
  endif
  mpi_dim = mpi_dims

  n_offset = mpi_coord*n                                                        ! offset global mesh

  !----------------------------------------------------------------------------
  ! Point interval is [-nghost : n + nghost - 1], physical interval is [0:n-1]
  !----------------------------------------------------------------------------

  ! Compute nlb and nub
  nlb = -nghost
  nub = n + nghost - 1
  do idim=1,3
    if (n(idim)==1) then
      nlb(idim) = 0
      nub(idim) = 0
    endif
  enddo

  ! Allocate data structures
  allocate(rho(nlb(1):nub(1),nlb(2):nub(2),nlb(3):nub(3)))

  !----------------------------------------------------------------------------
  ! Set up initial mesh. The mesh point position is at the center of the cell
  !----------------------------------------------------------------------------
  do idim=1,3
    allocate(m(idim)%r(nlb(idim):nub(idim)))                                   ! Allocate coordinate axis
    m(idim)%d = s(idim) / gn(idim)                                             ! Cell size

    ! Physical domain
    do i=0, n(idim) - 1
      m(idim)%r(i) = m(idim)%d * real((n_offset(idim) + i + 0.5),kind=16)      ! Coordinate of cell center
    enddo
  enddo

  !----------------------------------------------------------------------------
  ! Setup up a gaussian with width "width"
  !----------------------------------------------------------------------------
  do iz=0,n(3)-1
  do iy=0,n(2)-1
  do ix=0,n(1)-1
    rho(ix,iy,iz) = rho0 * exp( - ( (m(1)%r(ix) - s(1)/2.0)**2 + &
                                    (m(2)%r(iy) - s(2)/2.0)**2 + &
                                    (m(3)%r(iz) - s(3)/2.0)**2 ) / width**2 &
                              )
  enddo
  enddo
  enddo

  call boundary(rho)
  
END SUBROUTINE init_mesh
! Solve the equations of motion for a single timestep
!*******************************************************************************
SUBROUTINE solve
  implicit none

  real, dimension(nlb(1):nub(1),nlb(2):nub(2),nlb(3):nub(3)) :: divu
  !
  ! Calculate time derivative
  !
  divu = div(rho*u(1), rho*u(2), rho*u(3))
  call boundary(divu)
  !
  rho = rho - divu * dt
  !
END SUBROUTINE solve
! Boundary conditions -- assume periodic
!*******************************************************************************
SUBROUTINE boundary(f)
  implicit none
  real, dimension(nlb(1):nub(1),nlb(2):nub(2),nlb(3):nub(3)) :: f
  integer :: idim, ix, iy, iz, lb(3), ub(3), offset(3)
  !
  do idim=1,3
    ! If there is only a single mesh point in this direction, it is passive. Cycle loop
    if (n(idim)==1) cycle

    ! Wrapping offset for periodic boundaries
    ! Data has to be fetched a box length away
    offset = 0
    offset(idim) = n(idim)
    !
    ! Lower boundary
    !
    lb = nlb
    ub = nub
    ub(idim) = -1 ! only update boundary zones in lower part
    do iz=lb(3),ub(3) 
    do iy=lb(2),ub(2) 
    do ix=lb(1),ub(1) 
      f(ix,iy,iz) = f(ix + offset(1), iy + offset(2), iz + offset(3))
    enddo
    enddo
    enddo
    !
    ! Upper boundary
    !
    lb = nlb
    ub = nub
    lb(idim) = n(idim) ! only update boundary zones in upper part
    do iz=lb(3),ub(3) 
    do iy=lb(2),ub(2) 
    do ix=lb(1),ub(1) 
      f(ix,iy,iz) = f(ix - offset(1), iy - offset(2), iz - offset(3))
    enddo
    enddo
    enddo
  end do
  !
END SUBROUTINE boundary
! Divergence operator -- used centered finite difference
!*******************************************************************************
FUNCTION div(vx,vy,vz)
  implicit none
  real, dimension(nlb(1):nub(1),nlb(2):nub(2),nlb(3):nub(3)) :: div
  real, dimension(nlb(1):nub(1),nlb(2):nub(2),nlb(3):nub(3)) :: vx,vy,vz
  integer :: idim, ix, iy, iz
  real    :: ax, bx, ay, by, az, bz
  !
  div = 0.0
  ax = 8. / (12.*m(1)%d); bx = -1. / (12.*m(1)%d)
  ay = 8. / (12.*m(2)%d); by = -1. / (12.*m(2)%d)
  az = 8. / (12.*m(3)%d); bz = -1. / (12.*m(3)%d)
  !
  if (n(1) > 1) then ! x-derivative only non-zero iff x-dimension larger than 1
    do iz=0,n(3)-1
    do iy=0,n(2)-1
    do ix=0,n(1)-1
      div(ix,iy,iz) = ax*(vx(ix+1,iy,iz) - vx(ix-1,iy,iz)) &
                    + bx*(vx(ix+2,iy,iz) - vx(ix-2,iy,iz))                     ! d(vx)/dx
    end do
    end do
    end do
  endif
  if (n(2) > 1) then
    do iz=0,n(3)-1
    do iy=0,n(2)-1
    do ix=0,n(1)-1
      div(ix,iy,iz) = div(ix,iy,iz) + &
                    + ay*(vy(ix,iy+1,iz) - vy(ix,iy-1,iz)) &
                    + by*(vy(ix,iy+2,iz) - vy(ix,iy-2,iz))                     ! d(vy)/dy
    end do
    end do
    end do
  endif
  if (n(3) > 1) then
    do iz=0,n(3)-1
    do iy=0,n(2)-1
    do ix=0,n(1)-1
      div(ix,iy,iz) = div(ix,iy,iz) + &
                    + az*(vz(ix,iy,iz+1) - vz(ix,iy,iz-1)) &
                    + bz*(vz(ix,iy,iz+2) - vz(ix,iy,iz-2))                     ! d(vz)/dz
    end do
    end do
    end do
  endif
  !
END FUNCTION div
!
END MODULE solver
!*******************************************************************************
