from math import *
from scipy.integrate import odeint
from random import random as rand
import numpy as np
import sys

######CONSTANTS######
spy = 365e0*24e0*3600e0 #seconds per year
boltzmann_constant = 1.3806488e-16 #erg/K
idx_Oj = 0 #index O+
idx_H2 = 1 #index H2
idx_OHj = 2 #index OH+
idx_H = 3 #index H
idx_H2Oj = 4 #index H2O+
idx_H3Oj = 5 #index H3O+
idx_E = 6 #index E
idx_H2O = 7 #index H2O
idx_OH = 8 #index OH
idx_O = 9 #index O
idx_Tgas = 10 #index for Tgas
number_of_species = 11 #number of species
number_of_reactions = 8 #number of reactions

######FUNCTIONS START HERE######
#returns rate coefficients
def get_coefficients(Tgas):
	ionizing_cosmic_ray_flux = 1.3e-17 #1/s
	k = [0e0 for x in range(number_of_reactions)] #initialize coefficients
	invsqrt = 1e0/sqrt(Tgas/3e2)
	k[0] = 1.6e-9 #O+ + H2 -> OH+ + H
	k[1] = 1.1e-9 #OH+ + H2 -> H2O+ + H
	k[2] = 6.1e-10 #H2O+ + H2 -> H3O+ + H
	k[3] = 1.1e-7*invsqrt #H3O+ + E -> H2O + H
	k[4] = 8.6e-8*invsqrt #H2O+ + E -> OH + H
	k[5] = 3.9e-8*invsqrt #H2O+ + E -> O + H2
	k[6] = 6.3e-9*(Tgas/3e2)**(-.48) #OH+ + E -> O + H
	k[7] = 2.8e0*ionizing_cosmic_ray_flux #O + CR -> O+ + E

	return k

#returns reaction rates k*n*n or k*n (cm-3/s)
def get_reaction_rates(n):

	Tgas = n[idx_Tgas]
	k = get_coefficients(Tgas)

	return [k[0]*n[idx_Oj]*n[idx_H2],\
	 k[1]*n[idx_OHj]*n[idx_H2],\
	 k[2]*n[idx_H2Oj]*n[idx_H2],\
	 k[3]*n[idx_H3Oj]*n[idx_E],\
	 k[4]*n[idx_H2Oj]*n[idx_E],\
	 k[5]*n[idx_H2Oj]*n[idx_E],\
	 k[6]*n[idx_OHj]*n[idx_E],\
	 k[7]*n[idx_O]]


#retruns the population of the levels of the oxygen atom at a given Tgas
def get_levels_oxygen(n,Tgas,energy,A10):
	number_of_levels = 2 #number of excited levels+ground
	#initializes matrix
	M = [[0e0 for i in range(number_of_levels)] for j in range(number_of_levels)]
	g = [1e0,3e0] #degenerancy coefficients (one per level)
	ADD HERE THE REST OF THE FUNCTION

#returns O cooling, K/s/cm3
def get_cooling_oxygen(n,Tgas):
	energy = [0e0, 24e0] #energy levels (one per level), K
	A10 = 5.1e-5 #Einstein coefficient (transition 1->0), 1/s
	ADD HERE THE REST OF THE FUNCTION

#returns differential equations (dn/dt)
def differential_equations(n,t):

	#get the reaction rates
	rates = get_reaction_rates(n)
	
	#store temperature as a local variable
	Tgas = n[idx_Tgas]

	#initialize ODEs
	dn = [0e0 for x in range(number_of_species)]
	
	#ODEs
	dn[idx_Oj] = \
		-rates[0] \
		+rates[7]

	dn[idx_H2] = \
		-rates[0] \
		-rates[1] \
		-rates[2] \
		+rates[5]

	dn[idx_OHj] = \
		+rates[0] \
		-rates[1] \
		-rates[6]

	dn[idx_H] = \
		+rates[0] \
		+rates[1] \
		+rates[2] \
		+rates[3] \
		+rates[4] \
		+rates[6]

	dn[idx_H2Oj] = \
		+rates[1] \
		-rates[2] \
		-rates[4] \
		-rates[5]

	dn[idx_H3Oj] = \
		+rates[2] \
		-rates[3]

	dn[idx_E] = \
		-rates[3] \
		-rates[4] \
		-rates[5] \
		-rates[6] \
		+rates[7]

	dn[idx_H2O] = \
		+rates[3]

	dn[idx_OH] = \
		+rates[4]

	dn[idx_O] = \
		+rates[5] \
		+rates[6] \
		-rates[7]


	#differential for the temeperature
	dn[idx_Tgas] = COMPLETE THE EXPRESSION

	return dn

######FUNCTIONS END HERE######


######MAIN STARTS HERE######

time = 0e0 #initial total time (s)
time_step = spy #initial time-step (s)
time_end = 1e6*spy #end of simulation (s)

#default number densities (cm-3)
unknowns = [1e-40 for i in range(number_of_species)]
total_hydrogen_nuclei = 1e8 #cm-3
#define species number densities as fractions of total H nuclei
unknowns[idx_H2] = 1e-1*total_hydrogen_nuclei #cm-3
unknowns[idx_O] = 1e-3*total_hydrogen_nuclei #cm-3

#gas temperature
Tgas = 5e3 #K

#add Tgas to the list of the unknowns (initial condition)
unknowns[idx_Tgas] = Tgas

file_output_handle = open("fout.dat","w")

#infinite loop (breaks when time_end reached)
while(True):
	#computes electrons number density (sum over ions)
	unknowns[idx_E] = unknowns[idx_Oj] + unknowns[idx_OHj] + unknowns[idx_H2Oj] + unknowns[idx_H3Oj]
	time_grid = np.linspace(0, time_step, 2) #prepare time grid (2 value = 1 step)
	results = odeint(differential_equations, unknowns, time_grid, rtol=1e-6, atol=1e-20) #solve
	time += time_step #increases time (s)
	time_step = time_step * 1.1e0 #increase time-step (for output purposes)
	unknowns = results[1][:] #results are new initial conditions (mind results structure!)
	file_output_handle.write(str(time/spy)+" "+str(unknowns[idx_Tgas])+"\n")
	#breaks when reach the end time
	if(time>time_end): break
print "done!"
file_output_handle.close()
######MAIN ENDS HERE######






