!*******************************************************************************
! DISPATCH main module, which maintains a number of tasks that this MPI-rank
! handles -- possibly using OpenMP to distribute over cores / threads.  When
! we arrive here the code has already initialized MPI, so we are one rank out
! of many (possibly millions).  However, we only need to stay in contact with
! essentially 7 other ranks; our 6 neighbors in (dir,pos) space (3x2), and our
! father, one AMR level above us (the convention in this code is that levels
! increase downwards, as in an ancestor tree, and as in the gravitational 
! potential around the stars and planets we simulate.
!
! Our tasks are to 1) allocate the AMR structure table, making it large enough
! to hold the conceivable number of patches we could imagine to need, and then
! 2) create the actual number of patches that we are to handle here, locally,
! based on the value of levelmin, then 3) to fill these patches with initial
! values, and then start to evolve them.
!*******************************************************************************
MODULE dispatch_mod
  USE mhd_mod,    only : mhd_t, allocate_mhd
  USE timestep,   only : time_step
  USE amr_mod,    only : init_amr
  USE mesh_mod,   only : init_mesh
  USE boundaries, only : init_boundary
  USE updown_mod, only : upload
  USE io_amr,     only : write_amr
  USE task_mod
  USE io
  implicit none
PRIVATE
  logical:: alive=.true.
  real(8):: t_ref(50)=0.                          ! reference time (rabbit)
  integer, dimension(:), pointer:: lag              ! nondimensional lag
  character(len=mch):: datadir='.'
  real:: tout=1e30, tend=1e30
  integer:: nstep=10, fout=10
!...............................................................................
PUBLIC:: dispatch, alive
CONTAINS

!*******************************************************************************
SUBROUTINE dispatch
  USE poisson_mod, only : init_selfgravity
  implicit none
  type(task_t), pointer:: task, p
  namelist /run_params/ nstep, fout, tout, tend, do_trace, do_debug, datadir
  integer:: patch_unit=3
!...............................................................................
  call trace_begin ('dispatch')
  call init_io                                                                  ! I/O
  call init_amr                                                                 ! AMR patches
  call init_mesh                                                                ! mesh
  call init_boundary                                                            ! ghost zones
  call init_selfgravity                                                         ! selfgravity
  !
  rewind(input); read(input,run_params); if(master)write(*,run_params)          ! run control
  !
  call allocate_task(task)                                                      ! main taks
  call startup (task%f(1), task%upload)                                         ! initialize with ICs
  task%tout=tout; task%fout=fout; task%nstep=nstep; task%tend=tend
  call extra_tasks()
  !
  do while (task%tnext < task%t)                                                ! if next output is in the past
    task%tnext = task%tnext + task%tout                                         ! fast forward
  end do
  call write_amr (task, datadir)
  !
  !$omp parallel                                                                ! Start of parallel region
  !$omp single                                                                  ! One thread takes ownership of while-loop
  do while (alive)                                                              ! call die() to stop
    call dispatcher
    alive = alive.and.(task%istep < task%nstep).and.(task%t < task%tend)
  end do
  !$omp end single
  !$omp end parallel
  call trace_end
END SUBROUTINE dispatch

!*******************************************************************************
SUBROUTINE dispatcher
  implicit none
  integer:: i, it, loc(1), lvl
  real(8):: t, dt, lag, maxlag
  type(task_t), pointer:: p, task
!...............................................................................
  call trace_begin ('dispatcher')
  maxlag = -99.0
  do i=1,ntask
    p => task_table(i)
    it = p%it                                                                   ! time index
    if (it < 0) cycle                                                           ! skip dead entries
    lag = (t_ref(p%level)-p%f(it)%t)/p%f(it)%dt                                 ! dimensionless lag
    if (lag > maxlag) then                                                      ! find the largest
      maxlag = lag
      task => p                                                                 ! save task pointer
    end if
  end do
  if (do_trace) print'(i8,a,i5,a,i5,1p,e12.5)', &
    mpi_rank, ' is calling worker with task ID', task%id, &
   ' at time', task%it, task%f(it)%t
  !
  call trace ('call worker')
  !$omp task firstprivate(task)                                                 ! Spawn a task on an arbitrary thread
                                                                                ! Use firstprivate to copy make a copy
                                                                                ! of the pointer to the thread

  call worker (task)                                                            ! can be OpenMP parallel

  !$omp end task                                                                ! end of region for the task

  !$omp taskwait                                                                ! enforce sync point of tasks (like barrier)

  if (task%upload >= 0) call upload (task)                                      ! optional upload
  call write_amr (task, datadir)
  !
  call trace_end
END SUBROUTINE dispatcher

!*******************************************************************************
SUBROUTINE worker (task)
  USE operators,   only : scalar_stats
  USE poisson_mod, only : selfgravity, selfgravity_phi
  implicit none
  integer:: it0, it1, level
  real(8):: t_new
  type(task_t), pointer:: task
  type(mhd_t),  pointer:: f, d
!...............................................................................
  call trace_begin ('worker')
  it0 = task%it                                 ! previous slot

  ! Find phi if we are doing selfgravity
  call selfgravity_phi(task%p(it0),task%f(it0))

  it1 = mod(it0,task%nt)+1                      ! new slot
  if (do_trace) print'(i8,a,2i3)', &
    mpi_rank,' is copying time slots:',it0,it1
  task%f(it1) = task%f(it0)                     ! copy result over

  if (selfgravity) then                         ! copy Poisson structure over
    task%p(it1) = task%p(it0)                   ! and initialize control variables
    task%p(it1)%ready = .false.                 ! new patch is not ready yet
    task%p(it1)%it = 0                          ! no iterations has been run for the solver
    task%p(it1)%error = huge(1.0)               ! error starts out huge
    call scalar_stats(task%p(it1)%phi,'worker:phi')
  endif

  call time_step (task, it1)                    ! step from it0
  task%it = it1                                 ! increment timestep
  task%t  = task%f(it1)%t
  task%istep = task%istep+1
  t_new = task%t                                ! updated MHD time
  level = task%level                            ! FIXME
  t_ref(level) = max(t_ref(level), t_new)       ! update the reference time
  call trace_end
END SUBROUTINE worker

!*******************************************************************************
SUBROUTINE extra_tasks
  implicit none
  integer:: i
  logical:: exists
  character(len=mch):: filename
  type(task_t), pointer:: task
!...............................................................................
  do i=1,9
    write (filename,'("input",i1,".nml")') i
    inquire (file=filename, exist=exists)
    if (exists) then
      close (input)
      open (file=filename, unit=input, form='formatted', status='old')
      call allocate_task(task)                                                    ! extra task, just to test dispatcher
      call startup (task%f(1), task%upload)                                       ! initialize with ICs
      print *,'extra', i, task%upload
      task%tout=tout; task%fout=fout; task%nstep=nstep; task%tend=tend
      close (input)
      open (file=inputfile, unit=input, form='formatted', status='old')
      if (master) print*,trim(filename)//' created extra task with ID', task%id
    end if 
  end do
END SUBROUTINE extra_tasks

!*******************************************************************************
END MODULE dispatch_mod

!*******************************************************************************
! Call this routine from anywhere, to cause the dispatcher to stop
!*******************************************************************************
SUBROUTINE die (reason)
  USE dispatch_mod
  USE io
  implicit none
  character(len=*):: reason
!...............................................................................
  if (master) print'(a)','stopping because '//trim(reason)
  alive = .false.
END SUBROUTINE die
