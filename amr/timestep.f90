! $Id$
!************************************************************************
MODULE timestep
  USE mhd_mod
  USE pde_mod
  USE mesh_mod, only: m
  USE task_mod, only: task_t
  USE io
  implicit none
PRIVATE
  logical:: first_time=.true.
  real, dimension(3) ::  alpha, beta
  integer:: timeorder=3
  real(kind=8):: courant_number=0.2
PUBLIC:: time_step, courant_number
CONTAINS

!************************************************************************
SUBROUTINE init_timestep
  implicit none
  namelist /timestep_params/ courant_number, timeorder
!........................................................................
  call trace_begin ('init_timestep')
  rewind(input); read(input,timestep_params); if(master) write(*,timestep_params)
  if      (timeorder==1) then
                              alpha=(/ 0.00000,  0.000000,  0.000000 /)
                              beta =(/ 1.00000,  0.000000,  0.000000 /)
  else if (timeorder==2) then
                              alpha=(/ 0.00000, -0.500000,  0.000000 /)
                              beta =(/ 0.50000,  1.000000,  0.000000 /)
  else if (timeorder==3) then
                              alpha=(/ 0.00000, -0.641874, -1.310210 /)
                              beta =(/ 0.46173,  0.924087,  0.390614 /)
  end if
  first_time = .false.
  call trace_end
END SUBROUTINE init_timestep

!************************************************************************
SUBROUTINE time_step (task, it)
  implicit none
  type(task_t), pointer:: task
  integer:: it
  type(mhd_t), pointer:: f, d
  integer:: iz, substep, dir
  character(len=mch):: id="timestep $Id$"
!........................................................................
  call print_id (id)
  call trace_begin ('timestep')
  if (first_time) call init_timestep
!------------------------------------------------------------------------
!  Loop over Runge-Kutta stages
!------------------------------------------------------------------------
  f => task%f(it)                                                       ! point to variables
  d => task%d(it)                                                       ! point to derivatives
  m =  task%f(it)%m                                                     ! copy mesh
  do substep=1,timeorder
    if (substep==1) then
      !$omp parallel do
      do iz=m(3)%lb,m(3)%ub
          d%d(:,:,iz) = 0.
          d%s(:,:,iz) = 0.
        d%p%x(:,:,iz) = 0.
        d%p%y(:,:,iz) = 0.
        d%p%z(:,:,iz) = 0.
        d%B%x(:,:,iz) = 0.
        d%B%y(:,:,iz) = 0.
        d%B%z(:,:,iz) = 0.
      end do
    else
      !$omp parallel do
      do iz=m(3)%lb,m(3)%ub
          d%d(:,:,iz) = alpha(substep)*  d%d(:,:,iz)
          d%s(:,:,iz) = alpha(substep)*  d%s(:,:,iz)
        d%p%x(:,:,iz) = alpha(substep)*d%p%x(:,:,iz)
        d%p%y(:,:,iz) = alpha(substep)*d%p%y(:,:,iz)
        d%p%z(:,:,iz) = alpha(substep)*d%p%z(:,:,iz)
        d%B%x(:,:,iz) = alpha(substep)*d%B%x(:,:,iz)
        d%B%y(:,:,iz) = alpha(substep)*d%B%y(:,:,iz)
        d%B%z(:,:,iz) = alpha(substep)*d%B%z(:,:,iz)
      end do
    end if
    call pde (f, d)
    if (substep==1) call courant (f, courant_number, f%dt)
    !$omp parallel do
    do iz=m(3)%lb,m(3)%ub
        f%d(:,:,iz) =   f%d(:,:,iz) + (f%dt*beta(substep))*  d%d(:,:,iz)
        f%s(:,:,iz) =   f%s(:,:,iz) + (f%dt*beta(substep))*  d%s(:,:,iz)
      f%p%x(:,:,iz) = f%p%x(:,:,iz) + (f%dt*beta(substep))*d%p%x(:,:,iz)
      f%p%y(:,:,iz) = f%p%y(:,:,iz) + (f%dt*beta(substep))*d%p%y(:,:,iz)
      f%p%z(:,:,iz) = f%p%z(:,:,iz) + (f%dt*beta(substep))*d%p%z(:,:,iz)
      f%B%x(:,:,iz) = f%B%x(:,:,iz) + (f%dt*beta(substep))*d%B%x(:,:,iz)
      f%B%y(:,:,iz) = f%B%y(:,:,iz) + (f%dt*beta(substep))*d%B%y(:,:,iz)
      f%B%z(:,:,iz) = f%B%z(:,:,iz) + (f%dt*beta(substep))*d%B%z(:,:,iz)
    end do
  end do
  f%t   = f%t + f%dt
  f%offset = f%offset + f%u0*f%dt           ! update position
  do dir=1,3
    m(dir)%r = m(dir)%r + f%u0(dir)*f%dt
  end do
  call trace_end
END SUBROUTINE time_step
END MODULE timestep
