!*******************************************************************************
! Module with skeleton data type for MHD.
!*******************************************************************************
MODULE mhd_mod
  USE mesh_mod,    only: m, mesh_t
  USE vector_mod
  USE io
PRIVATE
  logical:: first_time=.true.
  type:: mhd_t
    logical:: is_allocated=.false.
    integer:: it, nt, level
    real(8), dimension(:), pointer:: time
    real(8), dimension(:), pointer:: dtime
    real(8):: t                               ! simulated time
    real(8):: dt                              ! simulated time
    real(8):: offset(3)=0.0                   ! offset relative cell center
    real, dimension(:,:,:), pointer:: d   ! mass per unit volume
    real, dimension(:,:,:), pointer:: s   ! entropy per unit volume
    type(vector_t):: p                        ! momentum per unit volume
    type(vector_t):: B                        ! magnetic flux per unit area
    type(mesh_t):: m(3)                       ! mesh
    real:: u0(3)
  end type
PUBLIC:: mhd_t, allocate_mhd, deallocate_mhd, reallocate_mesh, prolong_mesh, &
         restrict_mesh
CONTAINS

!*******************************************************************************
SUBROUTINE allocate_mhd (a)
  implicit none
  type(mhd_t), target:: a
  type(mhd_t), pointer:: p
!...............................................................................
  if (a%is_allocated) return
  !call trace_begin ('allocate_mhd')
  call allocate_scalar (a%d)
  call allocate_scalar (a%s)
  call allocate_vector (a%p)
  call allocate_vector (a%B)
  a%is_allocated = .true.
  !call trace_end
END SUBROUTINE allocate_mhd

!*******************************************************************************
SUBROUTINE deallocate_mhd (a)
  implicit none
  type(mhd_t):: a
!...............................................................................
  if (.not.a%is_allocated) return
  call deallocate_scalar (a%d)
  call deallocate_scalar (a%s)
  call deallocate_vector (a%p)
  call deallocate_vector (a%B)
  a%is_allocated = .false.
END SUBROUTINE deallocate_mhd

!*******************************************************************************
SUBROUTINE reallocate_mesh (mhd, n)
  type(mhd_t):: mhd
  integer:: n(3), lb(3), ub(3), dir, i
!...............................................................................
  call trace_begin ('reallocate_mesh')
  lb =   - mhd%m%nghost                                                         ! new lower boundary
  ub = n + mhd%m%nghost - 1                                                     ! new upper boundary
  mhd%m%d = mhd%m%s/n                                                           ! new dr=s/n
  mhd%m%n = n                                                                   ! new number of cells
  mhd%m%lb = lb                                                                 ! new lower boundary
  mhd%m%ub = ub                                                                 ! new upper boundary
  do dir=1,3
    deallocate (mhd%m(dir)%r)                                                   ! deallocate wrong size
    allocate   (mhd%m(dir)%r(n(dir)))                                           ! allocate right size
    do i=lb(dir),ub(dir)
      mhd%m(dir)%r(i) = mhd%m(dir)%pos + (i-n(dir)/2)*mhd%m(dir)%d              ! new mesh coordinates
    end do
  end do
  call trace_end
END SUBROUTINE reallocate_mesh

!*******************************************************************************
SUBROUTINE prolong_mesh (mhd1, mhd2)
  type(mhd_t):: mhd1, mhd2
  integer:: n(3)
!...............................................................................
  call trace_begin ('prolong_mesh')
  n = mhd1%m%n*2
  call reallocate_mesh (mhd2, n)
  call trace_end
END SUBROUTINE prolong_mesh

!*******************************************************************************
SUBROUTINE restrict_mesh (mhd1, mhd2)
  type(mhd_t):: mhd1, mhd2
  integer:: n(3)
!...............................................................................
  call trace_begin ('restrict_mesh')
  n = mhd1%m%n/2
  call reallocate_mesh (mhd2, n)
  call trace_end
END SUBROUTINE restrict_mesh

END MODULE mhd_mod
