MODULE refine_mod
  USE amr_mod
  USE io
CONTAINS

!*******************************************************************************
! Run through and create sons if the refine flag is set
!*******************************************************************************
SUBROUTINE refine (a)
  implicit none
  type(amr_t):: a
  integer id, ns
!...............................................................................
  call trace_begin('refine')
  id = 0
  do while (id < a%namr)
    id = id+1
    if (a%patch(id)%refine) then
      if (.not.a%patch(id)%refined) call make_sons (a, id)
      a%patch(id)%refine = .false.
    end if
  end do
  call trace_end
END SUBROUTINE refine
!*******************************************************************************
END MODULE refine_mod
