!*******************************************************************************
! Module with skeleton data type for AMR.  At least for now, we will keep things 
! simple, by maintaining an MPI-synchronized structure with a complete set of all
! patch positions and relations in space.  This is the task of this module, which 
! defines the derived data type and the related methods.  The data type is low cost, 
! with only about 100 bytes per patch, so even a million patches is a small amount 
! of the memory of one node.  A practical limit is perhaps to have at most 1e7 
! patches, before this starts to become a limitation. Each patch can hold for 
! example 32^3 cells for up to 3 billion cells.  With 10 mus/update and a desire 
! to do one update per second on all patches (conservative estimate since it 
! disregards the inherent sub-cycling), this starts to become a limitation when 
! using more than 3e9*1e-5 = 1e4 nodes (not cores!).  So, we are fine up to several 
! hundred thousand cores, for now.
!*******************************************************************************
MODULE amr_mod
  USE patch_mod
  USE io
  implicit none
!-------------------------------------------------------------------------------
  type:: amr_t
    integer:: levelmin, levelmax      ! min and max refinement
    logical:: is_allocated
    integer:: ntime                   ! number of time steps to retain
    integer:: maxamr, namr            ! max and actual number of amr patches
    type(patch_t), dimension(:), pointer:: patch    ! global need-to-know
  end type
!-------------------------------------------------------------------------------
  type(amr_t), target:: amr
  type(patch_t):: patch
  integer, private:: patch_bytes
CONTAINS

!*******************************************************************************
SUBROUTINE init_amr
  implicit none
  real:: amrgb=0.1
  integer:: levelmin=5, levelmax=5, maxamr=0
  namelist /amr_params/ levelmin, levelmax, maxamr, amrgb
  character(len=mch), save:: id= &
  'amr_mod.f90 $Id$'
!...............................................................................
  call print_id (id)
  rewind(input); read(input,amr_params); if(master) write(*,amr_params)
  patch_bytes=sizeof(patch)
  if (maxamr==0) maxamr = amrgb*1e9/patch_bytes
  amr%maxamr = maxamr
  if (master) print &
  '(1x,"allocating",f7.3," GB per rank to AMR table, for max",f8.3," million patches")', &
    maxamr*patch_bytes*1e-9, real(maxamr)*1e-6
  allocate (amr%patch(amr%maxamr))
  amr%is_allocated = .true.
  amr%levelmin = levelmin
  amr%levelmax = levelmax
  amr%namr = 0  
END SUBROUTINE init_amr

!*******************************************************************************
SUBROUTINE deallocate_amr (amr)
  implicit none
  type(amr_t):: amr
!...............................................................................
  deallocate (amr%patch)
  amr%is_allocated = .false.
END SUBROUTINE deallocate_amr

!*******************************************************************************
SUBROUTINE print_amr (a)
  implicit none
  integer i, nf
  type(amr_t):: a
!...............................................................................
  print'(a)', '  id   rank father level     size           position       '// &
  '              sons                neighbor     foregn ref  load'
  nf = 0
  do i=1,a%namr
    print'(i4,i7,i7,i6,f9.6,3x,3f8.5,2x,8i3,2x,6i3,i6,l4,f6.0)',  &
      a%patch(i)%id, a%patch(i)%rank, a%patch(i)%father, a%patch(i)%level, &
      a%patch(i)%size, a%patch(i)%pos, a%patch(i)%son, a%patch(i)%neighbor, &
      a%patch(i)%foreign, a%patch(i)%refine, a%patch(i)%load
    nf = nf + a%patch(i)%foreign
  end do
  print *,'total foreigners:', nf
END SUBROUTINE print_amr

!*******************************************************************************
! Create the eight sons of a father
!*******************************************************************************
SUBROUTINE make_sons (amr, id)
  implicit none
  integer:: id
  type(amr_t), target:: amr
  type(patch_t), pointer:: my
  integer:: ix, iy, iz, ii(3), dir, pos
!...............................................................................
  call trace_begin('make_sons')
  my => amr%patch(id)
  do iz=1,2; do iy=1,2; do ix=1,2
    amr%namr = amr%namr + 1
    if (amr%namr > amr%maxamr) call exit(1)
    my%son(ix,iy,iz) = amr%namr
    amr%patch(amr%namr) = make_son (ix, iy, iz, amr%namr, my)
  end do; end do; end do
  my%refined = .true.
  call search_for_neighbors (amr, id)
  call trace_end
END SUBROUTINE make_sons

!*******************************************************************************
SUBROUTINE search_for_neighbors (a, id)
  implicit none
  integer:: id
  type(amr_t), target:: a
  type(patch_t), pointer:: my, pa, son, pbor
  integer:: ix, iy, iz, ii(3), dir, pos, nf, inb
!...............................................................................
  call trace_begin('search_for_neighbors')
  my => a%patch(id)                         ! pointer to center   patch @ level
  !print *,'my id:', my%id
  pa => a%patch(my%father)                  ! pointer to father   patch @ level-1
  !print *,'pa id:', pa%id
  do iz=1,2; do iy=1,2; do ix=1,2
    nf = 0
    son => a%patch(my%son(ix,iy,iz))        ! pointer to new son  patch @ level
    do dir=1,3; do pos=1,2
      pbor => a%patch(pa%neighbor(dir,pos)) ! pointer to neighbor patch @ level-1
      if (pbor%refined) then                ! refined => index to neighbor son @ level
        ii = (/ix,iy,iz/)                   ! son-index
        ii(dir) = 3-ii(dir)                 ! mirror direction
        inb = pbor%son(ii(1),ii(2),ii(3))   ! index of nearest neighbor     
      else                                  ! not refined => index to neighbor @ level-1
        inb = pbor%id
      end if
      son%neighbor(dir,pos) = inb
      if (a%patch(inb)%rank /= son%rank) nf = nf+1
     !print'("son: id, inb, rank1, rank2, foreign",7i5)', &
     !  son%id, my%son(ix,iy,iz), inb, a%patch(inb)%rank, son%rank, nf
    end do; end do
    son%foreign = nf
  end do; end do; end do
  call trace_end
END SUBROUTINE search_for_neighbors

!*******************************************************************************
END MODULE amr_mod
