!*******************************************************************************
MODULE updown_mod
  USE mhd_mod,      only: mhd_t, reallocate_mesh, prolong_mesh, restrict_mesh
  USE task_mod,     only: task_t, allocate_task, ntask, task_table
  USE mesh_mod,     only: mesh_t, m
  USE interpol_mod, only: prolong_scalar, restrict_scalar, &
                          prolong_vector, restrict_vector
  USE io
  implicit none
PRIVATE
!...............................................................................
PUBLIC:: upload, prolong_task, restrict_task
CONTAINS

!*******************************************************************************
SUBROUTINE prolong_task (task1, task2)
  implicit none
  type(task_t), pointer:: task1, task2
!...............................................................................
  call trace_begin ('prolong_task')
  call allocate_task (task2)
  task2%it = 1
  call prolong_mesh   (task1%f(1)  , task2%f(1)  )
  call prolong_scalar (task1%f(1)%d, task2%f(1)%d, task2%f(1)%m)
  call prolong_scalar (task1%f(1)%s, task2%f(1)%s, task2%f(1)%m)
  call prolong_vector (task1%f(1)%p, task2%f(1)%p, task2%f(1)%m)
  call prolong_vector (task1%f(1)%B, task2%f(1)%B, task2%f(1)%m)
  call trace_end
END SUBROUTINE prolong_task

!*******************************************************************************
SUBROUTINE restrict_task (task1, task2)
  implicit none
  type(task_t), pointer:: task1, task2
!...............................................................................
  call trace_begin ('restrict_task')
  call allocate_task (task2)
  task2%it = 1
  call restrict_mesh   (task1%f(1)  , task2%f(1)  )
  call restrict_scalar (task1%f(1)%d, task2%f(1)%d, task2%f(1)%m)
  call restrict_scalar (task1%f(1)%s, task2%f(1)%s, task2%f(1)%m)
  call restrict_vector (task1%f(1)%p, task2%f(1)%p, task2%f(1)%m)
  call restrict_vector (task1%f(1)%B, task2%f(1)%B, task2%f(1)%m)
  call trace_end
END SUBROUTINE restrict_task

!*******************************************************************************
! Uploading data from one task into another one, replacing its values.
! For new we are assuming that the task to upload to is on the same rank (FIXME)
!*******************************************************************************
SUBROUTINE upload (task1)
  implicit none
  type(task_t), pointer:: task1, task2, task3
  integer:: i, dir, offset(3)
!...............................................................................
  call trace_begin ('upload')
  if (task1%id > 0) then
    do i=1,ntask
      if (do_trace) print*,'upload', task_table(i)%id, task1%id
      if (task_table(i)%id == task1%upload) then
        task2 => task_table(i)
        !
        if (all(task1%f(1)%m%n == task2%f(1)%m%n/2)) then                       ! twice the size?
          if (master) print'(" task",i5," prolonged to task",i5)', task1%id, task3%id
          call prolong_task (task1, task3)                                      ! yes, prolong
          task1 => task3                                                        ! masquerade
        else if (all(task1%f(1)%m%n == task2%f(1)%m%n*2)) then                  ! half the size?
          if (master) print'(" task",i5," restricted to task",i5)', task1%id, task3%id
          call restrict_task (task1, task3)                                     ! yes, restrict
          task1 => task3                                                        ! masquerade
        end if
        !
        if (master) print'(" task",i5," uploading to task",i5)', task1%id, task2%id
        offset = (task2%f(task2%it)%offset - task1%f(task1%it)%offset) &        ! integer offset (FIXME)
               / task1%f(task1%it)%m%d
        if (master) print'(" offset: ",3i5)', offset
        if (all(task1%f(1)%m%n == task2%f(1)%m%n)) then
          task2%f(task2%it)%d   = task1%f(task1%it)%d  
          task2%f(task2%it)%s   = task1%f(task1%it)%s  
          task2%f(task2%it)%p%x = task1%f(task1%it)%p%x
          task2%f(task2%it)%p%y = task1%f(task1%it)%p%y
          task2%f(task2%it)%p%z = task1%f(task1%it)%p%z
          task2%f(task2%it)%B%x = task1%f(task1%it)%B%x
          task2%f(task2%it)%B%y = task1%f(task1%it)%B%y
          task2%f(task2%it)%B%z = task1%f(task1%it)%B%z
          do dir=1,3
            task2%f(task2%it)%d   = cshift(task2%f(task2%it)%d  , offset(dir), dir)
            task2%f(task2%it)%s   = cshift(task2%f(task2%it)%s  , offset(dir), dir)
            task2%f(task2%it)%p%x = cshift(task2%f(task2%it)%p%x, offset(dir), dir)
            task2%f(task2%it)%p%y = cshift(task2%f(task2%it)%p%y, offset(dir), dir)
            task2%f(task2%it)%p%z = cshift(task2%f(task2%it)%p%z, offset(dir), dir)
            task2%f(task2%it)%B%x = cshift(task2%f(task2%it)%B%x, offset(dir), dir)
            task2%f(task2%it)%B%y = cshift(task2%f(task2%it)%B%y, offset(dir), dir)
            task2%f(task2%it)%B%z = cshift(task2%f(task2%it)%B%z, offset(dir), dir)
          end do
          !print'(a/(32f5.2))','t1',task1%f(task1%it)%B%z(:,task1%f(1)%m(2)%n/2,task1%f(1)%m(3)%n/2)
          !print'(a/(32f5.2))','t2',task2%f(task2%it)%B%z(:,task1%f(1)%m(2)%n/2,task1%f(1)%m(3)%n/2)
        end if
      end if
    end do
  end if
  call trace_end
END SUBROUTINE upload

!*******************************************************************************
END MODULE updown_mod
