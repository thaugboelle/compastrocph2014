!*******************************************************************************
! Partial Differential Equations corresponding to Magneto-Hydro-Dynamics
!*******************************************************************************
MODULE pde_mod
  USE state_mod,  only: state_t
  USE vector_mod, only: vector_t, allocate_vector, div_minus, &
                        dot, cross_subr, curl_minus, boundary_vector
  USE scalar_mod, only: allocate_scalar, laplace, scalar_stats
  USE mesh_mod,   only: mesh_t
  USE io
  implicit none
PRIVATE
  logical:: first_time=.true.
  type(vector_t):: U, E, flux                   ! velocity, electric field, flux
  real, dimension(:,:,:), pointer:: pg, pm      ! gas and magnetic pressure
  character(len=16):: eqn='mhd'
  real,public :: eta=0.001                      ! diffusion speed
  real,public :: nu =0.001                      ! diffusion speed
  real:: gamma=5./3.                            ! ideal gas adiabatic index
  real(kind=8):: courant_number=0.2
  public:: init_pde, pde, gamma, U, pg, pm, eqn, courant_internal, courant_number
CONTAINS

!*******************************************************************************
SUBROUTINE init_pde (m)
  implicit none
  type(mesh_t):: m(3)
  namelist /pde_params/ eqn, gamma, eta, nu
!...............................................................................
  if (.not.first_time) return
  first_time = .false.
  call allocate_vector (m, U)
  call allocate_vector (m, E)
  call allocate_vector (m, flux)
  call allocate_scalar (m, pg)
  call allocate_scalar (m, pm)
  rewind(input); read(input,pde_params); if(master) write(*,pde_params)
  eta = eta*sum(m%d**2)
  nu  = nu *sum(m%d**2)
END SUBROUTINE init_pde

!*******************************************************************************
SUBROUTINE pde (m, v, d)
  implicit none
  type(mesh_t):: m(3)
  type(state_t):: v, d
  !-----------------------------------------------------------------------------
  ! Compute velocity U=momentum/density, and electric field -E = U x B
  !-----------------------------------------------------------------------------
  call init_pde (m)
  call trace_begin ('pde')
  U%x = v%p%x/v%d;                         call scalar_stats (m, U%x, 'Ux')     ! U = p/d
  U%y = v%p%y/v%d;                         call scalar_stats (m, U%y, 'Uy')
  U%z = v%p%z/v%d;                         call scalar_stats (m, U%z, 'Uz')
  call cross_subr (v%B, U, E);             call scalar_stats (m, E%x, 'Ex')     ! E = - U x B
  !-----------------------------------------------------------------------------
  ! Conservation of magnetic flux
  !-----------------------------------------------------------------------------
  call curl_minus (m, E, d%B);             call scalar_stats (m, d%B%z, 'Bz')   ! dB/dt = -curl(E)
  call resistivity (m, v, d)                                                    ! add resistive effects
  if (eqn=='induction') return                                                  ! bail out if enough
  !-----------------------------------------------------------------------------
  ! Conservation of mass
  !-----------------------------------------------------------------------------
  call div_minus (m, v%p, d%d);            call scalar_stats (m, d%d, 'dd')     ! d(rho)/dt = -div(p) = -div(d*U)
  !-----------------------------------------------------------------------------
  ! Conservation of entropy
  !-----------------------------------------------------------------------------
  flux%x = v%s*U%x
  flux%y = v%s*U%y
  flux%z = v%s*U%z
  call div_minus (m, flux, d%s);           call scalar_stats (m, d%s, 'ds')     ! d(s)/dt = -div(s*U)
  !-----------------------------------------------------------------------------
  ! Conservation of momentum
  !-----------------------------------------------------------------------------
  pg =  v%d**gamma*exp(v%s/v%d*(gamma-1))                                       ! ideal gas
  pm =  0.5*(v%B%x**2+v%B%y**2+v%B%z**2)
  flux%x = v%p%x*U%x - v%B%x*v%B%x + pg + pm
  flux%y = v%p%x*U%y - v%B%x*v%B%y
  flux%z = v%p%x*U%z - v%B%x*v%B%z
  call div_minus (m, flux, d%p%x)                                               ! d(p%x)/dt = -div(p*U_x + P x_hat)
  flux%x = v%p%y*U%x - v%B%y*v%B%x
  flux%y = v%p%y*U%y - v%B%y*v%B%y + pg + pm
  flux%z = v%p%y*U%z - v%B%y*v%B%z
  call div_minus (m, flux, d%p%y)                                               ! d(p%y)/dt = -div(p*U_y + P y_hat)
  flux%x = v%p%z*U%x - v%B%z*v%B%x
  flux%y = v%p%z*U%y - v%B%z*v%B%y
  flux%z = v%p%z*U%z - v%B%z*v%B%z + pg + pm
  call div_minus (m, flux, d%p%z)                                               ! d(p%z)/dt = -div(p*U_z + P z_hat)
  call viscosity (m, v, d)                                                      ! add viscous effects
  !-----------------------------------------------------------------------------
  ! Ghost zone fill
  !-----------------------------------------------------------------------------
  !call boundary        (d%d)
  !call boundary        (d%s)
  !call boundary_vector (d%p)
  !
  call trace_end
END SUBROUTINE pde

!*******************************************************************************
! Simple dissipative effects
!*******************************************************************************
SUBROUTINE resistivity (m, v, d)
  implicit none
  type(mesh_t):: m(3)
  type(state_t):: v, d
  real:: ds
  ds = m(1)%d
  d%B%x = d%B%x + laplace(m, v%B%x, eta*ds)
  d%B%y = d%B%y + laplace(m, v%B%y, eta*ds)
  d%B%z = d%B%z + laplace(m, v%B%z, eta*ds)
END SUBROUTINE resistivity
!*******************************************************************************
SUBROUTINE viscosity (m, v, d)
  implicit none
  type(mesh_t):: m(3)
  type(state_t):: v, d
  real:: ds
  ds = m(1)%d
  d%p%x = d%p%x + laplace(m, v%p%x, nu*ds)
  d%p%y = d%p%y + laplace(m, v%p%y, nu*ds)
  d%p%z = d%p%z + laplace(m, v%p%z, nu*ds)
END SUBROUTINE viscosity

!*******************************************************************************
SUBROUTINE courant_internal (m, v, dt)
  USE io, only: do_trace
  implicit none
  type(mesh_t):: m(3)
  real(8):: c, dt
  type(state_t):: v
!-------------------------------------------------------------------------------
! Evaluate Courant condition
!-------------------------------------------------------------------------------
  call trace_begin ('courant')
  c = courant_number
  if (eqn=='induction') then
    dt = c*minval(m%d)/max(2.0,maxval(sqrt(dot(m,U,U))))                        ! velocity magnitue
  else
    dt = c*minval(m%d)/maxval(sqrt(dot(m,U,U)) + sqrt((2.*pm+gamma*pg)/v%d))    ! velocity magnitue + fast mode speed
  end if
  if (do_trace) print'(a,i4,1p,6g11.3)','courant: id,dt =', &
    m(1)%id, m(1)%d, dt, maxval(sqrt(dot(m,U,U))), &
    maxval(sqrt((2.*pm+gamma*pg)/v%d))
  call trace_end
END SUBROUTINE courant_internal
END MODULE pde_mod

!*******************************************************************************
SUBROUTINE courant (m, v, dt)
  USE mesh_mod,   only: mesh_t
  USE state_mod,  only: state_t
  USE pde_mod,    only: U, pg, pm, gamma, eqn, courant_internal, init_pde
  USE io,         only: do_trace, trace_begin, trace_end
  USE scalar_mod, only: scalar_stats
  implicit none
  type(mesh_t):: m(3)
  type(state_t):: v
  real(8):: dt
!-------------------------------------------------------------------------------
! Evaluate Courant condition
!-------------------------------------------------------------------------------
  call trace_begin ('courant')
  call init_pde (m)
  U%x = v%p%x/v%d
  U%y = v%p%y/v%d
  U%z = v%p%z/v%d
  pg =  v%d**gamma*exp(v%s/v%d*(gamma-1))                                       ! ideal gas
  pm =  0.5*(v%B%x**2+v%B%y**2+v%B%z**2)
  call courant_internal (m, v, dt)
  call trace_end
END SUBROUTINE
