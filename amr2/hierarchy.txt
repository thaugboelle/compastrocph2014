CALL HIERARCHY:

init_mpi
  dispatch                    dispatch_mod.f90            main driver
    init_io                   io_amr.f90                  initializes I/O
    init_amr                  amr_mod.f90                 allocates patch_table(:)
    allocate_tasks            task_mod.f90                pre-allocates tasks structure skeletons (no data)
    read_patches              patch_mod.f90               reads patch definitions; sets npatch in all ranks
      read_mesh               mesh_mod.f90                reads mesh parameters
    init_load_balance         load_balance_mod.f90        determines number of tasks (ntask) to run on this rank
      init_task               task_mod.f90                initialize task memory
        allocate_task         task_mod.f90                allocates data arrays for tasks
        startup               startup.f90                 construct initial conditions
          init_velocity       velocity_fields.f90         selection of velocity fields
          init_mfield         startup.f90                 set up magnetic field
    dispatcher                dispatch_mod.f90            decides which task to run next
      init_states             task_mod.f90                initializes the state structures
      courant                 pde_mod.f90                 estimate the time steps
      .. choose task ..
      worker                  dispatch_mod.f90            sets up for taking a timestep
        time_step             timestep_mod.f90            advance solution one step
          pde                 pde_mod.f90                 computes time derivatives
      write_amr               io_amr.f90                  writes output files
end_mpi