!*******************************************************************************
! Module with skeleton data type for AMR.
!*******************************************************************************
MODULE load_balance_mod
  USE amr_mod
  USE patch_mod, only: patch_table, npatch
  USE task_mod,  only: task_table, init_task
  USE mpi_base
  USE mpi_rma
  USE io
  type:: load_t
    logical:: is_allocated
    real, dimension(:), pointer:: load
    integer:: mrank
  end type
  type(load_t):: l
CONTAINS

!*******************************************************************************
SUBROUTINE allocate_load (l)
  implicit none
  type(load_t):: l
!...............................................................................
  if (l%is_allocated) return
  allocate (l%load(0:mpi_size-1))
  l%is_allocated = .true.
END SUBROUTINE allocate_load

!*******************************************************************************
SUBROUTINE load_stats (a)
  implicit none
  type(amr_t):: a
  integer:: i, rank
!...............................................................................
  call trace_begin('load_stats')
  if (.not. l%is_allocated) call allocate_load(l)
  l%mrank = 0
  l%load = 0.0
  do i=1,npatch
    rank = patch_table(i)%rank
    l%load(rank) = l%load(rank) + patch_table(i)%load
    l%mrank = max(l%mrank, patch_table(i)%rank)
!    print'(" i,load,rank,rankload:",2(i7,f6.0))', i, patch_table(i)%load, rank, l%load(rank)
  end do
  do i=0,l%mrank
    print'(" rank:",i7,5x,"load:",1p,e10.3)', i, l%load(i)
  end do
  call trace_end
END SUBROUTINE load_stats

!*******************************************************************************
! Start by giving out tasks equally to all ranks (possiby fewer on the last one)
!*******************************************************************************
SUBROUTINE init_load_balance
  implicit none
  integer:: i, n_per_rank
  type(task_t), pointer:: task
!...............................................................................
  call trace_begin('init_load_balance')
  n_per_rank = (npatch-1)/mpi_size + 1
  do i=1,npatch
    patch_table(i)%rank = (i-1)/n_per_rank
    if (mpi_rank==patch_table(i)%rank) then
      ntask = ntask + 1
      if (do_trace) print'(" rank",i4," taking patch", i4," as task", i4)', &
                             mpi_rank,                  i,         ntask
      task => task_table(ntask)
      call init_task (task)
    end if
  end do
  call trace_end
END SUBROUTINE init_load_balance

!*******************************************************************************
! Load balance:  Even out the load between ranks, by preferentially joining
! lost sons back to fathers -- this reduces the communication load as well.
!*******************************************************************************
SUBROUTINE load_balance (a)
  implicit none
  type(amr_t):: a
  call load_balance_rejoin (a)
  call load_balance_father (a)
END SUBROUTINE load_balance

!*******************************************************************************
SUBROUTINE load_balance_father (a)
  implicit none
  type(amr_t):: a
  real:: change, imbalance
  logical:: doit
  integer:: i, f
!...............................................................................
  call trace_begin('load_balance')
  call load_stats (a)
  !call sort (a)
  do i=2,npatch
    if (patch_table(i-1)%rank==patch_table(i)%rank) cycle
    imbalance = l%load(patch_table(i)%rank) - l%load(patch_table(i-1)%rank)
    if (imbalance > 0.0) then
      change = 2.*patch_table(i)%load
      doit = (change < imbalance)
      print'(" lb1:i,f,rank,frank,imb,chg,doit",4i6,2f12.0,l3)', &
        i, f, patch_table(i)%rank, patch_table(f)%rank, imbalance, change, doit
      if (doit) call change_rank (a, i, patch_table(i-1)%rank)
    else
      imbalance = -imbalance
      change = 2.*patch_table(i-1)%load
      doit = (change < imbalance)
      print'(" lb1:i,f,rank,frank,imb,chg,doit",4i6,2f12.0,l3)', &
        i, f, patch_table(i)%rank, patch_table(f)%rank, imbalance, change, doit
      if (doit) call change_rank (a, i-1, patch_table(i)%rank)
    end if
    call load_balance_rejoin (a)
  end do
  call load_stats (a)
  call trace_end
END SUBROUTINE load_balance_father

!*******************************************************************************
SUBROUTINE load_balance_rejoin (a)
  implicit none
  type(amr_t):: a
  real:: change, imbalance
  logical:: doit
  integer:: i, f
!...............................................................................
  call trace_begin('load_balance')
  call load_stats (a)
  !call sort (a)
  do i=1,npatch
    f = patch_table(i)%father
    if (patch_table(f)%rank == patch_table(i)%rank) cycle
    !---------------------------------------------------------------------------
    ! Select cases where father rank has more load, alse after rejoining
    !---------------------------------------------------------------------------
    imbalance = l%load(patch_table(i)%rank) - l%load(patch_table(f)%rank)
    if (imbalance > 0.0) then
      change = 2.*patch_table(i)%load
      doit = (change < imbalance)
      print'(" lb1:i,f,rank,frank,imb,chg,doit",4i6,2f12.0,l3)', &
        i, f, patch_table(i)%rank, patch_table(f)%rank, imbalance, change, doit
      if (doit) call join_father(a,i)
    end if
  end do
  do i=1,npatch
    f = patch_table(i)%father
    if (patch_table(f)%rank == patch_table(i)%rank) cycle
    !---------------------------------------------------------------------------
    ! Select cases where father rank has more load now, but a reverse is good
    !---------------------------------------------------------------------------
    imbalance = l%load(patch_table(i)%rank) - l%load(patch_table(f)%rank)
    if (imbalance > 0.0) then
      change = 2.*patch_table(i)%load
      doit = (change-imbalance) < imbalance
      print'(" lb2:i,f,rank,frank,imb,chg,doit",4i6,2f12.0,l3)', &
        i, f, patch_table(i)%rank, patch_table(f)%rank, imbalance, change, doit
      if (doit) then
        call join_father(a,i)
        exit                   ! done for now
      end if
    end if
  end do
  call load_stats (a)
  call trace_end
END SUBROUTINE load_balance_rejoin

!*******************************************************************************
SUBROUTINE join_father (a, i)
  implicit none
  type(amr_t):: a
  integer:: i, j, f, rank, frank, dir, pos
!...............................................................................
  rank   = patch_table(i)%rank
  f      = patch_table(i)%father
  frank  = patch_table(f)%rank
  !call send_patch (a, i, patch_table(i)%father)
  l%load(frank) = l%load(frank) + patch_table(i)%load
  l%load( rank) = l%load( rank) - patch_table(i)%load
  patch_table(i)%rank = frank
END SUBROUTINE join_father

!*******************************************************************************
! Assign the patch i to a new rank.   For now without actually shipping data
!*******************************************************************************
SUBROUTINE change_rank (a, i, rank)
  implicit none
  type(amr_t):: a
  integer:: i, rank, dir, pos, f, frank
!-------------------------------------------------------------------------------
! Now figure out the ranks of our 6 neighbors, and count foreign ranks. Then
! do the same for them.
!-------------------------------------------------------------------------------
  patch_table(i)%rank = rank
  !call search_for_neighbors (a, patch_table(i)%father) ! FIXME
END SUBROUTINE change_rank

!*******************************************************************************
END MODULE load_balance_mod
