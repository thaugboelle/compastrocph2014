!*******************************************************************************
MODULE io_amr
  USE mesh_mod  , only: mesh_t
  USE mpi_base  , only: master
  USE vector_mod, only: vector_t
  USE scalar_mod, only: scalar_stats
  USE task_mod  , only: task_t, write_task, datadir
  USE io
  implicit none
  integer:: iout = -1
  integer:: data_unit=2, log_unit=3, rec=1
  logical:: first_time=.true.
CONTAINS

!*******************************************************************************
SUBROUTINE amr_io
END SUBROUTINE amr_io

!*******************************************************************************
SUBROUTINE write_amr (task)
  implicit none
  type(task_t), pointer:: task
  character(len=mch):: filename
  logical:: exists
  integer:: it, iv
  real, dimension(:,:,:), pointer:: p
!...............................................................................
  call trace_begin ('amr_io')
  !
  if (mod(task%istep,task%fout) == 0) then
    if (master) print*,'write_amr because mod(istep,fout)==0 for task ID, it', task%id, task%iout
  else if (task%t(task%it) > task%tnext) then
    if (master) print*,'write_amr because t > tnext for task ID', task%id
  else
    call trace_end
    return
  end if
  if (task%t(task%it) > task%tnext) then
    task%tnext = task%tnext + task%tout
  end if
  !
  !write(filename,'(a,"/id=",i7.7,"_",i5.5,".task")')trim(datadir),task%id,task%iout
  !open (unit=data_unit,file=trim(filename),form='unformatted',status='unknown')
  !
  !write (data_unit) sizeof(task)
  !call write_task (data_unit, task)
  !close (data_unit)
  !
  write(filename,'(a,"/id=",i7.7,"_",i5.5,".dat")')trim(datadir),task%id,task%iout
  open (unit=data_unit,file=trim(filename),form='unformatted',status='unknown',&
        !access='direct',recl=product(task%m%n)*4)
        access='direct',recl=product(task%m%gn)*4)
  do iv=1,task%nv
    write(data_unit,rec=iv) task%mem(:,:,:,iv,task%it,1)
    p => task%mem(:,:,:,iv,task%it,1)
    call scalar_stats (task%m, p, 'f')
    !call write_field (task, task%mem(:,:,:,iv,task%id,1), 'f', iv)
  end do
  close (data_unit)
  !
  if (first_time) then
    open (log_unit,file=trim(datadir)//'/write_amr.log',status='unknown')
  else
    open (log_unit,file=trim(datadir)//'/write_amr.log',status='unknown',position='append')
  end if
  first_time = .false.
  write (filename,'("id=",i7.7,"_",i5.5,".dat")') task%id,task%iout
  write (log_unit,'(a)') trim(filename)
  write (log_unit,*) task%m%gn, task%t(task%it), task%level
  close (log_unit)
  !
  task%iout = task%iout + 1                                                     ! increment snapshot
  call trace_end
END SUBROUTINE write_amr

!*******************************************************************************
SUBROUTINE write_field (task, f, c, rec)
  USE mpi_reduce                                           ! max, min, average
  implicit none
  type(task_t), pointer:: task
  integer:: unit, rec
  real, dimension(task%m(1)%lb:task%m(1)%ub, &
                  task%m(2)%lb:task%m(2)%ub, &
                  task%m(3)%lb:task%m(3)%ub):: f
  character(len=*):: c
  real, pointer, dimension(:,:,:) :: interior              ! without ghost zones
  real:: mn, av, mx
  type(mesh_t), pointer:: m(:)
!...............................................................................
  m => task%m
  allocate(interior(m(1)%n,m(2)%n,m(3)%n))                                      ! buffer for I/O
  interior = f(0:m(1)%n-1,0:m(2)%n-1,0:m(3)%n-1)                                ! skip ghost zones
  write (data_unit,rec=rec) interior
  mn = minval(interior);       call min_real_mpi(mn)       ! global min
  av = sum(interior)/product(task%m%gn);   call sum_real_mpi(av)                ! global average
  mx = maxval(interior);       call max_real_mpi(mx)       ! global max
  if (master) print '(i5,3x,a,1p,e11.4,3x,a,3e11.3,2x,a)', &
    task%iout, 't =',task%t,' min, aver, max :', mn, av, mx, c                  ! printout
  deallocate (interior)
END SUBROUTINE write_field
END MODULE io_amr
