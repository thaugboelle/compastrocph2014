!*******************************************************************************
! Module with data type for MHD.  To be plug-in compatible in task_mod.f90 the
! module and data type names are generic, rather than problem specific.  The
! choice of problem and solver is done by linking with the relevant module.
!*******************************************************************************
MODULE state_mod
  USE mesh_mod,   only: mesh_t
  USE scalar_mod, only: init_scalar
  USE vector_mod, only: vector_t, init_vector
  USE io
PRIVATE
  type:: state_t
    real, dimension(:,:,:), pointer:: d, s
    type(vector_t):: p, b
  end type
PUBLIC:: state_t, init_state
CONTAINS

!*******************************************************************************
SUBROUTINE init_state (mem, mhd)
  implicit none
  real, dimension(:,:,:,:), pointer:: mem, v
  real, dimension(:,:,:), pointer:: s
  type(state_t):: mhd
!...............................................................................
  call trace_begin('init_mhd')
  s => mem(:,:,:,1)  ; call init_scalar (s, mhd%d)
  s => mem(:,:,:,2)  ; call init_scalar (s, mhd%s)
  v => mem(:,:,:,3:5); call init_vector (v, mhd%p)
  v => mem(:,:,:,6:8); call init_vector (v, mhd%b)
  call trace_end
END SUBROUTINE init_state

END MODULE state_mod
