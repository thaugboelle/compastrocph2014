!*******************************************************************************
! The location of a point should be expressed as
!
! r(i) = p%pos(dim,it) + m%r(i)
!
! where p%pos(:,it) are the center coordinates of the patch at time index it,
! and m%r(i) is the cell coordinate relative to the center.
!*******************************************************************************
MODULE mesh_mod
  USE mpi_base, only: master, end_mpi, mpi_rank
  USE io      , only: input, trace_begin, trace_end, trace
  implicit none
  type :: mesh_t
    integer :: gn                             ! Global mesh size
    integer :: n=16                           ! Local mesh size
    real(8) :: s=1d0                          ! interior mesh size
    real(8) :: d                              ! cell size
    real(8) :: o                              ! interior zero point offset
    real(8) :: p                              ! center position
    real(4) :: u                              ! mesh velocity
    real(8), allocatable, dimension(:) :: r   ! coordinate axis
    integer :: ng=2                           ! Number of ghost cells
    integer :: lb                             ! lower bdry of mesh incl ghost zones                
    integer :: ub                             ! upper bdry of mesh incl ghost zones     
    integer :: li                             ! lower bdry of mesh excl ghost zones                
    integer :: ui                             ! upper bdry of mesh excl ghost zones     
    integer :: mpi                            ! number of MPI splits           
    integer :: id                             ! copy of patch id
    real(4) :: lf, uf                         ! floating interpolation boundaries 
    logical :: is_allocated = .false.
  end type
  real(kind=8):: box(3) = 1d0                 ! box size
PRIVATE
PUBLIC mesh_t, read_mesh, init_mesh, box
CONTAINS

!*******************************************************************************
SUBROUTINE read_mesh (m, level, iostat)
  implicit none
  type(mesh_t):: m(3)
  integer:: level, iostat, dim
  real(8), dimension(3):: pos = (/0.0,0.0,0.0/)
  real,    dimension(3):: size = 1.0, u = 0.0
  integer, dimension(3):: n = 16
  namelist /mesh_params/ pos, size, n, level, u
  !-----------------------------------------------------------------------------
  ! Setup MPI with a cartesian MPI arrangement
  !-----------------------------------------------------------------------------
  call trace_begin ('read_mesh')
  level = 1
  read(input,mesh_params,iostat=iostat); if (master) write(*,mesh_params)       ! read one mesh
  if (iostat > 0) then
    print*,mpi_rank,' iostat:',iostat
    rewind(input)
    read(input,mesh_params); if (master) write(*,mesh_params)                   ! read one mesh to check
    return                                                                      ! last mesh done
    call trace_end
  end if
  m%p  = pos
  m%s  = size                                                                   ! global size
  m%n  = n                                                                      ! local domain size
  m%u  = u
  m%d  = m%s/m%n
  m%ng = 2
  m%gn = m%n + 2*m%ng
  m%lb = 1
  m%li = m%lb + m%ng
  m%ui = m%li + m%n - 1
  m%ub = m%ui + m%ng 
  where (m%n==1)
    m%lb = 0
    m%ub = 0
  end where
  call trace_end
END SUBROUTINE read_mesh

!*******************************************************************************
SUBROUTINE init_mesh (m)
  implicit none
  type(mesh_t):: m(3)
  integer:: i
!...............................................................................
  do i=1,3
    call init_mesh1(m(i))
  end do
END SUBROUTINE init_mesh

!*******************************************************************************
SUBROUTINE init_mesh1 (m)
  implicit none
  type(mesh_t):: m
  integer :: dim, i
  real(kind=8):: offset
  !----------------------------------------------------------------------------
  ! Set up mesh. The mesh point position is at the center of the cell.
  ! Point interval is [-ng : n + ng - 1], physical interval is [0:n-1]
  !----------------------------------------------------------------------------
  call trace_begin ('init_mesh1')
  m%d = m%s/m%n
  m%gn = m%n+2*m%ng
  m%lb = 1
  m%li = m%lb + m%ng
  m%ui = m%li + m%n - 1
  m%ub = m%ui + m%ng
  print*,'computing r for mesh id',m%id
  if (.not.m%is_allocated) allocate(m%r(m%lb:m%ub))
  offset = (m%s-m%d)/2d0
  do i=m%lb,m%ub
    m%r(i) = m%d*real(i-m%li,kind=8) - offset
  end do
  m%o = m%r(m%li)                    ! origin => floating index = 0.0 at m%li 
  m%lf = m%li - 0.01                 ! allow a 1% margin for extrapolation
  m%uf = m%ui + 0.01                 ! allow a 1% margin for extrapolation
  if (m%id == 1) m%uf = m%ui + 1.01  ! root grid has one more point
  m%is_allocated = .true.
  call trace_end
END SUBROUTINE init_mesh1

!*******************************************************************************
END MODULE mesh_mod
