! $Id$
!************************************************************************
MODULE timestep
  USE state_mod
  USE pde_mod,  only: pde, courant_internal, courant_number
  USE task_mod, only: task_t
  USE download_mod, only: download
  USE io
  implicit none
PRIVATE
  logical:: first_time=.true.
  real, dimension(3) ::  alpha, beta
  integer:: timeorder=3
PUBLIC:: time_step, courant_number
CONTAINS

!************************************************************************
SUBROUTINE init_timestep
  implicit none
  namelist /timestep_params/ courant_number, timeorder
!........................................................................
  call trace_begin ('init_timestep')
  rewind(input); read(input,timestep_params); if(master) write(*,timestep_params)
  if      (timeorder==1) then
                              alpha=(/ 0.00000,  0.000000,  0.000000 /)
                              beta =(/ 1.00000,  0.000000,  0.000000 /)
  else if (timeorder==2) then
                              alpha=(/ 0.00000, -0.500000,  0.000000 /)
                              beta =(/ 0.50000,  1.000000,  0.000000 /)
  else if (timeorder==3) then
                              alpha=(/ 0.00000, -0.641874, -1.310210 /)
                              beta =(/ 0.46173,  0.924087,  0.390614 /)
  end if
  first_time = .false.
  call trace_end
END SUBROUTINE init_timestep

!************************************************************************
SUBROUTINE time_step (task, it)
  implicit none
  type(task_t), pointer:: task
  integer:: it
  real(8):: t, dt, dtdt
  real, dimension(:,:,:,:), pointer:: v, w
  type(state_t):: f, d
  integer:: iv, substep
  character(len=mch):: id="timestep $Id$"
!........................................................................
  call print_id (id)
  call trace_begin ('timestep')
  if (first_time) call init_timestep
!------------------------------------------------------------------------
!  Loop over Runge-Kutta stages
!------------------------------------------------------------------------
  v => task%mem(:,:,:,:,it,1)
  w => task%mem(:,:,:,:,it,2)
  do substep=1,timeorder
    if (substep==1) then
      do iv=1,task%nv
        w(:,:,:,iv) = 0.0
      end do
      dtdt = 0.0
    else
      do iv=1,task%nv
        w(:,:,:,iv) = alpha(substep)*w(:,:,:,iv)
      end do
      dtdt = alpha(substep)*dtdt
    end if
    call pde (task%m, task%f, task%d)
    dtdt = dtdt + 1.0
    if (substep==1) call courant_internal (task%m, task%f, task%dt(it))
    do iv=1,task%nv
      v(:,:,:,iv) = v(:,:,:,iv) + (task%dt(it)*beta(substep))*w(:,:,:,iv)
    end do
    task%t(it)     = task%t(it)     + task%dt(it)*beta(substep)*dtdt
    task%pos(:,it) = task%pos(:,it) + task%dt(it)*beta(substep)*dtdt*task%m%u
    if (do_debug) print'(i7," task",i5," it,sb,t,dt =",2i3,2f10.6," pos =",3f9.4,5f10.5)', &
         mpi_rank, task%id, it, substep, task%t(it), task%dt(it), task%pos(:,it), &
         task%mem(8:12,10,10,8,it,1)
    call download (task%id)                       ! download relevant data
  end do
  call trace_end
END SUBROUTINE time_step

END MODULE timestep
