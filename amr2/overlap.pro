; $Id$
;-------------------------------------------------------------------------------
; Two cases:
; 1) download from lower resolution to ghost zones:
;    Check overlap with ghost zone regions; use returned index range to update
;    values there.  The overlap region is defined with respect to the full
;    interior of mesh m2, and the ghost zone region of our mesh (m1).
; 2) upload from overlap region with interior:
;    Check overlap with interiod; use returned index ranges to update values.
;    The overlap region is defined with respect to the fill interior of mesh m2
;    and our interior.
;-------------------------------------------------------------------------------
FUNCTION overlap, m1, m2, lu1, lu1p
  if m1.min eq 99. then m1.min = m1.c - (m1.n-1.)/2.*m1.d
  if m1.max eq 99. then m1.max = m1.c + (m1.n-1.)/2.*m1.d
  if m2.min eq 99. then m2.min = m2.c - (m2.n-1.)/2.*m2.d
  if m2.max eq 99. then m2.max = m2.c + (m2.n-1.)/2.*m2.d
  ; lower and uppper coordinate position in m2, relative to our index zero,
  ; expressed in our floating index coordinates
  l1p = (m2.min - m1.min)/m1.d
  u1p = (m2.max - m1.min)/m1.d
  ; no overlap if l1 too large or u1 too small
  if (l1p gt lu1[1]) or (u1p lt lu1[0]) then return, 0
  ; bracket to our range
  lu1[0] = ceil (l1p) > lu1[0]
  lu1[1] = floor(u1p) < lu1[1]
  return, 1
END

PRO interpolate, m1, m2, lu1
  for i1=lu1[0],lu1[1] do begin
    p2 = (m1.min + i1*m1.d - m2.min)/m2.d
    i2 = floor(p2)
    p2 = p2-i2
    print,i1,i2,p2
  end
END

PRO overlap_test
  m0 = {d:0.20, c:1.0, ng:2, n:8, min:99., max:99.}
  m1 = {d:0.10, c:1.0, ng:2, n:8, min:99., max:99.}
  m2 = {d:0.05, c:1.0, ng:2, n:8, min:99., max:99.}

  ; first we ask if mesh m0 can supply us with values (-ng,-ng+1) on mesh m1
  lu1 = [-m1.ng,-m1.ng+1]
  print, overlap(m1, m0, lu1), lu1
  if overlap(m1, m0, lu1) then interpolate, m1, m0, lu1

  ; then we ask if mesh m0 can supply us with values (n,n+ng-1) on mesh m1
  lu1 = [m1.n,m1.n+m1.ng-1]
  print, overlap(m1, m0, lu1), lu1
  if overlap(m1, m0, lu1) then interpolate, m1, m0, lu1

  ; then we ask if mesh m2 can supply us with values in (0,n-1) on mesh m1
  lu1 = [0,m1.n-1]
  print, overlap(m1, m2, lu1), lu1
  if overlap(m1, m2, lu1) then interpolate, m1, m2, lu1
END